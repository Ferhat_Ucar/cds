const path = require('path');
const fg = require('fast-glob');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// ---

const themes = () => {
    const theme = {};
    fg.sync(['./src/**/main.scss'])
        .forEach((file) => {
            const brand = file.replace('./src/', '').replace('/main.scss', '');
            theme[brand] = file;
        });
    return theme;
};


// ---

module.exports = {
    entry: {
        ...themes()
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'delete-me-[name].js'
    },
    module: {
        rules: [
            {
                test: /\.s[c|a]ss$/,
                use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
            },
            {
                test: /\.(svg|jpg|png)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {},
                  },
                ],
            },
            {
                test: /\.(woff|woff2)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                        context: 'src'
                    },
                  },
                ],
            }
        ]
    },
    devServer: {
        writeToDisk: true,
        contentBase: path.resolve(__dirname, 'dist'),
        compress: true,
        port: 9001,
        https: false,
        open: false
    },
    plugins: [
        new MiniCssExtractPlugin({
          filename: '[name]/[name].stylesheet.css',
        }),
        new FileManagerPlugin({
            onStart: {
                copy: [
                    {
                        source: './src/**/assets/**/*.*',
                        destination: './dist/'
                    }
                ]
            },
            onEnd: {
                delete: [
                    './dist/delete-me-*.js'
                ],
                copy: [
                    {
                        source: './dist/**/*.*',
                        destination: '../calvi-design-system/src/assets/'
                    }
                ]
            }
        })
    ]
};
