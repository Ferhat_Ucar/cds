```html
<c-list is="context-menu">
    <c-list-item>
        <c-link link="/path" css="list-item">Link</c-link>
    </c-list-item>
    <c-list-item>
        <c-button is="button-primary" css="list-item">Button</c-link>
    </c-list-item>
</c-list>
```
