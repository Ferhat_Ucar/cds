The chart dispatches a custom event called **c-chart**.
AngularJS has a special built-in ngOn directive for custom events.



#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-chart** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-chart="myVar = $event.detail.value()"**

or:
**ng-on-c-chart="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)




#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-chart** dispatches the following details:

| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **value()**       | object                | Returns all chart-item data as an object: { index: index of the chart-item, data: all available data}  |    |





#### Example
```html
<c-chart is="dounut-chart" ng-on-c-chart="myVar = $event.detail.value()">
    <c-chart-item label="i am a label" value="14.345,55" percentage="43" month="februari" year="2019"></c-chart-item>
    <c-chart-item label="i am a label" value="14.345,55" percentage="37" month="march" year="2019"></c-chart-item>
    <c-chart-item label="i am a label" value="14.345,55" percentage="46" month="april" year="2019" selected></c-chart-item>
</c-chart>
```
