```html
<c-chart is="pie-chart">
    <c-chart-item label="i am the label" value="14.345,55" percentage="70" color="color-accent-12"></c-chart-item>
    <c-chart-item label="i am the label" value="5.345,55" percentage="30" color="color-accent-02"></c-chart-item>
</c-chart>
```
