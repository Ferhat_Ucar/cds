Charts are graphical presentation of structured data.

All charts are rendered as list 'ul' all chart-items are rendered as list-items 'li'


### Chart
#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **is**            | bar-chart         | The attribute 'is' determines how the chart is rendered. (renders as bar-chart) | * |
|                   | column-chart      | The attribute 'is' determines how the chart is rendered. (renders as column-chart) | * |
|                   | donut-chart       | The attribute 'is' determines how the chart is rendered. (renders as donut-chart) | * |
|                   | pie-chart         | The attribute 'is' determines how the chart is rendered. (renders as pie-chart) | * |
|                   | spline-chart      | The attribute 'is' determines how the chart is rendered. (renders as spline-chart) | * |
|                   | stacked-bar-chart | The attribute 'is' determines how the chart is rendered. (renders as stacked-bar-chart) | * |
|                   | stacked-column-chart | The attribute 'is' determines how the chart is rendered. (renders as stacked-column-chart) | * |
| **style**         | x                 | The style attribute is not allowed | x |



### Chart-item
#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **label**         | string            | locale string describing the chart-item | * |
| **month**         | string            | locale string describing the month of the chart-item |   |
| **percentage**    | number            | numeric value of the chart-item representing the percentage value (do not use %-sign)| * |
| **selected**      | string            | The selected attribute preselects a chart-item | * |
| **style**         | x                 | The style attribute is not allowed | x |
| **value**         | string           | value of the chart-item |   |
| **year**          | string            | locale string describing the month of the chart-item |   |



#### User input
Users should be able to focus on a chart item by using the Tab-key.

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | chart-item        | Adds focus state on the chart-item |    |
| **key:tab**               | chart-item        | Adds focus state on the chart-item |    |
| **touch**                 | chart-item        | Adds focus state on the chart-item |    |
