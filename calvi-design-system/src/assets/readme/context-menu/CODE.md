```html
<c-overflow-menu>
    <c-button is="button-primary">
        Action
        <c-icon is="icon-arrow" css=""></c-icon>
    </c-button>
    <c-list is="context-menu">
        <c-list-item>
            <c-link link="/path" css="list-item"><c-icon is="icon-add"></c-icon> Go to person</c-link>
        </c-list-item>
        <c-list-item>
            <c-link link="/path" css="list-item"><c-icon is="icon-search"></c-icon> Go to asset</c-link>
        </c-list-item>
        <c-list-item>
            <c-link link="/path" css="list-item"><c-icon is="icon-add"></c-icon> Go to something else</c-link>
        </c-list-item>
        <c-list-item>
            <c-button is="button-primary" css="list-item">Button item</c-button>
        </c-list-item>
        <c-list-item>
            <c-divider></c-divider>
        </c-list-item>
        <c-list-item>
            <c-link link="/path" css="list-item">Create dispute</c-link>
        </c-list-item>
    </c-list>
</c-overflow-menu>
```
