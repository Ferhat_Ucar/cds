```html
<c-notification is="notification-success" visible="true">
    <c-button is="button-ghost-icon" task="toggle">
        <span class="hidden-label">Close</span>
        <c-icon is="icon-close"></c-icon>
    </c-button>
    <h3>Success message</h3>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Sed nisi quam, eleifend pharetra viverra ac, vehicula et nisi. In magna odio, vestibulum sed gravida in, fermentum ac dolor.
    </p>
</c-notification>
```
