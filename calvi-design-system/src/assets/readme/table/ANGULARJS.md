The table dispatches a custom event called **c-table**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-table** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-table="myVar = $event.detail.selectAllRows()"**

or:
**ng-on-c-table="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)


#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-table** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **selectAllRows()**| boolean              | Returns the value of select-all checkbox  |    |
|                   |                       |                                       |    |
| **selectRow()**   | boolean               | Returns the value of select-row checkbox  |    |
|                   |                       |                                       |    |
| **sortField()**   | string                | Returns the sort field  |    |
| **sortDirection()**| ascending/descending | Returns the sort direction  |    |
