```html
<c-table>
    <c-table-head>
        <c-table-row>
            <c-table-header>
                <c-button is="button-ghost" css="table-sort-button" data-sort-sortable="true" data-sort-field="Date" task="table-sort"><c-icon is="icon-sort"></c-icon>Table header 1</c-button>
            </c-table-header>
            <c-table-header>
                <c-button is="button-ghost" css="table-sort-button" data-sort-sortable="true" data-sort-field="Invoice number" task="table-sort"><c-icon is="icon-sort"></c-icon>Table header 2</c-button>
            </c-table-header>
            <c-table-header>
                <c-button is="button-ghost" css="table-sort-button" data-sort-sortable="true" data-sort-field="Customer account<" task="table-sort"><c-icon is="icon-sort"></c-icon>Table header 3</c-button>
            </c-table-header>
            <c-table-header>
                <c-button is="button-ghost" css="table-sort-button" data-sort-sortable="true" data-sort-field="Amount" task="table-sort"><c-icon is="icon-sort"></c-icon>Table header 4</c-button>
            </c-table-header>
        </c-table-row>
    </c-table-head>
    <c-table-body>
        <c-table-row>
            <c-table-cell>Row 1, Datacel 1</c-table-cell>
            <c-table-cell>Row 1, Datacel 2</c-table-cell>
            <c-table-cell>Row 1, Datacel 3</c-table-cell>
            <c-table-cell>Row 1, Datacel 4</c-table-cell>
        </c-table-row>
        <c-table-row>
            <c-table-cell>Row 2, Datacel 1</c-table-cell>
            <c-table-cell>Row 2, Datacel 2</c-table-cell>
            <c-table-cell>Row 2, Datacel 3</c-table-cell>
            <c-table-cell>Row 2, Datacel 4</c-table-cell>
        </c-table-row>
    </c-table-body>
</c-table>
```
