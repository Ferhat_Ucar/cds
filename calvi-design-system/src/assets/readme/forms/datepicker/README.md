Date and time pickers allow users to select a single or a range of dates and times.

Search is a wrapper using the [c-input](/components/forms/input-text) and [c-button](/components/buttons/button-primary) components.
Both the c-input and the c-button must be included inside the wrapper. The c-button must have the attribute 'task' set to 'datepicker-toggle';


#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **date**          | string            | Sets the initial selected date (optional). Use Calvi date format: yyyy-mm-dd |  |
| **iso**           | string            | Iso country code. eg. 'en-GB' | * |
| **label-month-next**| string          | locale label for button 'next-month'  | * |
| **label-month-previous**| string      | locale label for button 'previous-month'  | * |
| **label-year-next**| string           | locale label for button 'next-year'  | * |
| **label-year-previous**| string       | locale label for button 'previous-year'  | * |
| **max-date**      | string            | Sets the end date for a date range(optional). Use Calvi date format: yyyy-mm-dd  |  |
| **min-date**      | string            | Sets the start date for a date range(optional). Use Calvi date format: yyyy-mm-dd  |  |
| **months**        | string[]          | comma separated list of locale month names  | * |
| **notation**      | string            | string representing the locale date notation eg. 'mm/dd/yyyy'  | * |
| **style**         | x                 | The style attribute is not allowed | x |
| **value**         | string            | The 'value' attribute is optional |  |
| **weekdays**      | string[]          | comma separated list of locale weekday names  | * |



#### User input
Users should be able to focus on the input by clicking on the box directly or by clicking on its label.

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | input             | Adds focus state on the input |    |
| **key:tab**               | input             | Adds focus state on the input or when input is focussed, puts focus on the calendar button |    |
| **touch**                 | input             | Adds focus state on the input |    |
| **touch**                 | input             | Adds focus state on the input |    |
| **click**                 | label             | Adds focus state on the input |    |
| **touch**                 | label             | Adds focus state on the input |    |
| **click**                 | button            | toggles calendar |    |
| **key:enter**             | button             | toggles calendar |    |
| **touch**                 | button             | toggles calendar |    |
| **calendar:toggle**       | calendar           | focus on the current date. |    |
| **key:tab**               | calendar           | switch focus between selected date and mont/year buttons. |    |
| **&uarr; arrow up**       | calendar:date     | navigate between weeks on the same week day. - 1week |    |
| **&rarr; arrow right**    | calendar:date          | navigate the row by week day + 1day |    |
| **&darr; arrow down**     | calendar:date          | navigate between weeks on the same week day. + 1week |    |
| **&larr; arrow left**     | calendar:date         | navigate the row by week day - 1day |    |
| **&uArr; page up**       | calendar:date          | navigate between months. - 1month |    |
| **&dArr; page down**     | calendar:date         | navigate between months. + 1month |    |
| **click**                 | calendar:date         | select current date & close calendar  |    |
| **key:enter**             | calendar:date         | select current date & close calendar  |    |
| **key:escape**             | calendar:date         |  close the calendar without making a selection  |    |
| **click**                 | calendar:button         | select next/previous month/year  |    |
| **key:enter**             | calendar:button        | select next/previous month/year  |    |
| **key:escape**             | calendar:button        |  close the calendar without making a selection  |    |
