The datepicker dispatches a custom event called **c-datepicker**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-datepicker** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-datepicker="myVar = $event.detail.value()"**

or:
**ng-on-c-datepicker="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)


#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-datepicker** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **value()**       | string                | Returns the value attribute value  |    |


#### States
The datepicker component is watching the state: **value**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **value**         | value=""          | Sets the value in the input component |    |



#### Example
```html
<c-datepicker
    weekdays="Mon,Tue,Wed,Thu,Fri,Sat,Sun"
    months="januari,februari,march,april,may,june,july,august,september,october,november,december"
    notation="mm/dd/yyyy"
    iso="en-GB"
    label-year-next="Next year"
    label-year-previous="Previous year"
    label-month-next="Next month"
    label-month-previous="Previous month"
    ng-on-c-datepicker="myDatepickerVar = $event.detail.value();"
>
    <c-input is="text" value="" placeholder="Select a date (mm/dd/yyyy)" id="datepicker-01-input" pattern="((0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01])/[12]\d{3})" error-message-pattern="Error. The date format is incorrect" error-message-required="Error. This is a required field">Date</c-input>
    <c-button task="datepicker-toggle" is="button-primary-icon" id="datepicker-01-button"><span class="hidden-label">Select a date</span><c-icon is="icon-datepicker"></c-icon></c-button>
</c-datepicker>
```
