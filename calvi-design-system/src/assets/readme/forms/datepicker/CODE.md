```html
<c-datepicker
    weekdays="Mon,Tue,Wed,Thu,Fri,Sat,Sun"
    months="januari,februari,march,april,may,june,july,august,september,october,november,december"
    notation="mm/dd/yyyy"
    iso="en-GB"
    label-year-next="Next year"
    label-year-previous="Previous year"
    label-month-next="Next month"
    label-month-previous="Previous month"
>
    <c-input
        is="text"
        value=""
        placeholder="Select a date (mm/dd/yyyy)"
        id="datepicker-01-input"
        pattern="((0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01])/[12]\d{3})"
        error-message-pattern="Error. The date format is incorrect"
        error-message-required="Error. This is a required field"
    >
        Date
    </c-input>
    <c-button task="datepicker-toggle" is="button-primary-icon" id="datepicker-01-button">
        <span class="hidden-label">Select a date</span>
        <c-icon is="icon-datepicker"></c-icon>
    </c-button>
</c-datepicker>
```
