The radio dispatches a custom event called **c-radio**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-radio** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-radio="myVar = $event.detail.checked()"**

or:
**ng-on-c-radio="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)



#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-radio** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **checked()**     | boolean               | Returns the checked state of the component |    |
| **value()**       | string               | Returns the value attribute value  |    |



#### States
The radio component is watching two states: **checked** and **disabled**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **checked**       | checked           | A boolean indicating whether or not this radio button is the currently-selected item in the group |    |
| **disabled**      | disabled          | Determines the disabled state of the object |    |




#### Example
```html
<c-radio is="radio" id="your-id" name="radio-group-name" value="Radio value 1" ng-checked="{{true/false}}" ng-on-c-radio="myVar = $event.detail.checked()">Label</c-radio>
```
