```html
    <c-input
        is="text"
        value=""
        placeholder="Placeholder text"
        id="id-input-01"
        name="inputfield"
        pattern="^[a-zA-Z ]+$"
        required
        error-message-pattern="Error. The text can only contain letters"
        error-message-required="Error. This is a required field">
        Input label
    </c-input>
```
