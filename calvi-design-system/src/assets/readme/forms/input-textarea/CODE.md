```html
    <c-textarea
        value=""
        placeholder="Placeholder text"
        id="textarea-id-01"
        name="textarea"
        pattern="^[a-zA-Z .]+$"
        required
        error-message-pattern="Error. The text can only contain letters"
        error-message-required="Error. This is a required field">
            Textarea label
    </c-textarea>
```
