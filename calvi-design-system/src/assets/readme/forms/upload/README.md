#### Usage

The ```<c-input />``` tag specifies an input field where the user can enter data. An input field can vary in many ways, depending on the type 'is' attribute.

| Option            | Value                                 | Description                               | Required  |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **is**            | text                                  | defines the text input                    | * |
|                   | password                              | defines the password input                | * |
|                   | phone                                 | defines the phone input                   | * |
|                   | number                                | defines the number input                  | * |
|                   | email                                 | defines the email input                   | * |
|                   | date                                  | defines the date input                    | * |
|                   | time                                  | defines the time input                    | * |
|                   | datetime                              | defines the datetime input                | * |
|                   | file                                  | defines the file input                    | * |
| **css**           | size-small                            | add an extra css class                    |   |
| **disabled**      | boolean                               | disables the input                        |   |
| **required**      | boolean                               | requires the input                        | * |
| **placeholder**   | string                                | adds placeholder attribute                | * |
| **pattern**       | string                                | adds pattern attribute e.g. ^([a-zA-Z0-9]{4,10})$    | * |
| **id**            | string                                | adds an id attribute                      | * |
|                   | ```<c-input />```                     | Add a input field                         |   |
