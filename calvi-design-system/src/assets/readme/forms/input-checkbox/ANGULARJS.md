The checkbox dispatches a custom event called **c-checkbox**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-checkbox** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-checkbox="myVar = $event.detail.checked()"**

or:
**ng-on-c-checkbox="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)


#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-checkbox** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **checked()**     | boolean               | Returns the checked state of the component |    |
| **task()**        | string                | Returns the task attribute value  |    |
| **value()**       | string               | Returns the value attribute value  |    |




#### States
The checkbox component is watching 3 states: **checked**, **unchecked**, and **disabled**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **checked**       | checked           | Determines the checked state of the object |    |
| **unchecked**     | -                 | Determines the checked state of the object |    |
| **disabled**      | disabled          | Determines the disabled state of the object |    |



#### Example
```html
<c-checkbox is="checkbox" id="your-id" task="your-task" ng-checked="{{true/false}}" ng-on-c-checkbox="myVar = $event.detail.checked()">Label</c-checkbox>
```
