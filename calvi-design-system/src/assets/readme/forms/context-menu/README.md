#### Usage

The context-menu consists of 2 components, the button component and the list component. When the button gets the is-selected class (on click or with space bar), the list component will be shown otherwise it is hidden.

| Option        | Value                                 | Description                               | Required  |
| ------------- |:-------------:                        | ------------:                             |------------:|
| **is**        | button-primary                        | defines the primary button                | * |
|               | button-secondary                      | defines the secondary button              | * |
|               | button-ghost                          | defines the ghost button                  | * |
|               | context-menu                          | defines the list                          | * |
|               | quicklink                             | defines the list                          | * |
|               | list-item                             | defines the list item                     | * |
| **disabled**  | boolean                               | disables the button                       |   |
| **innerHTML** | string                                | defines the button label                  |   |
