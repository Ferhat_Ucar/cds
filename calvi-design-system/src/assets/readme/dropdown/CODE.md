```html
<c-dropdown>
    <input id="id-input-01" class="c-input is-text" placeholder="Start typing.." type="text" value="" />
    <c-button is="button-ghost-icon" task="clear">
        <span class="hidden-label">Clear</span>
        <c-icon is="icon-clear"></c-icon>
    </c-button>
    <c-button is="button-ghost-icon" task="toggle">
        <span class="hidden-label">Toggle</span>
        <c-icon is="icon-arrow-down"></c-icon>
    </c-button>
    <c-options id="id-options-01" label="options-label" label-no-results="There are no results">
        <c-option id="id-opion-01">Option 1</c-option>
        <c-option id="id-opion-02">Option 2</c-option>
        <c-option id="id-opion-03">Option 3</c-option>
    </c-options>
</c-dropdown>
```
