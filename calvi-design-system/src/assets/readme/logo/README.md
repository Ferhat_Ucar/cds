#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **link**          | string            | Sets the link for the click action on the logo (e.g. homepage) | * |
| **label**         | string            | Label for the alt-tag and hidden-label for screen readers | * |

