```js
import angular from 'angular';
import '@calvi/calvi-design-system/calvi-design-system';
import '@calvi/calvi-design-system/themes/calvi/calvi.stylesheet.css';

import '../style/app.css';


let app = () => {
    return {
        template: require('./app.html'),
        controllerAs: 'app',
        controller: 'AppCtrl'
    }
};



class AppCtrl {
    constructor() {
        this.checkbox01 = false;
        this.switch01 = true;
        this.radio01 = true;
        this.radio02 = false;
        this.radio03 = false;
        this.input01 = 'I am input 01';
        this.searchString = '';
        this.searchSubmit = false;
    }

    radio($event) {
        this.radio01 = false;
        this.radio02 = false;
        this.radio03 = false;
        let id = $event.detail.target().id.replace('c-', '');
        id = id.replace('-', '');
        this[id] = true;
    }
}




const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [])
    .directive('app', app)
    .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;

```
