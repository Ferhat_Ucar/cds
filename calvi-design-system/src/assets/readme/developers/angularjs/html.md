```html
<main>

    <c-checkbox is="checkbox" id="checkbox-01" task="demo-purpose" ng-checked="{{app.checkbox01}}" ng-on-c-checkbox="app.checkbox01 = $event.detail.checked()">Switch label</c-checkbox> <span class="state">value: {{app.checkbox01}}</span><br />

    <br /><br />

    <c-checkbox is="switch" id="switch-01" task="demo-purpose" ng-checked="{{app.switch01}}" ng-on-c-checkbox="app.switch01 = $event.detail.checked()">Switch label</c-checkbox> <span class="state">value: {{app.switch01}}</span><br />


    <br /><br />

    <c-radio is="radio" id="radio-01" name="c-radio-group-01" value="Radio value 1" ng-checked="{{app.radio01}}" ng-on-c-radio="app.radio($event)">Radio label</c-radio> <span class="state">value: {{app.radio01}}</span><br />
    <c-radio is="radio" id="radio-02" name="c-radio-group-01" value="Radio value 2" ng-checked="{{app.radio02}}" ng-on-c-radio="app.radio($event)">Radio label</c-radio> <span class="state">value: {{app.radio02}}</span><br />
    <c-radio is="radio" id="radio-03" name="c-radio-group-01" value="Radio value 3" ng-checked="{{app.radio02}}" ng-on-c-radio="app.radio($event)">Radio label</c-radio> <span class="state">value: {{app.radio03}}</span><br />


    <br /><br />

    <c-input
        is="text"
        value="{{app.input01}}"
        placeholder="Placeholder text"
        id="input-01"
        name="inputfield"
        pattern="^[a-zA-Z ]+$"
        required
        ng-on-c-input="app.input01 = $event.detail.value()"
        error-message-pattern="Error. The text can only contain letters"
        error-message-required="Error. This is a required field">
        Label
    </c-input>
    <span class="state">value: {{app.input01}}</span><br />


    <br /><br />


    <c-search ng-on-c-search="app.searchString = $event.detail.value(); app.searchSubmit = $event.detail.submit()">
        <c-input is="text" value="" placeholder="e.g. 200003" id="input-search-01" name="inputfield">Search</c-input>
        <c-button is="button-secondary" task="search" id="button-search-01">Search</c-button>
    </c-search>
    <span class="state">search: {{app.searchString}}, submit: {{app.searchSubmit}}</span><br />


</main>

```
