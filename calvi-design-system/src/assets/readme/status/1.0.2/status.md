#### Status
| Item                  | Type          | Status       | Tested       |
| --------------------  |:-------------:| ------------:| ------------:|
| Accordion             | Component     | Done         |   No         |
| Breadcrumbs           | Component     | Done         |   No         |
| Buttons (all)         | Component     | Done         |   No         |
| Charts: Bar           | Component     | Done         |   No         |
| Charts: Column        | Component     | Done         |   No         |
| Charts: Donut         | Component     | Done         |   No         |
| Charts: Pie           | Component     | Done         |   No         |
| Charts: Spline        | Component     | Done         |   No         |
| Charts: Stacked-bar   | Component     | Done         |   No         |
| Charts: Stacked-col   | Component     | Done         |   No         |
| Context-menu          | Component     | Done         |   No         |
| Divider               | Component     | Done         |   No         |
| Filter: Quick filters | Component     | Done         |   No         |