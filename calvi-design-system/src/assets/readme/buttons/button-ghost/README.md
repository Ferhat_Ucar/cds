#### Usage

Buttons express what action will occur when the user clicks or touches it. Buttons are used to initialize an action, either in the background or foreground of an experience.
The component bubbles a click event

| Option        | Value                                 | Description                               | Required  |
| ------------- |:-------------:                        | ------------:                             |------------:|
| **is**        | button-primary                        | defines the primary button                | * |
|               | button-primary-icon                   | defines the primary icon button           | * |
|               | button-secondary                      | defines the secondary button              | * |
|               | button-secondary-icon                 | defines the secondary icon button         | * |
|               | **button-ghost**                      | defines the ghost button                  | * |
|               | button-ghost-icon                     | defines the ghost icon button             | * |
| **css**       | size-small                            | add an extra css class                    |   |
| **disabled**  | boolean                               | disables the button                       |   |
| **type**      | button                                | defines the button type                   |   |
|               | submit                                | defines the submit type                   |   |
| **id**        | string                                | adds an id attribute                      |   |
| **name**      | string                                | adds a name attribute                     |   |
| **innerHTML** | string                                | defines the button label                  |   |
|               | ```<c-icon />```                     | Add a button icon                         |   |
|               | ```<span class="hidden-label" />``` | Add a hidden label for screen readers     | (*) inside icon buttons |
