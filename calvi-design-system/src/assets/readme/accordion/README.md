#### Usage

Expandable and collapsible accordion component, to hide or show content

| Option        | Value                                 | Description                               | Required  |
| ------------- |:-------------:                        | ------------:                             |------------:|
| **is**        | c-button-accordion                    | defines the accordion button              | * |
| **id**        | c-accordion-{#}                       | sets an id for the component              | * |
