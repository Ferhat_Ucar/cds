import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DevelopersComponent {
  constructor() { }
  
}
