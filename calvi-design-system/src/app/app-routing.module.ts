import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// dashboard
import { DashboardComponent } from './dashboard/dashboard.component';


// CDS
import { AboutComponent } from './cds/about/about.component';
import { DesignersComponent } from './cds/designers/designers.component';
import { DevelopersComponent } from './cds/developers/developers.component';
import { StatusComponent } from './cds/status/status.component';

// Styles
import { StylesOverviewComponent } from './styles/styles-overview/styles-overview.component';
import { ColorsComponent } from './styles/colors/colors.component';
import { FontComponent } from './styles/font/font.component';
import { LogoComponent } from './styles/logo/logo.component';
import { TileIconsComponent } from './styles/tile-icons/tile-icons.component';
import { UiIconsComponent } from './styles/ui-icons/ui-icons.component';


// Components
import { AccordionComponent } from './components/accordion/accordion.component';
import { ComponentOverviewComponent } from './components/component-overview/component-overview.component';
import { ContextComponent } from './components/lists/context/context.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { ButtonPrimaryComponent } from './components/buttons/button-primary/button-primary.component';
import { ButtonSecondaryComponent } from './components/buttons/button-secondary/button-secondary.component';
import { ButtonGhostComponent } from './components/buttons/button-ghost/button-ghost.component';
import { DividerComponent } from './components/dividers/divider/divider.component';
import { DropdownComponent } from './components/forms/dropdown/dropdown.component';
import { FieldsetComponent } from './components/forms/fieldset/fieldset.component';
import { TableComponent } from './components/table/table.component';
import { HeadingsComponent } from './components/text/headings/headings.component';
import { InputCheckboxComponent } from './components/forms/input-checkbox/input-checkbox.component';
import { InputRadioComponent } from './components/forms/input-radio/input-radio.component';
import { InputSwitchComponent } from './components/forms/input-switch/input-switch.component';
import { InputTextComponent } from './components/forms/input-text/input-text.component';
import { InputTextareaComponent } from './components/forms/input-textarea/input-textarea.component';
import { LinkComponent } from './components/text/link/link.component';
import { SkiptolinksComponent } from './components/skiptolinks/skiptolinks.component';
import { LoaderComponent } from './components/loader/loader.component';
import { OptionsComponent } from './components/forms/options/options.component';
import { OrderedComponent } from './components/lists/ordered/ordered.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ParagraphComponent } from './components/text/paragraph/paragraph.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { SearchComponent } from './components/forms/search/search.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TileComponent } from './components/tile/tile.component';
import { NotificationComponent } from './components/notification/notification.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { QuickfilterComponent } from './components/filters/quickfilter/quickfilter.component';
import { UnorderedComponent } from './components/lists/unordered/unordered.component';
import { QuicklinkComponent } from './components/lists/quicklink/quicklink.component';
import { PieChartComponent } from './components/charts/pie-chart/pie-chart.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { ColumnChartComponent } from './components/charts/column-chart/column-chart.component';
import { DonutChartComponent } from './components/charts/donut-chart/donut-chart.component';
import { StackedBarChartComponent } from './components/charts/stacked-bar-chart/stacked-bar-chart.component';
import { StackedColumnChartComponent } from './components/charts/stacked-column-chart/stacked-column-chart.component';
import { SplineChartComponent } from './components/charts/spline-chart/spline-chart.component';
import { DatepickerComponent } from './components/forms/datepicker/datepicker.component';
import { FlashMessageComponent } from './components/flash-message/flash-message.component';
import { ModalComponent } from './components/modal/modal.component';
import { UploadComponent } from './components/forms/upload/upload.component';

// Blocks
import { ButtonGroupsComponent } from './blocks/button-groups/button-groups.component';
import { GridComponent } from './blocks/grid/grid.component';
import { GridActionBarComponent } from './blocks/grid-action-bar/grid-action-bar.component';
import { HeaderComponent } from './blocks/header/header.component';
import { TileGroupComponent } from './blocks/tile-group/tile-group.component';


// ---


const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'cds/about', component: AboutComponent },
    { path: 'cds/designers', component: DesignersComponent },
    { path: 'cds/developers', component: DevelopersComponent },
    { path: 'cds/status', component: StatusComponent },
    { path: 'styles', component: StylesOverviewComponent },
    { path: 'styles/colors', component: ColorsComponent },
    { path: 'styles/font', component: FontComponent },
    { path: 'styles/logo', component: LogoComponent },
    { path: 'styles/tile-icons', component: TileIconsComponent },
    { path: 'styles/ui-icons', component: UiIconsComponent },
    { path: 'components', component: ComponentOverviewComponent },
    { path: 'components/accordion', component: AccordionComponent },
    { path: 'components/breadcrumbs', component: BreadcrumbsComponent },
    { path: 'components/buttons/button-ghost', component: ButtonGhostComponent },
    { path: 'components/buttons/button-primary', component: ButtonPrimaryComponent },
    { path: 'components/buttons/button-secondary', component: ButtonSecondaryComponent },
    { path: 'components/context-menu', component: ContextMenuComponent },
    { path: 'components/divider', component: DividerComponent },
    { path: 'components/filters/quickfilter', component: QuickfilterComponent },
    { path: 'components/flash-message', component: FlashMessageComponent },
    { path: 'components/forms/datepicker', component: DatepickerComponent },
    { path: 'components/forms/dropdown', component: DropdownComponent },
    { path: 'components/forms/fieldset', component: FieldsetComponent },
    { path: 'components/forms/input-checkbox', component: InputCheckboxComponent },
    { path: 'components/forms/input-radio', component: InputRadioComponent },
    { path: 'components/forms/input-switch', component: InputSwitchComponent },
    { path: 'components/forms/input-text', component: InputTextComponent },
    { path: 'components/forms/input-textarea', component: InputTextareaComponent },
    { path: 'components/forms/options', component: OptionsComponent },
    { path: 'components/forms/search', component: SearchComponent },
    { path: 'components/forms/upload', component: UploadComponent },
    { path: 'components/charts/bar-chart', component: BarChartComponent },
    { path: 'components/charts/column-chart', component: ColumnChartComponent },
    { path: 'components/charts/donut-chart', component: DonutChartComponent },
    { path: 'components/charts/pie-chart', component: PieChartComponent },
    { path: 'components/charts/spline-chart', component: SplineChartComponent },
    { path: 'components/charts/stacked-bar-chart', component: StackedBarChartComponent },
    { path: 'components/charts/stacked-column-chart', component: StackedColumnChartComponent },
    { path: 'components/table', component: TableComponent },
    { path: 'components/lists/context', component: ContextComponent },
    { path: 'components/lists/ordered', component: OrderedComponent },
    { path: 'components/lists/quicklink', component: QuicklinkComponent },
    { path: 'components/lists/unordered', component: UnorderedComponent },
    { path: 'components/loader', component: LoaderComponent },
    { path: 'components/modal', component: ModalComponent },
    { path: 'components/pagination', component:  PaginationComponent },
    { path: 'components/progress-bar', component:  ProgressBarComponent },
    { path: 'components/tabs', component: TabsComponent },
    { path: 'components/tile', component: TileComponent },
    { path: 'components/notification', component: NotificationComponent },
    { path: 'components/tooltip', component: TooltipComponent },
    { path: 'components/text/headings', component: HeadingsComponent },
    { path: 'components/text/link', component: LinkComponent },
    { path: 'components/text/paragraph', component: ParagraphComponent },
    { path: 'components/skiptolinks', component: SkiptolinksComponent },
    { path: 'blocks/button-group', component: ButtonGroupsComponent },
    { path: 'blocks/grid', component: GridComponent },
    { path: 'blocks/grid-action-bar', component: GridActionBarComponent },
    { path: 'blocks/header', component: HeaderComponent },
    { path: 'blocks/tile-group', component: TileGroupComponent }
];


// ---


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
