import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
//
import { MarkdownModule } from 'ngx-markdown';



// ---



// Calvi Library:
import '../../../components/dist/components';
// Calvi Styleguide scripts:
import '../styleguide';


// ---


// overview
import { ComponentOverviewComponent } from './components/component-overview/component-overview.component';
// cds
import { AboutComponent } from './cds/about/about.component';
import { DesignersComponent } from './cds/designers/designers.component';
import { DevelopersComponent } from './cds/developers/developers.component';
import { StatusComponent } from './cds/status/status.component';
// styles
import { DashboardComponent } from './dashboard/dashboard.component';
import { ColorsComponent } from './styles/colors/colors.component';
import { FontComponent } from './styles/font/font.component';
import { LogoComponent } from './styles/logo/logo.component';
import { TileIconsComponent } from './styles/tile-icons/tile-icons.component';
import { UiIconsComponent } from './styles/ui-icons/ui-icons.component';
// blocks
import { ButtonGroupsComponent } from './blocks/button-groups/button-groups.component';
import { GridComponent } from './blocks/grid/grid.component';
import { GridActionBarComponent } from './blocks/grid-action-bar/grid-action-bar.component';
// components
import { AccordionComponent } from './components/accordion/accordion.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { ButtonPrimaryComponent } from './components/buttons/button-primary/button-primary.component';
import { ButtonSecondaryComponent } from './components/buttons/button-secondary/button-secondary.component';
import { ButtonGhostComponent } from './components/buttons/button-ghost/button-ghost.component';
import { DividerComponent } from './components/dividers/divider/divider.component';
import { HeadingsComponent } from './components/text/headings/headings.component';
import { FieldsetComponent } from './components/forms/fieldset/fieldset.component';
import { DropdownComponent } from './components/forms/dropdown/dropdown.component';
import { InputCheckboxComponent } from './components/forms/input-checkbox/input-checkbox.component';
import { InputRadioComponent } from './components/forms/input-radio/input-radio.component';
import { InputSwitchComponent } from './components/forms/input-switch/input-switch.component';
import { InputTextComponent } from './components/forms/input-text/input-text.component';
import { OptionsComponent } from './components/forms/options/options.component';
import { LinkComponent } from './components/text/link/link.component';
import { SkiptolinksComponent } from './components/skiptolinks/skiptolinks.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { ContextComponent } from './components/lists/context/context.component';
import { QuicklinkComponent } from './components/lists/quicklink/quicklink.component';
import { OrderedComponent } from './components/lists/ordered/ordered.component';
import { UnorderedComponent } from './components/lists/unordered/unordered.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { ParagraphComponent } from './components/text/paragraph/paragraph.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { QuickfilterComponent } from './components/filters/quickfilter/quickfilter.component';
import { StylesOverviewComponent } from './styles/styles-overview/styles-overview.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { InputTextareaComponent } from './components/forms/input-textarea/input-textarea.component';
import { SearchComponent } from './components/forms/search/search.component';
import { TileComponent } from './components/tile/tile.component';
import { NotificationComponent } from './components/notification/notification.component';
import { TableComponent } from './components/table/table.component';
import { PieChartComponent } from './components/charts/pie-chart/pie-chart.component';
import { BarChartComponent } from './components/charts/bar-chart/bar-chart.component';
import { ColumnChartComponent } from './components/charts/column-chart/column-chart.component';
import { DonutChartComponent } from './components/charts/donut-chart/donut-chart.component';
import { StackedBarChartComponent } from './components/charts/stacked-bar-chart/stacked-bar-chart.component';
import { StackedColumnChartComponent } from './components/charts/stacked-column-chart/stacked-column-chart.component';
import { SplineChartComponent } from './components/charts/spline-chart/spline-chart.component';
import { DatepickerComponent } from './components/forms/datepicker/datepicker.component';
import { FlashMessageComponent } from './components/flash-message/flash-message.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './blocks/header/header.component';
import { TileGroupComponent } from './blocks/tile-group/tile-group.component';
import { UploadComponent } from './components/forms/upload/upload.component';




// ---


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ColorsComponent,
    FontComponent,
    UiIconsComponent,
    HeadingsComponent,
    ParagraphComponent,
    AccordionComponent,
    BreadcrumbsComponent,
    ButtonPrimaryComponent,
    ButtonSecondaryComponent,
    ButtonGhostComponent,
    DividerComponent,
    InputCheckboxComponent,
    InputRadioComponent,
    LinkComponent,
    SkiptolinksComponent,
    ContextMenuComponent,
    TabsComponent,
    InputSwitchComponent,
    InputTextComponent,
    ButtonGroupsComponent,
    FieldsetComponent,
    ComponentOverviewComponent,
    LoaderComponent,
    TooltipComponent,
    QuicklinkComponent,
    ContextComponent,
    OrderedComponent,
    UnorderedComponent,
    QuickfilterComponent,
    OptionsComponent,
    DropdownComponent,
    StylesOverviewComponent,
    PaginationComponent,
    InputTextareaComponent,
    SearchComponent,
    TileComponent,
    TileIconsComponent,
    NotificationComponent,
    TableComponent,
    GridComponent,
    ProgressBarComponent,
    PieChartComponent,
    BarChartComponent,
    ColumnChartComponent,
    DonutChartComponent,
    StackedBarChartComponent,
    StackedColumnChartComponent,
    SplineChartComponent,
    DatepickerComponent,
    FlashMessageComponent,
    ModalComponent,
    LogoComponent,
    AboutComponent,
    DesignersComponent,
    DevelopersComponent,
    StatusComponent,
    GridActionBarComponent,
    HeaderComponent,
    TileGroupComponent,
    UploadComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot({
        loader: HttpClient
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
