import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tile-icons',
  templateUrl: './tile-icons.component.html',
  styleUrls: ['./tile-icons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TileIconsComponent {
  constructor() { }
}
