import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileIconsComponent } from './tile-icons.component';

describe('TileIconsComponent', () => {
  let component: TileIconsComponent;
  let fixture: ComponentFixture<TileIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
