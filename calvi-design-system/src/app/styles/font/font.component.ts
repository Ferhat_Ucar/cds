import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-font',
    templateUrl: './font.component.html',
    styleUrls: ['./font.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FontComponent {
    constructor() { }
}
