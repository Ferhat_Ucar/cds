import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-colors',
    templateUrl: './colors.component.html',
    styleUrls: ['./colors.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ColorsComponent {
    constructor() { }
}
