import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-styles-overview',
  templateUrl: './styles-overview.component.html',
  styleUrls: ['./styles-overview.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StylesOverviewComponent {
    constructor() { }
}
