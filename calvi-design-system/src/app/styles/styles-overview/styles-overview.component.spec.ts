import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StylesOverviewComponent } from './styles-overview.component';

describe('StylesOverviewComponent', () => {
  let component: StylesOverviewComponent;
  let fixture: ComponentFixture<StylesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StylesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StylesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
