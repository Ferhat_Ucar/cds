import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-ui-icons',
    templateUrl: './ui-icons.component.html',
    styleUrls: ['./ui-icons.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UiIconsComponent {
    constructor() { }
}
