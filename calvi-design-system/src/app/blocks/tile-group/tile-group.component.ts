import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-tile-group',
    templateUrl: './tile-group.component.html',
    styleUrls: ['./tile-group.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TileGroupComponent {
  constructor() { }
}
