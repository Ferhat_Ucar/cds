import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridActionBarComponent } from './grid-action-bar.component';

describe('GridActionBarComponent', () => {
  let component: GridActionBarComponent;
  let fixture: ComponentFixture<GridActionBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridActionBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridActionBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
