import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-grid-action-bar',
    templateUrl: './grid-action-bar.component.html',
    styleUrls: ['./grid-action-bar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class GridActionBarComponent {
    constructor() { }
}
