import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-button-groups',
    templateUrl: './button-groups.component.html',
    styleUrls: ['./button-groups.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ButtonGroupsComponent {
    constructor() { }
}
