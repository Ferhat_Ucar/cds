import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-fieldset',
    templateUrl: './fieldset.component.html',
    styleUrls: ['./fieldset.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FieldsetComponent {
  constructor() { }
}
