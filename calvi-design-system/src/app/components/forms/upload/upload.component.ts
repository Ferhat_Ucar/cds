import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class UploadComponent {
    constructor() { }
}
