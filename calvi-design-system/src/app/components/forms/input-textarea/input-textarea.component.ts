import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-input-textarea',
  templateUrl: './input-textarea.component.html',
  styleUrls: ['./input-textarea.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InputTextareaComponent {
    constructor() { }
}
