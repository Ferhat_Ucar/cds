import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-input-text',
    templateUrl: './input-text.component.html',
    styleUrls: ['./input-text.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InputTextComponent {
    constructor() { }
}
