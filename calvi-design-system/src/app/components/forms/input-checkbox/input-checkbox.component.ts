import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-input-checkbox',
    templateUrl: './input-checkbox.component.html',
    styleUrls: ['./input-checkbox.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InputCheckboxComponent {
    constructor() { }
}
