import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-input-radio',
    templateUrl: './input-radio.component.html',
    styleUrls: ['./input-radio.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InputRadioComponent {
    constructor() { }
}
