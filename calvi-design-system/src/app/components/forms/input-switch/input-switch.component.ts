import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-input-switch',
    templateUrl: './input-switch.component.html',
    styleUrls: ['./input-switch.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class InputSwitchComponent {
    constructor() { }
}
