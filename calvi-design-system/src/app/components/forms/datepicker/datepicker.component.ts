import { Component, ViewEncapsulation } from '@angular/core';

@Component({
      selector: 'app-datepicker',
      templateUrl: './datepicker.component.html',
      styleUrls: ['./datepicker.component.scss'],
      encapsulation: ViewEncapsulation.None
})
export class DatepickerComponent {
  constructor() { }
}
