import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-flash-message',
    templateUrl: './flash-message.component.html',
    styleUrls: ['./flash-message.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class FlashMessageComponent {
    constructor() { }
}
