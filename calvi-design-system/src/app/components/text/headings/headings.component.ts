import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-headings',
    templateUrl: './headings.component.html',
    styleUrls: ['./headings.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HeadingsComponent {
    constructor() { }
}
