import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-link',
    templateUrl: './link.component.html',
    styleUrls: ['./link.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LinkComponent {
    constructor() { }
}
