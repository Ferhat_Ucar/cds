import { Component, ViewEncapsulation } from '@angular/core';


@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BreadcrumbsComponent {
    constructor() { }
}
