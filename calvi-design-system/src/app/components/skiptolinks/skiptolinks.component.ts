import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-skiptolinks',
  templateUrl: './skiptolinks.component.html',
  styleUrls: ['./skiptolinks.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SkiptolinksComponent {
  constructor() { }
}
