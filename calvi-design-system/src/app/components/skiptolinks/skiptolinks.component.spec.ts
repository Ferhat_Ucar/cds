import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiptolinksComponent } from './skiptolinks.component';

describe('PaginationComponent', () => {
  let component: SkiptolinksComponent;
  let fixture: ComponentFixture<SkiptolinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiptolinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiptolinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
