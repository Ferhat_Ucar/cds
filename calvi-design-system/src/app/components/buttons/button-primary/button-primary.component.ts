import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-button-primary',
    templateUrl: './button-primary.component.html',
    styleUrls: ['./button-primary.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ButtonPrimaryComponent {
    constructor() { }
}
