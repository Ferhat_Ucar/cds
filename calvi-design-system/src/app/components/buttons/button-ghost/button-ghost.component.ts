import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-button-ghost',
    templateUrl: './button-ghost.component.html',
    styleUrls: ['./button-ghost.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ButtonGhostComponent {
    constructor() { }
}
