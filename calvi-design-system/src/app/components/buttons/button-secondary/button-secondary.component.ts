import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-button-secondary',
    templateUrl: './button-secondary.component.html',
    styleUrls: ['./button-secondary.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ButtonSecondaryComponent {
    constructor() { }
}
