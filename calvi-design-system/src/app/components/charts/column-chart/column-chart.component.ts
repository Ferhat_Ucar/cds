import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-column-chart',
    templateUrl: './column-chart.component.html',
    styleUrls: ['./column-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ColumnChartComponent {
    constructor() { }
}
