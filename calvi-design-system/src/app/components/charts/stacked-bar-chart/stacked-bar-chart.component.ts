import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-stacked-bar-chart',
    templateUrl: './stacked-bar-chart.component.html',
    styleUrls: ['./stacked-bar-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class StackedBarChartComponent {
    constructor() { }
}
