import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-spline-chart',
    templateUrl: './spline-chart.component.html',
    styleUrls: ['./spline-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SplineChartComponent {
  constructor() { }
}
