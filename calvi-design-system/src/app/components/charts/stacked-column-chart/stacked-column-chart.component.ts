import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-stacked-column-chart',
    templateUrl: './stacked-column-chart.component.html',
    styleUrls: ['./stacked-column-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class StackedColumnChartComponent {
    constructor() { }
}
