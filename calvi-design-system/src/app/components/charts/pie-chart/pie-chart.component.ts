import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-pie-chart',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
  export class PieChartComponent {
  constructor() { }
}
