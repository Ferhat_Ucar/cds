import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-donut-chart',
    templateUrl: './donut-chart.component.html',
    styleUrls: ['./donut-chart.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DonutChartComponent {
  constructor() { }
}
