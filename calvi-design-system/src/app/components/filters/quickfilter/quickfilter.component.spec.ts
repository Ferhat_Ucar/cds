import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickfilterComponent } from './quickfilter.component';

describe('QuickfilterComponent', () => {
  let component: QuickfilterComponent;
  let fixture: ComponentFixture<QuickfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
