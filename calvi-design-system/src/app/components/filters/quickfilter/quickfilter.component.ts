import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-quickfilter',
    templateUrl: './quickfilter.component.html',
    styleUrls: ['./quickfilter.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuickfilterComponent {
    constructor() { }
}
