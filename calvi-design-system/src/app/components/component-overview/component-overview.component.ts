import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-component-overview',
    templateUrl: './component-overview.component.html',
    styleUrls: ['./component-overview.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ComponentOverviewComponent {
    constructor() { }
}
