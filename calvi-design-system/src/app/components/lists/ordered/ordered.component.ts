import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-ordered',
  templateUrl: './ordered.component.html',
  styleUrls: ['./ordered.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OrderedComponent {
    constructor() { }
}
