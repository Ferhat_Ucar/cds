import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-context',
  templateUrl: './context.component.html',
  styleUrls: ['./context.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContextComponent {
    constructor() { }
}
