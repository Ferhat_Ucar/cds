import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-quicklink',
    templateUrl: './quicklink.component.html',
    styleUrls: ['./quicklink.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuicklinkComponent {
  constructor() { }
}
