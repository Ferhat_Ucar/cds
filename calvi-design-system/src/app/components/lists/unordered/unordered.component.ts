import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-unordered',
  templateUrl: './unordered.component.html',
  styleUrls: ['./unordered.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UnorderedComponent {
  constructor() { }
}
