// Styleguide functions


const session: any = {
    get: (name: string) => sessionStorage.getItem(name) || '',
    set: (name: string, value: any) => sessionStorage.setItem(name, value)
};


// ---


const menuToggle: Function = (menuSession: string) => {
    const menu: any = document.querySelector('.styleguide-menu') || false;
    const accordions: any = menu.querySelectorAll('.c-accordion-button');
    const items: any = menu.querySelectorAll('.styleguide-menu_item');
    const logo: any = menu.querySelector('.styleguide-menu_item');

    // session
    session.set('calvi-design-system:menu-toggle', menuSession);

    // toggle
    if (menuSession === 'is-hidden') {
        menu.classList.remove('is-hidden');
    } else {
        menu.classList.add('is-hidden');
    }

    // set tabindex
    accordions.forEach(function (item: any)  {
        (menuSession === 'is-hidden')
            ? item.removeAttribute('tabindex')
            : item.setAttribute('tabindex', '-1');
    });

    // set tabindex
    items.forEach(function (item: any)  {
        (menuSession === 'is-hidden')
            ? item.removeAttribute('tabindex')
            : item.setAttribute('tabindex', '-1');
    });
};



// ---


const gridToggle: Function = (visibility: boolean) => {
    const items: any = document.querySelectorAll('.styleguide-example');
    items.forEach(function (item: any)  {
        (visibility === true)
            ? item.classList.add('display-grid')
            : item.classList.remove('display-grid');
    });
};


// ---


(() => {
    setTimeout(() => {
        let grid = false;
        const menuSession: string = session.get('calvi-design-system:menu-toggle') || 'is-visible';
        const accordions: any = document.querySelectorAll('c-accordion');


        // buttons
        const buttons: any = {
            grid: document.getElementById('c-styleguide-grid-toggle') || false,
            menu: document.getElementById('c-styleguide-menu-toggle') || false
        };



        // init
        menuToggle(menuSession);
        accordions.forEach(function (accordion: any) {
            if (accordion.classList.contains('active')) {
                accordion.setAttribute('expanded', '');
            }
        });


        // events
        buttons.menu.onclick = () => {
            const visibility: string = (session.get('calvi-design-system:menu-toggle') === 'is-visible')
                ? 'is-hidden'
                : 'is-visible';
            menuToggle(visibility);
        };
        buttons.grid.onclick = () => {
            grid = (grid === false);
            gridToggle(grid);
        };


        // modal demo
        document.addEventListener('c-button', function (event: any) {
            if (event.detail.task() === 'open-modal-01' && event.detail.type() === 'click') {
                document.getElementById('modal-id-01').setAttribute('visible', 'true');
            }
            if (event.detail.task() === 'open-modal-02' && event.detail.type() === 'click') {
                document.getElementById('modal-id-02').setAttribute('visible', 'true');
            }
        });
    }, 1);
})();
