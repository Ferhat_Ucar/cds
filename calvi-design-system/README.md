# Calvi Design System

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Calvi: Using Web Components
For your custom element to work on all browsers, you’ll have to include the polyfills for it. The simplest way is to include webcomponentsjs into your project:
`npm install @webcomponents/webcomponentsjs`

find the file polyfill.ts and add the line:
`import '@webcomponents/webcomponentsjs/webcomponents-sd-ce.js';`

Open the app.module.ts and add the CUSTOM_ELEMENT_SCHEMA:
`import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';`
Add the CUSTOM_ELEMENT_SCHEMA to the NgModule:
`@NgModule({
  ...
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  ..
})`
