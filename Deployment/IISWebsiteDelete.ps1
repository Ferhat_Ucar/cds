
param
(
    [string]$webSiteName
)

Import-Module WebAdministration -ErrorAction SilentlyContinue

if (Test-Path "IIS:\Sites\$($webSiteName)")
{
    Write-Host "Website '$($webSiteName)' exists - removing it..."
    Remove-WebSite $webSiteName
}
else
{
    Write-Host "Website '$($webSiteName)' doesn't exist."
}