param
(
    [string]$webSiteUrl
)

# Explicitly set the security protocol to TLS1.2.
# This is required since Invoke-WebRequest uses SSL3 by default.
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$endpoints = @()
$endpoints += ""

foreach ($endpoint in $endpoints) {
    $url = "$($webSiteUrl)/$($endpoint)"
    Write-Host "Heartbeat test for endpoint: $($url)" -ForegroundColor Cyan
    $result = Invoke-WebRequest "$($url)" -UseBasicParsing
}