
param
(
    [string]$appPoolName
)

Import-Module WebAdministration -ErrorAction SilentlyContinue

if (Test-Path "IIS:\AppPools\$($appPoolName)")
{
    Write-Host "Application pool '$($appPoolName)' exists - removing it..."
    Remove-WebAppPool $appPoolName
}
else
{
    Write-Host "Application pool '$($appPoolName)' doesn't exist."
}