const path = require('path');


// ---


module.exports = {
    entry: {
        components: './src/npm-output.js'
    },
    output: {
        library: 'calvi-design-system',
        libraryTarget: 'umd',
        umdNamedDefine: true,
        path: path.resolve(__dirname, '..', 'npm-output'),
        filename: 'calvi-design-system.js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre",
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: [ ".tsx", ".ts", ".js" ]
    }
};
