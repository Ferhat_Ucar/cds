const path = require('path');
const FileManagerPlugin = require('filemanager-webpack-plugin');


// ---


module.exports = {
    entry: {
        components: './src/index.ts'
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre",
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: [ ".tsx", ".ts", ".js" ]
    },
    devServer: {
        writeToDisk: true,
        contentBase: path.resolve(__dirname, 'dist'),
        compress: true,
        port: 9000,
        https: false,
        open: false
    },
    plugins: [
        new FileManagerPlugin({
            onStart: {
                copy: [
                    {
                        source: './dist/components.js',
                        destination: '../calvi-design-system/src/assets/components.js'
                    }
                ]
            },
            onEnd: {
                copy: [
                    {
                        source: './src/components/**/*.md',
                        destination: '../calvi-design-system/src/assets/readme/'
                    }
                ]
            }
        })
    ]
};
