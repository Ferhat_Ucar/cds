// fetch.file

export const fetchFile = (path: string) => {
    return fetch(path)
            .then(response => response.text())
            .then(text => text)
            .catch(console.error.bind(console));
}
