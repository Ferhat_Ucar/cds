// fetch.json

export const fetchJSON = (path: string) => {
    return fetch(path)
            .then(response => response.json())
            .then(json => json)
            .catch(console.error.bind(console));
}
