// button.ts
import { camelCaseToHyphen } from '../../factories/format.camelcase'
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { buttonController } from './button.controller'

export class CButton extends HTMLElement {
    private get checkAttributes() {
        return {
            html: [
                [
                    'button-primary-icon',
                    'hidden-label'
                ],
                [
                    'button-secondary-icon',
                    'hidden-label'
                ],
                [
                    'button-ghost-icon',
                    'hidden-label'
                ]
            ],
            is: [
                'button-primary',
                'button-primary-icon',
                'button-secondary',
                'button-secondary-icon',
                'button-ghost',
                'button-ghost-icon'
            ],
            style: 'not-allowed',
            task: 'not-empty'
        };
    }

    // get the button element
    private get button() {
        return this.querySelector('button') || false;
    }

    // get the button controller
    private controller() {
        buttonController(this.button);
    }

    // render the button component
    private render() {
        const html: string = this.innerHTML.trim();
        let dataset: string = '';

        const id: string = (this.hasAttribute('id'))
            ? `id="c-${this.getAttribute('id')}"`
            : '';

        const disabled: string = (this.hasAttribute('disabled'))
            ? 'disabled'
            : '';

        const icon: string = (html.indexOf('<i') < 0)
            ? ''
            : (html.indexOf('<i') === 0)
                ? 'has-icon-left-side'
                : 'has-icon-right-side';

        // when we want to add a custom data attribute
        const domStringMap: DOMStringMap = this.dataset || {};

        for (const [data, value] of Object.entries(domStringMap)) {
            dataset += `data-${camelCaseToHyphen(data)}="${value}" `;
        }

        return `
            <button
                ${id}
                name="${this.getAttribute('name') || ''}"
                type="button"
                ${disabled}
                role="${this.getAttribute('role') || 'button'}"
                class="c-button is-${this.getAttribute('is') || 'button-primary'} ${this.getAttribute('css') || ''} ${icon}"
                data-task="${this.getAttribute('task') || ''}"
                ${dataset}
                >${this.innerHTML.trim() || 'No label set for this button'}
            </button>
        `
    }

    // check when attribute 'disabled' is active, changed or updated
    // if so, fire the attributeChangedCallback function below
    static get observedAttributes() {
        return [
            'disabled'
        ]
    }

    // takes 3 parameters
    // 1: the name of the attribute (disabled)
    // 2: the current value
    // 3: the new value
    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const button: any = this.button;
        const val = newValue !== null;

        if (button) {
            switch(attribute) {
                case 'disabled':
                    if (button.disabled !== val) button.disabled = val;
                    button.style.width = '200px';
                    break;

                default:
                    return;
            }
        }
    }

    // when the element is added to the DOM
    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        // render component
                        webcomponent(
                            this,
                            this.render(),
                            'custom-append',
                            this.checkAttributes
                        );

                        // render controller
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }
}
