// button-controller.ts
import { broadcast } from '../../factories/event.broadcast'

export const buttonController: Function = ((button: HTMLButtonElement) => {
    const events: Function = () => {
        if (button) {
            const broadcaster: any = (event: any) => {
                broadcast(button, 'c-button', event, {
                    task: () => button.getAttribute('data-task')
                });

                button.style.backgroundColor = 'darkred';
                button.textContent = 'Clicked button';
            };

            // event: handlers
            button.addEventListener('click', broadcaster, true);
            button.addEventListener('keyup', broadcaster, true);
        }
    }

    events();
});
