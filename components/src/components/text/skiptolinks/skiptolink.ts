// skiptolink.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent';


// ---


export class CSkipToLink extends HTMLElement {


    private get checkAttributes() {
        return {
            link: 'not-empty',
            style: 'not-allowed',
            id: 'not-allowed'
        };
    }


    // ---

    private render() {
        return `<a href="${this.getAttribute('link') || '#'}" class="c-skip-to-link ${this.getAttribute('css')}">${this.innerHTML.trim()}</a>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                    }
                });
        }
    }



    // ---



    disconnectedCallback() {}
}
