#### Usage

The skip to navigation component is activated by tab, when you want to skip to a specific content of the page

| Option        | Value                                 | Description                               | Required  |
| ------------- |:-------------:                        | ------------:                             |------------:|
| **link**      | any                                   | sets the path                             | * |
