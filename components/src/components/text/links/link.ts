// text linkts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'


// ---


export class CLink extends HTMLElement {


    private get checkAttributes() {
        return {
            link: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<a href="${this.getAttribute('link') || '#'}" class="c-link ${this.getAttribute('css')}" target="_self">${this.innerHTML.trim()}</a>`;
    }

    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                    }
                });
        }
    }
}
