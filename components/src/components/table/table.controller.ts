// table.controller.ts
import { broadcast } from '../../factories/event.broadcast';


export const tableController: Function = ((component: any) => {


    const table = component.table;
    const head = component.head;
    const body = component.body;


    // ---


    const events: Function = () => {
        if (head && body) {
            const sortButtons: HTMLButtonElement[] = Array.from(head.querySelectorAll('button'));


            // buttons
            component.addEventListener('c-button', (event: any) => {

                // sorting
                if (event.detail.type() === 'click' && event.detail.task() === 'table-sort') {
                    const button: any = event.detail.target();

                    if (button.dataset.sortSortable === "true") {
                        const direction: string = button.dataset.sortDirection ? (button.dataset.sortDirection === "ascending") ? "descending" : "ascending" : "ascending";
                        sortButtons.forEach((button: HTMLButtonElement) => button.removeAttribute('data-sort-direction'));
                        button.dataset.sortDirection =  direction;
                        broadcast(component, 'c-table', null, {
                            sortField: () => button.dataset.sortField,
                            sortDirection: () => button.dataset.sortDirection
                        });
                    }
                }


                // advanced search
                if (event.detail.type() === 'click' && event.detail.task() === 'advanced-search') {
                    const filter: any = component.querySelector('.c-table-row.advanced-search') || false;
                    const items: Element[] = (filter) ? Array.from(filter.querySelectorAll('.c-input-text')) : [];
                    if (filter) {
                        if (filter.hasAttribute('visible')) {
                            filter.removeAttribute('visible');
                            items.forEach((item: any) => item.setAttribute('tabindex', '-1'));
                        } else {
                            filter.setAttribute('visible', '');
                            items.forEach((item: any) => item.removeAttribute('tabindex'));
                        }
                    }
                }
            });


            // ----


            // checkboxes
            component.addEventListener('c-checkbox', (event: any) => {

                // toggle column
                if (event.detail.task().indexOf('table-column-toggle') > -1) {
                    const classname: string = event.detail.task().replace('table-column-toggle:', 'hide-column-');
                    if (table) {
                        (event.detail.checked() === false)
                            ? table.classList.add(classname)
                            : table.classList.remove(classname);
                    }
                }


                // select / deselect row
                if (event.detail.task() === 'table-select-row') {
                    broadcast(component, 'c-table', null, {
                        selectRow: () => event.detail.checked()
                    });
                }


                // select / deselect all
                if (event.detail.task() === 'table-select-all') {
                    const items: Element[] = Array.from(body.querySelectorAll('c-checkbox'));
                    items.forEach((item: any) => (event.detail.checked() === true) ? item.setAttribute('checked', '') : item.removeAttribute('checked'));
                    broadcast(component, 'c-table', null, {
                        selectAllRows: () => event.detail.checked()
                    });
                }
            });
        }
    }


    // ---


    const init: Function = () => {
        setTimeout(() => { events() }, 100);
    }


    // ---


    init();
});
