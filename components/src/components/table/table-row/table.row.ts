// table.row.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'



// ---



export class CTableRow extends HTMLElement {

    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            optional: [
                'td.c-table-cell',
                'th.c-table-header'
            ]
        };
    }


    // ---


    private render() {
        return `<tr class="c-table-row ${this.getAttribute('css') || ''} ">${this.innerHTML.trim()}</tr>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes, this.checkComponents));
    }
}
