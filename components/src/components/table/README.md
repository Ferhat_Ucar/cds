The table component can be used to represent data.

## Table

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | x                                     | The css attribute is not allowed          | x |
| **style**         | x                                     | The style attribute is not allowed        | x |


---------------------------


## Table-head (thead)

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | x                                     | The css attribute is not allowed          | x |
| **style**         | x                                     | The style attribute is not allowed        | x |


---------------------------


## Table-header (th)

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | string                                | The css attribute can be uses to add an extra style class on the element  |  |
| **style**         | x                                     | The style attribute is not allowed | x |


---------------------------


## Table-body (tbody)

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | x                                     | The css attribute is not allowed          | x |
| **style**         | x                                     | The style attribute is not allowed        | x |



---------------------------

## Table-row (tr)

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | string                                | The css attribute can be uses to add an extra style class on the element  |  |
| **style**         | x                                     | The style attribute is not allowed        | x |


---------------------------

## Table-cell (td)

#### Attributes
| Attribute         | Value                                 | Description                               | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | string                                | The css attribute can be uses to add an extra style class on the element  |  |
| **style**         | x                                     | The style attribute is not allowed        | x |
