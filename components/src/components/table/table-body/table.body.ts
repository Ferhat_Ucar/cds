// table.body.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'


// ---


export class CTableBody extends HTMLElement {


    private get checkAttributes() {
        return {
            css: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: ['tr.c-table-row']
        };
    }


    // ---


    private render() {
        return `<tbody class="c-table-body">${this.innerHTML.trim()}</tbody>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes, this.checkComponents));
    }
}
