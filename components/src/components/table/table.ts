// table.ts
import { ready } from '../../factories/webcomponent.ready'
import { tableController } from './table.controller'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CTable extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed',
            task: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: ['.c-table-body'],
            optional: ['.c-table-head']
        };
    }


    // ---


    private get table() {
        return this.querySelector('table.c-table') || false;
    }


    // ---


    private get head() {
        return this.querySelector('.c-table-head') || false;
    }


    // ---


    private get body() {
        return this.querySelector('.c-table-body') || false;
    }


    // ---


    private get selectAll() {
        return this.querySelector('checkbox[data-task="table-select-all"]') || false;
    }



    // ---


    private render() {
        return `<table class="c-table ${this.getAttribute('css') || ''}">${this.innerHTML.trim()}</table>`;
    }


    // ---


    private controller() {
        return tableController(this);
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                    this.controller();
                }
            });
        } else {
            this.controller();
        }
    }
}
