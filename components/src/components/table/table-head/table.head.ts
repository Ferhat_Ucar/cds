// table.head.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'



// ---




export class CTableHead extends HTMLElement {


    private get checkAttributes() {
        return {
            css: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: ['tr.c-table-row']
        };
    }


    // ---


    private render() {
        return `<thead class="c-table-head">${this.innerHTML.trim()}</thead>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes, this.checkComponents));
    }
}
