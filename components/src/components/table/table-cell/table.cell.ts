// table.cell.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'


// ---


export class CTableCell extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<td class="c-table-cell ${this.getAttribute('css') || ''}">${this.innerHTML.trim()}</td>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes));
    }
}
