The table component uses 2 internal functions for view manipulation.

## advanced-search

The table can contain a button with the task: 'advanced-search' and a table row with the css: 'advanced search'.
When both are present in the DOM, the table component will listen to the button event.
A click/touch will toggle the visibility of the advanced search bar. When advanced search is hidden, the input fields get a tab-index of -1;

<c-button is="button-ghost-icon" task="advanced-search" id="c-table-button-filter-table"><c-icon is="icon-search"></c-icon><span class="hidden-label">Filter</span></c-button>


| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | button[task='advanced search']             | Toggles the advanced search (if present) |    |
| **touch**                 | button[task='advanced search']             | Toggles the advanced search (if present) |    |
| **key:Spacebar**          | button[task='advanced search']             | Toggles the advanced search (if present) |    |
| **key:Tab**               | button[task='advanced search']             | Adds focus state on the button |    |

---------------------------


## Toggle column

The table can contain a context-menu with switches that toggle the visibility of a column.
Each switch should contain the task: 'table-column-toggle:Index'. The index represents a numeric value index of the table-column. e.g.: task="table-column-toggle:1"

<c-button is="button-ghost-icon" task="context-menu-toggle"><c-icon is="icon-enable-columns"></c-icon><span class="hidden-label">Toggle table columns</span></c-button>

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | switch[task='table-column-toggle:1']       | Toggles visibility of the first column |    |
| **touch**                 | switch[task='table-column-toggle:1']       | Toggles visibility of the first column |    |
| **key:Spacebar**          | switch[task='table-column-toggle:1']       | Toggles visibility of the first column |    |


---------------------------
