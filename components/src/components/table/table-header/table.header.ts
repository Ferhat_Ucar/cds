// table.header.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'


// ---


export class CTableHeader extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<th class="c-table-header ${this.getAttribute('css') || ''}">${this.innerHTML.trim()}</th>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes));
    }
}
