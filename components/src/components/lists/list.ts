// lists.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CList extends HTMLElement {


    private get checkAttributes() {
        return {
            is: [
                'context',
                'quicklink',
                'ordered',
                'unordered'
            ],
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: ['.c-list-item'],
            optional: ['hr']
        };
    }


    // ---


    private render() {
        // optional attributes
        const css: string = (this.hasAttribute('css')) ? this.getAttribute('css') || '' : '';
        const id: string = (this.hasAttribute('id')) ? `id="${this.getAttribute('id') || ''}` : '';
        const is: string = (this.hasAttribute('is')) ? `is-${this.getAttribute('is')}` : '';
        const label: string = (this.hasAttribute('label')) ? `data-label="${this.getAttribute('label') || ''}` : '';
        const node: string = (this.getAttribute('is') === 'ordered') ? 'ol' : 'ul';
        const start: string = (this.hasAttribute('start')) ? `start="${this.getAttribute('start') || ''}` : '';
        const type: string = (this.hasAttribute('type')) ? `type="${this.getAttribute('type') || ''}` : '';

        return `
            <${node} class="c-list ${is} ${css}" ${type} ${id} ${start} ${label}>
                ${this.innerHTML.trim()}
            </${node}>
        `;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-replace', this.checkAttributes, this.checkComponents);
                }
            });
    }
}
