```html
<c-list is="ordered">
    <c-list-item>First item</c-list-item>
    <c-list-item>Second item</c-list-item>
    <c-list-item>
        Third item
        <c-list is="ordered">
            <c-list-item>First subitem</c-list-item>
            <c-list-item>Second subitem</c-list-item>
        </c-list>
    </c-list-item>
    <c-list-item>Fourth item</c-list-item>
</c-list>
```
