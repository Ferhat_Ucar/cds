// list-item.ts
import { ready } from '../../factories/webcomponent.ready';
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CListItem extends HTMLElement {

    private get checkAttributes() {
        return {
            id: 'not-allowed',
            css: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<li class="c-list-item" role="option">${this.innerHTML.trim()}</li>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                }
            });
    }
}
