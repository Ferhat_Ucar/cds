// breadcrumbs.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CBreadcrumb extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---

    private get crumbs() {
        const parent: any = this.parentNode || false;
        return (parent) ? parent.querySelectorAll('c-breadcrumb') : [];
    }


    // ---


    private render() {
        const crumbs: Element[] = Array.from(this.crumbs);

        const crumb: string = (crumbs.indexOf(this) !== (this.crumbs.length - 1))
            ? `<a href="${this.getAttribute('link') || ''}" class="c-breadcrumb">${this.innerHTML.trim() || 'No label set for this breadcrumb'}</a>`
            : `<span class="c-breadcrumb" aria-current="page">${this.innerHTML.trim() || 'No label set for this breadcrumb'}</span>`;

        const separator: string = (crumbs.indexOf(this) === 0)
            ? '<c-icon is="icon-home" label=""></c-icon>'
            : '<c-icon is="icon-breadcrumb" label=""></c-icon>';

        return `${separator}${crumb}`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                if (result) webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
            });
        }
    }
}
