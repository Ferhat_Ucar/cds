// breadcrumbs.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CBreadcrumbs extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private get crumbs() {
        return this.querySelectorAll('c-breadcrumb');
    }


    // ---


    private render() {
        const crumbs: Element[] = Array.from(this.crumbs);
        let html: string = '';

        crumbs.forEach((crumb: any) => {
            html += `<li>${crumb.outerHTML}</li>`;
        })

        return `
            <nav class="c-breadcrumbs ${this.getAttribute('css') || ''} aria-label="Breadcrumb">
                <ul>${html.trim()}</ul>
            </nav>
        `;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                }
            });
    }
}
