// tile.controller

export const tileController: Function = ((component: any) => {

    const tile: any = component.tile;
    const draghandle: any = component.draghandle;


    const events: Function = () => {
        if (draghandle !== null) {
            draghandle.onmousedown = () => {
                tile.draggable = true;
            }


            draghandle.onclick = (event: MouseEvent) => {
                event.preventDefault();
                event.stopPropagation();
                return false;
            }


            draghandle.ondragstart = (event: any) => {
                event.dataTransfer.setData("text", tile.id);
                event.dataTransfer.effectAllowed = "copy";
            }


            tile.ondrag = () => {
                tile.classList.add('is-dragging');
            }


            tile.ondragend = () => {
                tile.draggable = false;
                tile.classList.remove('is-dragging');
            }
        }
    }


    // ---


    events();
});
