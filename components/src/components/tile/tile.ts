// tile.ts
import { ready } from '../../factories/webcomponent.ready';
import { webcomponent } from '../../factories/webcomponent';
import { tileController } from './tile.controller';



// ---



export class CTile extends HTMLElement {


    private get checkAttributes() {
        return {
            disabled: 'not-allowed',
            href: 'not-empty',
            id: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'h3',
                '.c-tile-icon'
            ]
        };
    }


    // ---


    private get tile() {
        return this.querySelector('a.c-tile') || false;
    }


    // ---


    private get draghandle() {
        return this.querySelector('.c-tile_draghandle') || false;
    }


    // ---



    private render() {
        // atributes
        const id: string = `c-${this.getAttribute('id')}` || '';
        const draghandle: string = (this.hasAttribute('draggable')) ? `<span class="c-tile_draghandle"><c-icon is="icon-drag-and-drop"></c-icon></span>` : ``;
        const tooltip: string = (this.hasAttribute('tooltip')) ? `<c-tooltip id="${id}-tooltip">${this.getAttribute('tooltip')}</c-tooltip>` : '';

        return `
            <a
                id="${id}"
                class="c-tile ${this.getAttribute('css') || ''}"
                href="${this.getAttribute('href') || ''}"
                ${(this.hasAttribute('draggable')) ? 'draggable' : ''}"
                >
                    <span class="c-tile_menu">
                        ${tooltip}
                        ${draghandle}
                    </span>
                    ${this.innerHTML.trim() || 'no label set for this tile'}
                    <span class="c-tile_description">${this.getAttribute('description') || ''}</span>
                </a>
        `;
    }


    // ---


    private controller() {
        tileController(this);
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }



    // ---


    disconnectedCallback() {}
}
