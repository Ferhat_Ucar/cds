```html
    <c-tile-icon is="adaptiveportalconfiguration" label="Tile label"></c-tile-icon>
    <c-tile-icon is="advancedreporting" label="Tile label"></c-tile-icon>
    <c-tile-icon is="asset" label="Tile label"></c-tile-icon>
    <c-tile-icon is="assetOverview" label="Tile label"></c-tile-icon>
    <c-tile-icon is="assetsgrid_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="assetsgrid" label="Tile label"></c-tile-icon>
    <c-tile-icon is="assetspivot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="assetspivot" label="Tile label"></c-tile-icon>

    <c-tile-icon is="ban" label="Tile label"></c-tile-icon>
    <c-tile-icon is="budgetcontrol" label="Tile label"></c-tile-icon>

    <c-tile-icon is="callNowTransfer" label="Tile label">></c-tile-icon>
    <c-tile-icon is="cdrDownload" label="Tile label"></c-tile-icon>
    <c-tile-icon is="cdrgrid_custom" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="cdrgrid" label="Tile label"></c-tile-icon>
    <c-tile-icon is="cdrpivot_custom" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="cdrpivot" label="Tile label"></c-tile-icon>
    <c-tile-icon is="changeOverview" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="costcenter" label="Tile label"></c-tile-icon>
    <c-tile-icon is="costcentercosts" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="costcentergrid_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="costcentergrid" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="costcenterpivot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="costcenterpivot" label="Tile label"></c-tile-icon>

    <c-tile-icon is="discounts" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="DisputeManagementAdminTools" label="Tile label"></c-tile-icon>
    <c-tile-icon is="DisputeManagementMaintenance" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="downloadcenter" label="Tile label"></c-tile-icon>
    <c-tile-icon is="downloadtilesinvpdf" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="downloadtilesinvsource" label="Tile label"></c-tile-icon>

    <c-tile-icon is="fixedSubscription" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="FP" label="Tile label"></c-tile-icon>

    <c-tile-icon is="genericConfigManagement" label="Call Now Transfer"></c-tile-icon>

    <c-tile-icon is="help" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="hrupload" label="Tile label"></c-tile-icon> 

    <c-tile-icon is="internationalgrid_custom" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="internationalgrid" label="Tile label"></c-tile-icon>
    <c-tile-icon is="internationalpivot_custom" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="internationalpivot" label="Tile label"></c-tile-icon>

    <c-tile-icon is="manageAssetClass" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="manageBlacklist" label="Tile label"></c-tile-icon>
    <c-tile-icon is="managedownloadtypes" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="managepublishedprofilereports" label="Tile label"></c-tile-icon>
    <c-tile-icon is="managerawdataconfiguration" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="manageSpecifiedProfileImpersonation" label="Tile label"></c-tile-icon>
    <c-tile-icon is="manageWhiteList" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="ML" label="Tile label"></c-tile-icon>
    <c-tile-icon is="mobilegrid_custom" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="mobilegrid" label="Tile label"></c-tile-icon>
    <c-tile-icon is="mobilepayment" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="mobilepivot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="mobilepivot" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="mobileSubscription" label="Tile label"></c-tile-icon>
    <c-tile-icon is="mydownloads" label="Tile label"></c-tile-icon>

    <c-tile-icon is="locations" label="Tile label"></c-tile-icon>

    <c-tile-icon is="oneoff" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="orchestrationBillCycleConsolidatedView" label="Tile label"></c-tile-icon>
    <c-tile-icon is="orchestrationBillCycleOverview" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="orchestrationCustomerFeedOverview" label="Tile label"></c-tile-icon>
    <c-tile-icon is="otherCosts" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="otherSubscription" label="Tile label"></c-tile-icon>
    <c-tile-icon is="outputmanagement" label="Tile label"></c-tile-icon>

    <c-tile-icon is="Package-Basic" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="Package-Extreme" label="Tile label"></c-tile-icon>
    <c-tile-icon is="Package-Professional" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="Package-Ultimate" label="Tile label"></c-tile-icon>
    <c-tile-icon is="packages" label="Call Now Transfer"></c-tile-icon>
    <c-tile-icon is="pe" label="Tile label"></c-tile-icon>
    <c-tile-icon is="person" label="Tile label"></c-tile-icon>
    <c-tile-icon is="persongrid_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="persongrid" label="Tile label"></c-tile-icon>
    <c-tile-icon is="personpivot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="personpivot" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="personpivot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="piegrid" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="plustile" label="Tile label"></c-tile-icon>
    <c-tile-icon is="pievot" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="pievot_custom" label="Tile label"></c-tile-icon>
    <c-tile-icon is="productgrid_custom" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="productgrid" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="profilemanagement" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionadvancedreporting" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionbudgetcontrol" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotioncostcenter" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotiondownloadcenter" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionhrupload" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionoutputmanagement" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionperson" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionrawdataexportinvoiceoverview" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="promotionuser" label="Tile label"></c-tile-icon>  

    <c-tile-icon is="rawdataexportinvoiceinsight" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="rawdataexportinvoiceoverview" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="recurring" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="reportTemplates" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="ReportManager" label="Tile label"></c-tile-icon>  

    <c-tile-icon is="servicegrid_custom" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="servicegrid" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="serviceNumber" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="servicepivot_custom" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="servicepivot" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="splitbilling" label="Tile label"></c-tile-icon> 

    <c-tile-icon is="topCallingAbroad" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="topCallingWhileAbroad" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="topDataNational" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="topRoamingData" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="topServiceNumbers" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="topSMS" label="Tile label"></c-tile-icon> 
    <c-tile-icon is="topTotalCosts" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="TE" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="tilemanagement" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="totalcostdistribution" label="Tile label"></c-tile-icon>  

    <c-tile-icon is="quickcheck" label="Tile label"></c-tile-icon>  

    <c-tile-icon is="usage" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="usageInternational" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="user" label="Tile label"></c-tile-icon>  

    <c-tile-icon is="watches" label="Tile label"></c-tile-icon>  
    <c-tile-icon is="watchesAdminTools" label="Tile label"></c-tile-icon>  
```
