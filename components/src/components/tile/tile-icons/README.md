
#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **is**            | string            | Sets the tile-icon | * |
| **label**         | string            | Sets the inner title of the svg-element     | * |

