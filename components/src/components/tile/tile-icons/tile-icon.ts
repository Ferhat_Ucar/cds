// tile-icons.ts
import { fetchFile } from '../../../services/fetch.file'
import { webcomponent } from '../../../factories/webcomponent'
import { icons } from '../../../resources/tile-icons'
import { cleansvg } from '../../../factories/svg.clean'


// ---


export class CTileicon extends HTMLElement {


    private get checkAttributes() {
        return {
            css: 'not-allowed',
            label: 'not-empty',
            style: 'not-allowed'
        };
    }

    private render(icon: string, found: boolean) {
        const emoji = (found === true) ? '' : 'display-crying-emoji';
        icon = cleansvg(icon, this.getAttribute('label') || '');

        return `
            <i class="c-tile-icon ${this.getAttribute('is') || 'icon-add'} ${emoji} ${this.getAttribute('css') || ''}">${icon.trim()}</i>
        `;
    }


    // ---


    connectedCallback() {
        const icon: string = this.getAttribute('is') || '';
        const theme: string = sessionStorage.getItem(`cds:theme`) || 'calvi';
        const path: string = `/assets/${theme}/assets/tile-icons/`;
        const session: any = sessionStorage.getItem(`cds:${theme}:tile-icon:${icons[icon]}`) || false;

        // fetch: icon file
        if (icons[icon]) {
            if (session) {
               webcomponent(this, this.render(session, true), 'custom-replace', this.checkAttributes);
            } else {
                fetchFile(path + icons[icon]).then((response: any) => {
                    if (response.indexOf('<svg') > -1)  {
                        sessionStorage.setItem(`cds:${theme}:tile-icon:${icons[icon]}`, response);
                        webcomponent(this, this.render(response, true), 'custom-replace', this.checkAttributes);
                    } else {
                        webcomponent(this, this.render('', false), 'custom-replace', this.checkAttributes);
                    }
                });
            }
        } else {
            webcomponent(this, this.render('', false), 'custom-replace', this.checkAttributes);
        }
    }
}
