```html
<c-tile
    draggable
    description="&euro; 14.374,96"
    href="/to-a-page"
    id="tile-id-01"
    icon="/path-to-svg"
    tooltip="Hey, I have some extra information">
        Very long title for a feature with support for multiple lines of text
</c-tile>
```
