// divider.ts
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CDivider extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<hr class="c-divider ${this.getAttribute('css') || ''}"></hr>`;
    }


    // ---


    connectedCallback() {
        webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
    }
}
