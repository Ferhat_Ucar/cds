// context-menu.controller.ts
export const contextMenuController: Function = ((component: any) => {


    let selected : number = 0;
    const button = component.button;
    const list = component.list;
    const listitems = component.listitems;


    // ---


    const reset = () => {
        const expands: Array<HTMLElement> = Array.from(document.querySelectorAll('[aria-expanded="true"]'));
        expands.forEach((expanded: any) => {
            expanded.setAttribute('aria-expanded', 'false');
        });
    }


    // ---


    const outsideClick = (event: MouseEvent) => {
        const target: any = event.target;
        if (target.closest('.c-list') === null) {
            button.setAttribute('aria-expanded', 'false');
            button.focus();
            document.removeEventListener("mouseup", outsideClick, false);
        }
    }


    // ---


    const select = {
        first : () => {
            selected = 0;
            let item: any = listitems[selected].firstElementChild;
            if (item.nodeName.indexOf('C-') > -1) item = item.children[0];
            item.focus();
        },
        last : () => {
            selected = listitems.length - 1;
            let item: any = listitems[selected].firstElementChild;
            if (item.nodeName.indexOf('C-') > -1) item = item.children[0];
            item.focus();
        },
        next : () => {
            selected = ((selected + 1) < listitems.length) ? (selected + 1) : 0;
            let item: any = listitems[selected].firstElementChild;
            if (item.nodeName.indexOf('C-') > -1) item = item.children[0];
            item.focus();
        },
        previous : () => {
            selected = ((selected - 1) > -1) ? (selected - 1) : (listitems.length - 1);
            let item: any = listitems[selected].firstElementChild;
            if (item.nodeName.indexOf('C-') > -1) item = item.children[0];
            item.focus();
        }
    }


    // ---


    const toggle = () => {
        if (button.getAttribute('aria-expanded') === 'false') {
            reset();
            button.setAttribute('aria-expanded', 'true');
            document.addEventListener("mouseup", outsideClick)
        } else {
            button.setAttribute('aria-expanded', 'false');
            document.removeEventListener("mouseup", outsideClick, false);
        }
    };


    // ---


    const events: Function = () => {

        // button events
        button.onclick = () => {
            toggle();
            select.first();
        }
        button.onkeydown = (event: KeyboardEvent) => {
            if (event.key === 'Spacebar' || event.key === 'ArrowDown' || event.key === 'Enter') {
                event.preventDefault();
                toggle();
                select.first();
            }
            if (event.key === 'ArrowUp') {
                event.preventDefault();
                toggle();
                select.last();
            }
            if (event.key === "Escape") {
                button.setAttribute('aria-expanded', 'false');
            }
        }

        // list events
        list.onkeydown = (event: KeyboardEvent) => {
            if (event.key === 'Tab') {
                toggle();
            }
            if (event.key === 'ArrowDown') {
                event.preventDefault();
                select.next();
            }
            if (event.key === 'ArrowUp') {
                event.preventDefault();
                select.previous();
            }
            if (event.key === 'Home') {
                event.preventDefault();
                select.first();
            }
            if (event.key === 'End') {
                event.preventDefault();
                select.last();
            }
            if (event.key === "Escape") {
                button.setAttribute('aria-expanded', 'false');
                button.focus();
            }
        };
        list.blur = () => {}
    };


    // ---


    const init: Function = () => {
        if (button && list && listitems) events();
    }


    // ---


    init();
});
