// context-menu.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { contextMenuController } from './context-menu.controller';




// ---



export class CContextMenu extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="context-menu-toggle"]',
                '.c-list'
            ]
        };
    }


    // ---


    private get button() {
        return this.querySelector('.c-button') || false;
    }


    // ---


    private get list() {
        return this.querySelector('.c-list') || false;
    }


    // ---


    private get listitems() {
        return Array.from(this.querySelectorAll('.c-list-item')) || []
    }


    // ---



    private render() {
        const button: any = this.button;
        const list: any = this.list;
        const listitems: any = this.listitems;

        // set attributes
        button.setAttribute('aria-expanded', 'false');
        button.setAttribute('aria-controls', <string>this.getAttribute('id'));
        button.setAttribute('aria-haspopup', 'true');
        button.setAttribute('task', 'context-menu-toggle');
        list.setAttribute('id', <string>this.getAttribute('id'));
        list.setAttribute('role', 'menu');
        list.setAttribute('tabindex', '-1');

        // set: list item attributes
        listitems.forEach((item: any) => {
            item.setAttribute('role', 'none');
            if (item.firstElementChild !== null) {
                item.firstElementChild.setAttribute('tabindex', '-1');
                item.firstElementChild.setAttribute('role', 'menuitem');
            }
        });

        return `<div class="c-context-menu">
            ${button.outerHTML.trim()}
            ${list.outerHTML.trim()}
        </div>`;
    }



    // ---



    private controller() {
        contextMenuController(this);
    }



    // ---



    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }
}
