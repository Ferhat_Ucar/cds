// logo.ts
import { fetchFile } from '../../services/fetch.file'
import { webcomponent } from '../../factories/webcomponent'
import { cleansvg } from '../../factories/svg.clean'


// ---


export class CLogo extends HTMLElement {

    private get checkAttributes() {
        return {
            link: 'not-empty',
            label: 'not-empty',
            style: 'not-allowed',
            css: 'not-allowed'
        };
    }


    private render(logo: string, found: boolean) {
        logo = cleansvg(logo, this.getAttribute('label') || '');

        return `
            <a class="c-logo" href="${this.getAttribute('link') || ''}" target="_self">
                <span class="hidden-label">${this.getAttribute('label') || ''}</span>
                ${logo.trim()}
            </a>
        `;
    }


    // ---


    connectedCallback() {
        const theme: string = sessionStorage.getItem('cds:theme') || 'calvi';
        const session: any = sessionStorage.getItem(`cds:${theme}:logo`) || false;
        const path: string = `/assets/${theme}/assets/svg/brand.svg`;

        if (session) {
           webcomponent(this, this.render(session, true), 'custom-replace', this.checkAttributes);
        } else {
           fetchFile(path).then((response: any) => {
               if (response.indexOf('<svg') > -1)  {
                   sessionStorage.setItem(`cds:${theme}:logo`, response);
                   webcomponent(this, this.render(response, true), 'custom-replace');
               } else {
                   webcomponent(this, this.render('', false), 'custom-replace');
               }
           });
       }
    }
}
