// accordion-button.ts
import { webcomponent } from '../../factories/webcomponent';
import { ready } from '../../factories/webcomponent.ready'


// ---


export class CAccordionButton extends HTMLElement {

    // vars
    private get checkAttributes() {
        return {
            id: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <h3>
                <button class="c-accordion-button ${this.getAttribute('css') || ''}" aria-expanded="false">
                    ${this.innerHTML.trim()}
                </button>
            </h3>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                });
        }
    }
}
