// accordion.controller.ts
import { broadcast } from '../../factories/event.broadcast';


export const accordionController = ((component: any) => {

    const accordion: any = component.accordion;
    const button: any = component.button;

    // ---


    const events = () => {
        if (button) {
            button.onclick = () => component.hasAttribute('expanded')
                ? component.removeAttribute('expanded')
                : component.setAttribute(
                    'expanded',
                    ''
                );
            button.onfocus = () => accordion.classList.add('has-focus');
            button.onblur = () => accordion.classList.remove('has-focus');

            const broadcaster: any = (event: any) => broadcast(button, 'c-accordion-button', event);

            // event: handlers
            button.addEventListener('click', broadcaster, true);
            button.addEventListener('keyup', broadcaster, true);
        }
    };


    // ---


    events();
});
