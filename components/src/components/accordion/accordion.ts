// accordion.ts
import { webcomponent } from '../../factories/webcomponent'
import { accordionController } from './accordion.controller'
import { ready } from '../../factories/webcomponent.ready'


// ---


export class CAccordion extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed',
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'h3',
                '.c-accordion-region'
            ]
        };
    }


    // ---


    private get accordion(){
        return this.querySelector('.c-accordion') || false;
    }


    // ---


    private get button(){
        return this.querySelector('.c-accordion-button') || false;
    }


    // ---


    private get region(){
        return this.querySelector('.c-accordion-region') || false;
    }


    // ---


    private get expanded(){
        return this.hasAttribute('expanded') ? true : false;
    }


    // ---


    private controller() {
        accordionController(this);
    }


    // ---


    private render() {
        const button: any = this.button;
        const id: string = this.getAttribute('id') || '';
        const region: any = this.region;

        button.setAttribute('id', `${id}-button`);
        button.setAttribute('aria-controls', `${id}-region`);
        button.setAttribute('aria-expanded', this.expanded);

        region.setAttribute('id', `${id}-region`);
        region.setAttribute('aria-labelledby', `${id}-button`);
        if (this.expanded === false) {
            region.setAttribute('hidden', '');
        } else {
            region.removeAttribute('hidden');
        }


        return `
            <div class="c-accordion" id="${id}">
                ${button.outerHTML}
                ${region.outerHTML}
            </div>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    static get observedAttributes() {
        return [
            'expanded'
        ]
    }



    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const button: any = this.button;
        const region: any = this.region;


        if (button && region) {
            switch (attribute) {
                case 'expanded':
                    button.setAttribute('aria-expanded', this.expanded)
                    if (this.expanded === false) {
                        region.setAttribute('hidden', '');
                    } else {
                        region.removeAttribute('hidden');
                    }
                    break;

                default:
                    return;
            }
        }
    }
}
