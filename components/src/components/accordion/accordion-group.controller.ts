// accordion-group.controller.ts

export const accordionGroupController = ((component: any) => {

    const accordions: any = Array.from(component.accordions);
    const buttonExpand: any = component.querySelector('button[data-task="accordion-expand-all"]') || false;
    const buttonCollapse: any = component.querySelector('button[data-task="accordion-collapse-all"]') || false;


    // ---


    const focus: Function = (accordion: any) => {
        const button: any = accordion.querySelector('.c-accordion-button') || false;
        if (button) button.focus();
    };



    // ---


    const next: Function = (accordion: any) => {
        let current: number = accordions.indexOf(accordion) || 0;
        current = ((current + 1) > accordions.length) ? 0 : (current + 1);
        focus(accordions[current]);
    };


    // ---


    const previous: Function = (accordion: any) => {
        let current: number = accordions.indexOf(accordion) || 0;
        current = ((current - 1) > - 1) ? (current - 1) : (accordions.length - 1);
        focus(accordions[current]);
    };


    // ---


    const checkButtons: Function = () => {
        setTimeout(() => {
            let expanded: number = 0;
            accordions.forEach((accordion: any) => { if (accordion.hasAttribute('expanded')) expanded++; });

            if (expanded === 0) {
                if (buttonCollapse) buttonCollapse.setAttribute('disabled', '');
                if (buttonExpand) buttonExpand.removeAttribute('disabled');
            } else if (expanded === accordions.length) {
                if (buttonCollapse) buttonCollapse.removeAttribute('disabled');
                if (buttonExpand) buttonExpand.setAttribute('disabled', '');
            } else {
                if (buttonCollapse) buttonCollapse.removeAttribute('disabled');
                if (buttonExpand) buttonExpand.removeAttribute('disabled');
            }
        }, 100);
    };


    // ---


    const events = () => {



        // key events
        component.onkeydown = (event: KeyboardEvent) => {
            const element: any = document.activeElement;
            const accordion: any = (element.classList.contains('c-accordion-button')) ? element.parentNode.parentNode : false;

            switch (event.key) {
                case 'ArrowUp':
                    event.preventDefault();
                    if (accordion) previous(accordion);
                    break;
                case 'ArrowDown':
                    event.preventDefault();
                    if (accordion) next(accordion);
                    break;
                case 'End':
                    event.preventDefault();
                    if (accordion) focus(accordions[accordions.length - 1]);
                    break;
                case 'Home':
                    event.preventDefault();
                    if (accordion) focus(accordions[0]);
                    break;
                default:
                    return;
            }
        }


        component.addEventListener('c-button', (event: any) => {
            if (event.detail.task() === 'accordion-expand-all') {
                if (event.detail.type() === 'click') {
                    accordions.forEach((accordion: any) => accordion.setAttribute('expanded', ''));
                    checkButtons();
                }
            }
            if (event.detail.task() === 'accordion-collapse-all') {
                if (event.detail.type() === 'click') {
                    accordions.forEach((accordion: any) => accordion.removeAttribute('expanded'));
                    checkButtons();
                }
            }
        });


        component.addEventListener('c-accordion-button', (event: any) => {
            if (event.detail.type() === 'click') {
                const button: any = event.detail.target();
                const current: any = (button !== false) ? button.parentNode.parentNode : false;

                if (component.close === true) {
                    accordions.forEach((accordion: any) => {
                        if (accordion !== current) accordion.removeAttribute('expanded');
                    });
                }
                checkButtons();
            }
        });
    };


    // ---


    events();
    checkButtons();
});
