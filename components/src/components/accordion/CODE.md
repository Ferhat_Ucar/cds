```html
<c-accordion id="accordion-id-01">
    <c-accordion-button>
        <c-icon is="icon-arrow-right" css="circle-outline"></c-icon>
        Accordion item 1
    </c-accordion-button>
    <c-accordion-region>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit
        </p>
    </c-accordion-region>
</c-accordion>
```
