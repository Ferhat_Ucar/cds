// accordion-region.ts
import { webcomponent } from '../../factories/webcomponent'
import { ready } from '../../factories/webcomponent.ready'


// ---


export class CAccordionRegion extends HTMLElement {

    private get checkAttributes() {
        return {
            id: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <div class="c-accordion-region" role="region" hidden>
                ${this.innerHTML.trim()}
            </div>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                });
        }
    }
}
