// accordion-group.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { accordionGroupController } from './accordion-group.controller';


// ---


export class CAccordionGroup extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private get close() {
        return (this.hasAttribute('close-others')) ? true : false;
    }


    // ---


    private get accordions() {
        return this.querySelectorAll('c-accordion');
    }


    // ---


    private controller() {
        accordionGroupController(this);
    }


    // ---


    private render() {
        return `<div class="c-accordion-group">${this.innerHTML.trim()}</div>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }
}
