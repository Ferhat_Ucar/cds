```html
<c-chart is="spline-chart">
    <c-chart-item label="i am a label" value="14.345,55" percentage="43" month="februari" year="2019"></c-chart-item>
    <c-chart-item label="i am a label" value="14.345,55" percentage="37" month="march" year="2019"></c-chart-item>
    <c-chart-item label="i am a label" value="14.345,55" percentage="46" month="april" year="2019" selected></c-chart-item>
</c-chart>
```
