// c-chart-item.spline.ts

export const chartItemSpline: Function = ((data: any = {}) => {

    const id: string = 'chart-item-spline-' + Math.random().toString(36).replace(/[^a-z]+/g, '')


    // labels
    const label: string = `<figcaption>${data.month.charAt(0)}</figcaption>`;
    const yearLabel: string = (data.index !== 0)
        ? (data.items[data.index - 1].getAttribute('data-year') !== data.year)
            ? 'display-year-label'
            : ''
        : (data.index === 0)
            ? 'display-year-label'
            : '';


    // line calculation
    const start: number = (data.index !== 0) ? (data.percentage - data.items[data.index - 1].getAttribute('data-percentage')) : 0;
    const end: number = (data.index !== (data.items.length - 1)) ? (data.percentage - data.items[data.index + 1].getAttribute('percentage')) : 0;
    const startPoint: string = (start !== 0) ? `M 0,${100 - (data.percentage - (start / 2))}` : '';
    const currentPoint: string = (start === 0) ? `M 50,${100 - data.percentage}` : `L 50,${100 - data.percentage}`;
    const endPoint: string = (end !== 0) ? `L 100,${100 - (data.percentage - (end / 2))}` : '';


    // radius & stroke
    const radius: number = (data.items.length < 13) ? ((data.items.length < 7) ? 6 : 10) : 12;
    const stroke: number = (data.items.length < 13) ? ((data.items.length < 7) ? 2 : 4) : 5;


    // css
    const selected: string = (data.selected) ? 'is-selected' : '';


    // return object
    return `<li
                tabindex="0"
                class="c-chart-item is-${data.is} ${selected} ${yearLabel}"
                data-percentage="${data.percentage}"
                data-year="${data.year}"
                data-month="${data.month}"
                style="width: ${100 / data.items.length}%"
                role="figure"
                aria-labelledby="${id}"
            >
                <figure
                    aria-valuenow="${data.percentage}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                >
                    <svg width="100%" role="presentation" viewbox="0 0 100 100" preserveAspectRatio="xMaxYMid slice">
                        <title id="${id}">${data.label}: ${data.month}/${data.year} ${data.value} (${data.percentage}%)</title>
                        <path d="
                            ${startPoint}
                            ${currentPoint}
                            ${endPoint}
                        " stroke-width="${stroke}" />
                        <circle
                            class="${data.color}"
                            r="${radius}"
                            cx="50"
                            cy="${100 - data.percentage}"
                            stroke-width="${stroke}"
                         />
                    </svg>
                    ${label}
                </figure>
            </li>
    `;
});
