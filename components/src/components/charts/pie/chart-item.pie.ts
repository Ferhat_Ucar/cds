// c-chart-item.pie.ts

export const chartItemPie: Function = ((data: any = {}) => {

    const id: string = 'chart-item-bar-' + Math.random().toString(36).replace(/[^a-z]+/g, '');
    const labelRotation: number = data.rotate + ((data.percentage / 2) * 3.6);
    const label: string = (data.display === true)
            ? `<figcaption style="transform: rotate(${labelRotation}deg); animation-duration: ${data.duration}s; animation-delay: ${data.delay}s;">
                    <span style="transform: rotate(${0 - 1 - labelRotation + 1}deg);" class="${data.color}">${data.label}</span>
                </figcaption>`
            : '';

    // return object
    return `<li
                tabindex="0"
                class="c-chart-item is-${data.is} ${data.css}"
                data-percentage="${data.percentage}"
                role="figure"
                aria-labelledby="${id}"
            >
                <figure
                    aria-valuenow="${data.percentage}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                >
                    ${label}
                    <svg width="100%" height="100%" role="presentation" viewbox="0 0 100 100" style="transform: rotate(${data.rotate}deg)"">
                        <title id="${id}">${data.label}: ${data.value} (${data.percentage}%)</title>
                        <circle
                            class="${data.color}"
                            r="${data.radius}"
                            cx="50"
                            cy="50"
                            stroke-dasharray="${data.stroke} ${data.circumference}"
                            stroke-dashoffset=${data.stroke}
                            style="animation-duration: ${data.duration}s; animation-delay: ${data.delay}s;"
                         />
                    </svg>
                </figure>
            </li>
    `;
});
