// c-chart.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { broadcast } from '../../factories/event.broadcast'



// ---


export class CChart extends HTMLElement {


    private get checkAttributes() {
        return {
            is: [
                'bar-chart',
                'column-chart',
                'donut-chart',
                'pie-chart',
                'spline-chart',
                'stacked-bar-chart',
                'stacked-column-chart'
            ],
            style: 'not-allowed'
        };
    }


    // ---


    private get is() {
        return this.getAttribute('is') || 'bar-chart';
    }


    // ---


    private get css() {
        return this.getAttribute('css') || '';
    }



    // ---


    private controller() {
        const chartitems: any = this.querySelectorAll('li');

        const broadcaster: any = (event: any) => {
            const data: object = {
                index: Array.from(chartitems).indexOf(event.target),
                data: JSON.parse(JSON.stringify(event.target.dataset || {}))
            };

            broadcast(this, 'c-chart', event, {
                value: () => data
            });
        };

        chartitems.forEach((chartitem: HTMLLIElement) => {
            chartitem.addEventListener('focus', broadcaster);
        })
    }


    // ---


    private render() {
        return `<ul class="c-chart is-${this.is} ${this.css}" role="presentation">${this.innerHTML.trim()}</ul>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                }
            });
        } else {
            this.controller();
        }
    }
}
