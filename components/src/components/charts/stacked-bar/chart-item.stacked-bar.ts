// c-chart-item.stacked-bar.ts

export const chartItemStackedBar: Function = ((data: any = {}) => {

    const id: string = 'chart-item-stacked-bar-' + Math.random().toString(36).replace(/[^a-z]+/g, '');
    const label: string = (data.display === true) ? `<figcaption style="animation-duration: ${data.duration}s; animation-delay: ${data.delay}s;">${data.label}</figcaption>` : '';


    // return object
    return `<li
                tabindex="0"
                class="c-chart-item is-${data.is} ${data.css}"
                style="width:${data.percentage}%"
                data-percentage="${data.percentage}"
                role="figure"
                aria-labelledby="${id}"
            >
                <figure
                    aria-valuenow="${data.percentage}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                >
                    <svg width="100%" height="100%"  role="presentation">
                        <title id="${id}">${data.label}: ${data.value} (${data.percentage}%)</title>
                        <rect
                            x="0"
                            y="0"
                            width="100%"
                            height="100%"
                            class="${data.color}"
                            role="presentation"
                            style="animation-duration: ${data.duration}s; animation-delay: ${data.delay}s;"
                         />
                    </svg>
                    ${label}
                </figure>
            </li>
    `;
});
