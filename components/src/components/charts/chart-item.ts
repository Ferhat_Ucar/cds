// c-chart-item.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { chartItemBar } from './bar/chart-item.bar'
import { chartItemColumn } from './column/chart-item.column'
import { chartItemDonut } from './donut/chart-item.donut'
import { chartItemPie } from './pie/chart-item.pie'
import { chartItemSpline } from './spline/chart-item.spline'
import { chartItemStackedBar } from './stacked-bar/chart-item.stacked-bar'
import { chartItemStackedColumn } from './stacked-column/chart-item.stacked-column'


// ---


export class CChartItem extends HTMLElement {


    private get checkAttributes() {
        return {
            label: 'not-empty',
            percentage: 'not-empty',
            style: 'not-allowed',
            value: 'not-empty'
        };
    }


    // ---


    private get css() {
        return this.getAttribute('css') || '';
    }


    // ---


    private get color() {
        return this.getAttribute('color') || '';
    }


    // ---


    private get is() {
        const parent: any = this.parentNode || false;
        let is: string = 'bar';
        if (parent && parent.hasAttribute('is')) is = parent.getAttribute('is').replace('-chart', '');
        return is;
    }


    // ---


    private get label() {
        return this.getAttribute('label') || 'No label set for this item';
    }


    // ---


    private get month() {
        return this.getAttribute('month') || 'Month is not defined';
    }


    // ---


    private get displayLabel() {
        const parent: any = this.parentNode || false;
        let display: boolean = false;
        if (parent && parent.hasAttribute('display-labels')) display = true;
        return display;
    }


    // ---


    private get percentage() {
        return this.getAttribute('percentage') || '0';
    }


    // ---


    private get selected() {
        return this.hasAttribute('selected') ? true : false;
    }


    // ---


    private get value() {
        return this.getAttribute('value') || '0';
    }


    // ---


    private get year() {
        return this.getAttribute('year') || 'year is not defined';
    }


    // ---


    private get data () {
        let data: object = {}
        let start: number = 0;
        let total: number = 0;
        let i: number = 0;
        let percentage: number = parseInt(this.percentage);

        const label: string = this.label;
        const display: boolean = this.displayLabel;
        const value: string = this.value;
        const parent: any = this.parentNode || false;
        const items: any = (parent) ? Array.from(parent.children) : false;
        const index: number = (items) ? items.indexOf(this) : 0;
        const is: string = this.is;

        if (index !== 0 && items) {
            items.forEach(() => {
                const perc: number = parseInt(items[i].getAttribute('data-percentage')) || parseInt(items[i].getAttribute('percentage')) || 0;
                if (i < index) start += perc;
                total += perc;
                i++;
            })
        }


        // calculate
        if (isNaN(percentage) || percentage < 0 || percentage > 100) percentage = 0;
        const rotate: number = 0 - 91 + ((start / 100) * 360);
        const radius: number = 50;
        const circumference: number = Math.ceil(2 * Math.PI * radius);
        const stroke = Math.ceil((percentage / 100) * circumference);
        const duration: number = (1 * (percentage / 100));
        const delay: any = (0.4 + (1 * (start / 100))).toFixed(2);


        return data = {
            circumference: circumference,
            color: this.color,
            css: this.css,
            delay: delay,
            display: display,
            duration: duration,
            is: is,
            index: index,
            items: items,
            label: label,
            month: this.month,
            percentage: percentage,
            rotate: rotate,
            radius: radius,
            selected: this.selected,
            stroke: stroke,
            value: value,
            year: this.year
        }
    }


    // ---


    private render() {
        switch(this.is) {
            case 'bar':
                return chartItemBar(this.data);

            case 'column':
                return chartItemColumn(this.data);

            case 'donut':
                return chartItemDonut(this.data);

            case 'pie':
                return chartItemPie(this.data);

            case 'spline':
                return chartItemSpline(this.data);

            case 'stacked-bar':
                return chartItemStackedBar(this.data);

            case 'stacked-column':
                return chartItemStackedColumn(this.data);

            default:
                return;
        }
    }


    // ---


    connectedCallback() {
        ready(this, 'components').then((result: boolean) => {
            if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
        });
    }
}
