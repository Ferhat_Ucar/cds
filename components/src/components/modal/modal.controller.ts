// modal.controller

export const modalController = ((component: any) => {

    const elements: string[] = [
        'a[href]:not([disabled])',
        'button:not([disabled])',
        'textarea:not([disabled])',
        'input[type="text"]:not([disabled])',
        'input[type="radio"]:not([disabled]):checked',
        'input[type="checkbox"]:not([disabled])',
        'select:not([disabled])'
    ]


    const toggle: Function = () => {
        const visible: boolean = (component.getAttribute('visible') === 'true') ? false : true;
        component.setAttribute('visible', visible);
    }


    const events: Function = () => {
        const focusable: any = Array.from(component.querySelectorAll(elements));


        // close button
        component.addEventListener('c-button', (event: any) => {
            if (event.detail.type() === 'click' && event.detail.task() === 'toggle') {
                event.stopPropagation();
                event.preventDefault();
                toggle();
                event.target.blur()
            }
        });


        // click outside
        component.wrapper.onclick = (event: MouseEvent) => {
           const target: any = event.target;
           if (target.classList.contains('c-modal_wrapper')) toggle();
        }


        // esc key
        component.addEventListener('keydown', (event: KeyboardEvent) => {
            if (event.key === 'Escape') {
                event.stopPropagation();
                event.preventDefault();
                component.setAttribute('visible', 'false');
            }
            if (event.key === 'Tab') {
                if (event.shiftKey) { /* shift + tab */
                    if (document.activeElement === focusable[0]) {
                        focusable[focusable.length - 1].focus();
                        event.preventDefault();
                    }
                } else {
                    if (document.activeElement === focusable[focusable.length - 1]) {
                        focusable[0].focus();
                        event.preventDefault();
                    }
                }
            }
       });
    };


    // ---

    setTimeout(() => {
        events();
    }, 100);

});
