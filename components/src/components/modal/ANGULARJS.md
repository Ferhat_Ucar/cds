The notification element doesn't dispatch a custom event.
Instead the notification component toggles visibility based on the visible="true/false" attribute.


#### States
The notification component is watching the state: **visible**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **visibility**    | visible="true/false"        | toggles notification visibility |    |



#### Example
```html
<c-notification is="notification-success" visible="true">
    <c-button is="button-ghost-icon" task="toggle">
        <span class="hidden-label">Close</span>
        <c-icon is="icon-close"></c-icon>
    </c-button>
    <h3>Success message</h3>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Sed nisi quam, eleifend pharetra viverra ac, vehicula et nisi. In magna odio, vestibulum sed gravida in, fermentum ac dolor.
    </p>
</c-notification>
```
