// modal.ts
import { webcomponent } from '../../factories/webcomponent'
import { modalController } from './modal.controller'
import { ready } from '../../factories/webcomponent.ready'


// ---


export class CModal extends HTMLElement {
    opener: any;


    constructor() {
        super();
        this.opener = false;
    }


    // vars
    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed',
            title: 'not-empty',
            'label-button-close': 'not-empty'
        };
    }


    // ---


    private get button(){
        return this.querySelector('.c-button') || false;
    }


    // ---


    private get modal(){
        return this.querySelector('.c-modal') || false;
    }


    // ---


    private get height(){
        const content: any = this.querySelector('.c-modal_content') || false;
        const header: any = this.querySelector('.c-modal_header') || false;
        const footer: any = this.querySelector('.c-modal_footer') || false;
        const contentHeight: number = (content) ? content.offsetHeight : 0;
        const headerHeight: number = (header) ? header.offsetHeight : 0;
        const footerHeight: number = (footer) ? footer.offsetHeight : 0;
        return (contentHeight + headerHeight + footerHeight);
    }


    // ---


    private get wrapper(){
        return this.querySelector('.c-modal_wrapper') || false;
    }


    // ---


    private get buttons(){
        return this.querySelector('.block-button-group') || false;
    }


    // ---


    private render() {
        let html: string = '';
        const css: string = this.getAttribute('css') || '';
        const id: string = this.getAttribute('id') || '';
        const hidden: string= (!this.hasAttribute('visible') && this.getAttribute('visible') !== 'true') ? 'hidden' : '';
        const title: string= this.getAttribute('title') || '';
        const label: string = this.getAttribute('label-button-close') || 'Close';
        const buttons: any = this.buttons;
        if (buttons !== false) {
            this.removeChild(buttons);
            html = `<div class="c-modal_footer">${buttons.outerHTML}</div>`;
        }


        return `
            <div class="c-modal_wrapper">
                <div
                    id="c-${id}"
                    class="c-modal ${css}"
                    role="dialog"
                    aria-labelledby="c-${id}-label"
                    aria-describedby="c-${id}-content"
                    aria-modal="true"
                    ${hidden}
                >
                    <div class="c-modal_header">
                        <h3 id="c-${id}-label">${title}</h3>
                        <c-button is="button-ghost-icon" task="toggle">
                            <span class="hidden-label">${label}</span>
                            <c-icon is="icon-close"></c-icon>
                        </c-button>
                    </div>
                    <div id="c-${id}-content" class="c-modal_content ${(buttons !== false) ? 'has-buttons' : ''}">${this.innerHTML.trim()}</div>
                    ${html}
                </div>
            </div>`
    }


    // ---


    private controller() {
        modalController(this);
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    static get observedAttributes() {
        return [
            'visible'
        ]
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const modal: any = this.modal;
        const button: any = this.button;
        const val = newValue !== null;

        if (modal) {
            switch(attribute) {
                case 'visible':
                    if (newValue === 'true') {
                        const event: any = window.event;
                        this.opener = event.target || false;
                        modal.removeAttribute('hidden');
                        button.removeAttribute('tabindex');
                        button.focus();
                        if (this.height !== 0) modal.setAttribute('style', `height: ${this.height}px;`)
                    } else {
                        modal.setAttribute('hidden','');
                        button.setAttribute('tabindex','-1');
                        if (this.opener !== false) this.opener.focus();
                    }
                    break;

                default:
                    return;
            }
        }
    }
}
