```html
<c-modal id="modal-id-01" title="Modal title" label-button-close="Close" visible="false">
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Sed nisi quam, eleifend pharetra viverra ac, vehicula et nisi. In magna odio, vestibulum sed gravida in, fermentum ac dolor.
    </p>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Sed nisi quam, eleifend pharetra viverra ac, vehicula et nisi. In magna odio, vestibulum sed gravida in, fermentum ac dolor.
    </p>
    <block-button-group is="button-group-align-left">
        <c-button task="demo-purpose" is="button-secondary" id="button-id-11">Cancel</c-button>
        <c-button task="demo-purpose" is="button-primary" id="button-id-12">Save</c-button>
    </block-button-group>
</c-modal>
```
