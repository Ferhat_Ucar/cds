// notification.controller

export const notificationController = ((component: any) => {


    const events = () => {
        component.addEventListener('c-button', (event: any) => {
            if (event.detail.type() === 'click' && event.detail.task() === 'toggle') {
                const visible: boolean = (component.getAttribute('visible') === 'true') ? false : true;
                component.setAttribute('visible', visible);
                event.target.blur()
            }
        });
    };


    // ---


    events();
});
