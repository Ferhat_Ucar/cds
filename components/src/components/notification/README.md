Notifications are messages that communicate information to the user.

Notification is a wrapper using the [c-button](/components/buttons/button-primary) component.
The c-button must be included inside the wrapper. The c-button must have the attribute 'task' set to 'toggle';


#### Attributes
| Attribute         | Value                 | Description  | Required     |
| -------------     |:-------------:        | ------------: |------------:|
| **is**            | flash-error           | Renders the notification as 'flash-error' | * |
|                   | flash-success         | Renders the notification as 'flash-success' | * |
|                   | flash-warning         | Renders the notification as 'flash-warning' | * |
|                   | notification-error    | Renders the notification as 'notification-error' | * |
|                   | notification-info     | Renders the notification as 'notification-info' | * |
|                   | notification-success  | Renders the notification as 'notification-success' | * |
|                   | notification-warning  | Renders the notification as 'notification-warning' | * |
| **style**         | x                     | The style attribute is not allowed | x |
| **visible**       | boolean               | True/false boolean toggles notification visibility | * |




#### User input

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | button            | toggles notification visibility |    |
| **key:enter**             | button            | toggles notification visibility |    |
| **key:tab**               | button            | set focus on the button. |    |
| **touch**                 | button            | toggles notification visibility |    |
