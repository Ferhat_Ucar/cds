// notification.ts
import { webcomponent } from '../../factories/webcomponent'
import { notificationController } from './notification.controller'
import { ready } from '../../factories/webcomponent.ready'


// ---


export class CNotification extends HTMLElement {

    // vars
    private get checkAttributes() {
        return {
            is: [
                'notification-error',
                'notification-info',
                'notification-success',
                'notification-warning',
                'flash-error',
                'flash-success',
                'flash-warning'
            ],
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="toggle"]',
            ],
            optional: [
                'h3',
                'p',
                'div'
            ]
        };
    }


    // ---


    private get button(){
        return this.querySelector('.c-button') || false;
    }


    // ---


    private get notification(){
        return this.querySelector('.c-notification') || false;
    }


    // ---


    private render() {
        const css: string = this.getAttribute('css') || '';
        const hidden: string= (!this.hasAttribute('visible') && this.getAttribute('visible') !== 'true') ? 'hidden' : '';

        return `
            <div aria-live="polite" class="c-notification is-${this.getAttribute('is') || ""} ${css}" ${hidden}>
                ${this.innerHTML.trim()}
            </div>
        `
    }


    // ---


    private controller() {
        notificationController(this);
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    static get observedAttributes() {
        return [
            'visible'
        ]
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const notification: any = this.notification;
        const button: any = this.button;
        const val = newValue !== null;

        if (notification) {
            switch(attribute) {
                case 'visible':
                    if (newValue === 'true') {
                        notification.removeAttribute('hidden');
                        button.removeAttribute('tabindex');
                        button.focus();
                    } else {
                        notification.setAttribute('hidden','');
                        button.setAttribute('tabindex','-1');
                    }
                    break;

                default:
                    return;
            }
        }
    }
}
