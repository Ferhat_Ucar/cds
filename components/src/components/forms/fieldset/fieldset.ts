// fieldset.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'


// ---


export class CFieldset extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        const css: string = this.getAttribute('css') || '';

        return `
            <fieldset class="c-fieldset ${css}">
                ${(this.hasAttribute('legend')) ? `<legend class="c-fieldset-legend">${this.getAttribute('legend')}</legend>` : ''}
                ${this.innerHTML.trim()}
            </fiedset>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                    }
                });
        }
    }
}
