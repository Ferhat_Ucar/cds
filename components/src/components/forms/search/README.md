Search enables users to specify a word or a phrase to find particular relevant pieces of content without the use of navigation. Search can be used as the primary means of discovering content, or as a filter to aid the user in finding content.

Search is a wrapper using the [c-input](/components/forms/input-text) and [c-button](/components/buttons/button-primary) components.
Both the c-input and the c-button must be included inside the wrapper. The c-button must have the attribute 'task' set to 'search';


#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **style**         | x                 | The style attribute is not allowed | x |
| **value**         | string            | The 'value' attribute is optional |  |



#### User input
Users should be able to focus on the input by clicking on the box directly or by clicking on its label.

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | input             | Adds focus state on the input |    |
| **key:enter**             | input             | submits the search input |    |
| **key:tab**               | input             | Adds focus state on the input or when input is focussed, puts focus on the submit button |    |
| **touch**                 | input             | Adds focus state on the input |    |
| **touch**                 | input             | Adds focus state on the input |    |
| **click**                 | label             | Adds focus state on the input |    |
| **touch**                 | label             | Adds focus state on the input |    |
| **click**                 | button            | submits the search input |    |
| **key:enter**             | button             | submits the search input |    |
| **key:tab**               | button             | Adds focus state on the button |    |
| **touch**                 | button             | submits the search input |    |
