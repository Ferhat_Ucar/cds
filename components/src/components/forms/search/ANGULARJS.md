The input dispatches a custom event called **c-search**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-search** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-search="myVar = $event.detail.value()"**

or:
**ng-on-c-search="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)


#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-search** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **value()**       | string                | Returns the value attribute value  |    |
| **submit()**       | boolean              | Returns the submit state  |    |


#### States
The search component is watching the state: **value**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **value**         | value=""          | Sets the value in the input component |    |



#### Example
```html
<c-search ng-on-c-search="mySearchVar = $event.detail.value(); mySearchSubmit = $event.detail.submit()" value="">
    <c-input is="text" value="" placeholder="e.g. 200003" id="search-id-01" name="inputfield">Search</c-input>
    <c-button is="button-secondary" task="search">Search</c-button>
</c-search>
```
