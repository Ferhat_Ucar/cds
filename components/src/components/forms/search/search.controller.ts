// search.controller.ts
import { broadcast } from '../../../factories/event.broadcast'



export const searchController: Function = ((component: any) => {


    const button: any = component.button;
    const inputParent: any = component.input;
    const input: any = inputParent.querySelector('.c-input-text');


    // ---


    const events: Function = () => {

        // input events
        component.addEventListener('c-input', (event: any) => {

            // input focus /  blur event
            if (event.detail.focus() === true) {
                component.setAttribute('data-submit', 'false');
            } else {
                if (event.detail.value() === "") {
                }
            }


            // key events
            if (event.detail.key() !== false) {
                switch (event.detail.key()) {

                    case 'Enter':
                        event.cancelBubble = true;
                        event.preventDefault();
                        component.setAttribute('data-submit', 'true');
                        input.dispatchEvent(new Event('submit'));
                        break;

                    default:
                        event.cancelBubble = true;
                        event.preventDefault();
                        component.setAttribute('data-submit', 'false');
                        input.dispatchEvent(new Event('change'));
                        break;
                }
            }
        });



        // button events
        component.addEventListener('c-button', (event: any) => {
            if (event.detail.key() === 'Enter' || event.detail.type() === 'click') {
                event.cancelBubble = true;
                event.preventDefault();
                component.setAttribute('data-submit', 'true');
                input.dispatchEvent(new Event('submit'));
            }
        });



        const broadcaster: any = (event: any) => {
            // event: broadcast
            broadcast(input, 'c-search', event, {
                value: () => input.value,
                focus: () => input === document.activeElement,
                submit: () => component.getAttribute('data-submit') || false
            });

            // update: component
            component.setAttribute('value', input.value);
        };


        input.addEventListener('change', broadcaster);
        input.addEventListener('submit', broadcaster);
    }


    // ---

    events();
});
