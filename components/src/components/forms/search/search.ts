// search.ts
import { webcomponent } from '../../../factories/webcomponent'
import { ready } from '../../../factories/webcomponent.ready'
import { searchController } from './search.controller';


// ---


export class CSearch extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed',
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="search"]',
                'c-input'
            ]
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'value'
        ]
    }


    // ---


    private get button() {
        return this.querySelector('c-button[task="search"]') || false;
    }



    // ---


    private get input() {
        return this.querySelector('c-input') || false;
    }


    // ---


    private controller() {
        searchController(this);
    }


    // ---


    private render() {
        const button: any = this.button;
        const input: any = this.input;

        return `
            <div class="c-search" data-submit="false">
                <div class="c-search_input">${input.outerHTML}</div>
                <div class="c-search_button">${button.outerHTML}</div>
            </div>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const input: any = this.input;
        const val = newValue !== null;

        if (input) {
            switch(attribute) {
                case 'value':
                    if (input.getAttribute('value') !== newValue) {
                        input.querySelector('c-input')
                        input.setAttribute('value', newValue);
                    }
                    break;

                default:
                    return;
            }
        }
    }
}
