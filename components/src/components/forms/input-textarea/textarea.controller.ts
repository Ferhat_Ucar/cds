import { broadcast } from '../../../factories/event.broadcast'



export const textareaController: Function = ((component: any) => {

    const textarea: any = component.textarea;
    const error: any = component.error;
    const messagePattern: string = component.errorMessagePattern;
    const messageRequired: string = component.errorMessageRequired;
    const wrapper: any = component.wrapper;



    // classes
    const classes: any = {
        all: ['is-invalid', 'is-required', 'has-focus'],
        focus: ['is-required', 'has-focus'],
        keyup: ['is-invalid', 'is-required'],
    }


    // value
    const value = () => textarea.value


    // reset
    const reset = (type: string) => {
        if (type === "all" || type === "keyup") error.innerText = '';
        textarea.classList.remove(...classes[type]);
        wrapper.classList.remove(...classes[type]);
    }


    // validate
    const validate: any = {
        required: () => {
            const required: boolean = component.required;
            if (value() === "" && required === true) {
                error.innerText = messageRequired;
                textarea.classList.add('is-required');
                wrapper.classList.add('is-required');
            }
        },
        regex: () => {
            const pattern: string = component.pattern;
            if (!new RegExp(pattern).test(value()) && value() !== "") {
                error.innerText = messagePattern;
                textarea.classList.add('is-invalid');
                wrapper.classList.add('is-invalid');
            }
        }
    }



    // ---



    const events: Function = () => {

        // event: focus
        textarea.onfocus = () => {
            reset('focus');
            wrapper.classList.add('has-focus');
        };


        // event: blur
        textarea.onblur = () => {
            reset('all');
            validate.regex();
            validate.required();
        };


        const broadcaster: any = (event: any) => {
            // event: broadcast
            broadcast(textarea, 'c-textarea', event, {
                value: () => value(),
                focus: () => textarea === document.activeElement,
            });

            // update: component
            component.setAttribute('value', value());
        };


        textarea.addEventListener('focus', broadcaster);
        textarea.addEventListener('blur', broadcaster);
        textarea.addEventListener('keyup', broadcaster);
    }



    // ---



    events();
});
