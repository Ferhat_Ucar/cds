// textarea.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'
import { textareaController } from './textarea.controller';



// ---



export class CTextarea extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            placeholder: 'not-empty',
            style: 'not-allowed',
            css: 'not-allowed'
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'value',
            'required',
            'disabled',
            'readonly'
        ]
    }


    // ---


    private get textarea() {
        return this.querySelector('textarea.c-textarea') || false;
    }


    // ---


    private get error() {
        return this.querySelector('.c-textarea_error') || false;
    }


    // ---


    private get errorMessagePattern() {
        return this.getAttribute('error-message-pattern') || '';
    }


    // ---


    private get errorMessageRequired() {
        return this.getAttribute('error-message-required') || '';
    }


    // ---


    private get pattern() {
        return this.getAttribute('pattern') || '';
    }


    // ---


    private get required() {
        return (this.hasAttribute('required') ? true : false);
    }


    // ---


    private get wrapper() {
        return this.querySelector('div.c-textarea') || false;
    }


    // ---


    private controller() {
        textareaController(this);
    }


    // ---


    private render() {
        const disabled: string = (this.hasAttribute('disabled')) ? 'disabled' : '';
        const required: string = (this.hasAttribute('required')) ? 'required' : '';
        const readonly: string = (this.hasAttribute('readonly')) ? 'readonly' : '';
        const label: string = (this.innerHTML.trim() !== '') ? `<label class="c-textarea_label" for="c-${this.getAttribute('id') || ''}">${this.innerHTML.trim()}</label>` : ``;

        return `
            <div class="c-textarea">
                ${label}
                <textarea
                    id="c-${this.getAttribute('id') || ''}"
                    class="c-textarea"
                    name="${this.getAttribute('name') || ''}"
                    type="${this.getAttribute('is') || 'text'}"
                    placeholder="${this.getAttribute('placeholder') || ''}"
                    pattern="${this.getAttribute('pattern') || ''}"
                    aria-describedby="c-${this.getAttribute('id') || ''}-error-message"
                    ${disabled}
                    ${required}
                    ${readonly}
                />${this.getAttribute('value') || ''}</textarea>
                <span id="c-${this.getAttribute('id') || ''}-error-message" class="c-textarea_error" aria-live="polite"></span>
            </div>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const textarea: any = this.textarea;
        const val = newValue !== null;

        if (textarea) {
            switch(attribute) {
                case 'value':
                    if (textarea.value !== newValue) {
                        textarea.value = newValue;
                        textarea.dispatchEvent(new Event('blur'));
                    }
                    break;

                case 'disabled':
                    if (textarea.disabled !== val) textarea.disabled = val;
                    break;

                case 'required':
                    if (textarea.required !== val) textarea.required = val;
                    if (val === false) textarea.dispatchEvent(new Event('blur'));
                    break;

                case 'readonly':
                    if (textarea.readonly !== val) textarea.readonly = val;
                    break;

                default:
                    return;
            }
        }
    }
}
