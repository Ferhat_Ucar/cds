// radio.controller.ts
import { broadcast } from '../../../factories/event.broadcast'


export const radioController: Function = ((component: any) => {


    const events: Function = () => {
        const radio: any = component.radio;

        if (radio) {
            // event: function
            const broadcaster: any = (event: any) => {

                // event: broadcast
                broadcast(radio, 'c-radio', event, {
                    checked: () => radio.checked,
                    value: () => radio.value
                });

                // update: component
                (radio.checked)
                    ? component.setAttribute('checked', '')
                    : component.removeAttribute('checked');

                // update c-radio attributes
                const radios = Array.from(document.querySelectorAll(`input[type="radio"][name="${radio.name}"]`))  || [];
                radios.forEach((input) => {
                    if (input !== radio) {
                        const parent: any = input.parentNode || null;
                        if (parent) parent.removeAttribute('checked');
                    }
                })
            };

            // event: handlers
            radio.addEventListener('change', broadcaster, true);
        }
    }


    // ---


    events();
});
