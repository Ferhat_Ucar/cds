// radio.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'
import { radioController } from './radio.controller'



// ---



export class CRadio extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            is: ['radio'],
            name: 'not-empty',
            style: 'not-allowed',
            value: 'not-empty'
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'checked',
            'disabled'
        ]
    }


    // ---


    private get radio() {
        return this.querySelector('input[type="radio"]') || false;
    }


    // ---


    private controller() {
        radioController(this);
    }


    // ---


    private render() {

        return `
            <input
                type="radio"
                id="c-${this.getAttribute('id') || ''}"
                class="c-radio is-${this.getAttribute('is') || ''} ${this.getAttribute('css') || ''}"
                name="c-${this.getAttribute('name') || ''}"
                value="c-${this.getAttribute('value') || ''}"
                ${(this.hasAttribute('checked')) ? 'checked' : ''}
                ${(this.hasAttribute('disabled')) ? 'disabled' : ''}
            >
            <label for="c-${this.getAttribute('id') || ''}">
                ${this.innerHTML.trim() || 'No label set for this radio'}
            </label>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const radio: any = this.radio;
        const value = newValue !== null;

        if (radio) {
            switch(attribute) {
                case 'checked':
                    if (radio.checked !== value) {
                        radio.checked = value;
                        radio.dispatchEvent(new Event('change'));
                    }
                    break;

                case 'disabled':
                    if (radio.disabled !== value) radio.disabled = value;
                    break;

                default:
                    return;
            }
        }
    }
}
