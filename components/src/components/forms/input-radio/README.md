Radio buttons are used when a list of two or more options are mutually exclusive, meaning the user must select only one option.



#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     | -------------     | ------------ | ------------ |
| **checked**       | checked           | A boolean indicating whether or not this radio button is the currently-selected item in the group |    |
| **css**           | string            | The css attribute can be uses to add an extra style class on the element  |  |
| **disabled**      | disabled          | Determines the disabled state of the object |    |
| **id**            | unique identifier | The id attribute specifies a unique id for an HTML element (the value must be unique within the HTML document). | * |
| **is **           | radio             | The attribute 'is' determines how the radio is rendered. (renders as radio) | * |
| **name**          | unique identifier | A radio group is defined by giving each of radio buttons in the group the same name. Once a radio group is established, selecting any radio button in that group automatically deselects any currently-selected radio button in the same group.  | * |
| **style**         | x                 | The style attribute is not allowed | x |
| **value**         | string            | The value attribute is a DOMString containing the radio button's value | * |



#### User input
Users should be able to select the radio by clicking on the box directly or by clicking on its label.

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **&uarr; arrow up**       | radio          | Move to radio above current and set checked state |    |
| **&rarr; arrow right**    | radio          | Move to radio below current and set checked state |    |
| **&darr; arrow down**     | radio          | Move to radio below current and set checked state |    |
| **&larr; arrow left**     | radio          | Move to radio above current and set checked state |    |
| **click**                 | radio          | Toggles the checked state |    |
| **tab**                   | radio          | Adds focus state on the radio |    |
| **touch**                 | radio          | Toggles the checked state |    |
