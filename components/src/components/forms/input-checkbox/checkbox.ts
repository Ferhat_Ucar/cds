// checkbox.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'
import { checkboxController } from './checkbox.controller'


// ---



export class CCheckbox extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            is: ['checkbox', 'switch'],
            style: 'not-allowed',
            task: 'not-empty'
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'checked',
            'disabled',
            'label'
        ]
    }


    // ---


    private get checkbox() {
        return this.querySelector('input[type="checkbox"]') || false;
    }


    // ---


    private get label() {
        return this.getAttribute('label') || '';
    }


    // ---


    private controller() {
        if (this.hasAttribute('ready')) checkboxController(this)
    }


    // ---


    private render() {
        return `
            <input
                type="checkbox"
                id="c-${this.getAttribute('id') || ''}"
                class="c-checkbox is-${this.getAttribute('is') || ''} ${this.getAttribute('css') || ''}"
                data-task="${this.getAttribute('task') || ''}"
                ${(this.hasAttribute('value')) ? `value="${this.getAttribute('value') || ''}"` : ''}
                ${(this.hasAttribute('checked')) ? 'checked' : ''}
                ${(this.hasAttribute('disabled')) ? 'disabled' : ''}
            />
            <label for="c-${this.getAttribute('id')}" class="${(this.innerHTML.indexOf('hidden') > -1) ? 'label-is-hidden' : ''}">
                ${this.label}
            </label>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const checkbox: any = this.checkbox;
        const value = newValue !== null;

        if (checkbox) {
            switch(attribute) {
                case 'checked':
                    if (checkbox.checked !== value) {
                        checkbox.checked = value;
                        checkbox.dispatchEvent(new Event('change'));
                    }
                    break;

                case 'disabled':
                    if (checkbox.disabled !== value) checkbox.disabled = value;
                    break;

                case 'label':
                    const label: any = this.querySelector('label') || false;
                    if (label) label.innerHTML = newValue;
                    break;

                default:
                    return;
            }
        }
    }
}
