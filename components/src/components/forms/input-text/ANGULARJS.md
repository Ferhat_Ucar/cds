The input dispatches a custom event called **c-input**.
AngularJS has a special built-in ngOn directive for custom events.


#### ng-On
The ngOn directive adds an event listener to a DOM element and evaluates an expression when the event is fired.
Adding the ngOn directive combined with the custom event: **ng-on-c-input** on the component, adds a listeners for all events.
Use an expression or a function inside the ngOn to handle the event details

eg.:
**ng-on-c-input="myVar = $event.detail.checked()"**

or:
**ng-on-c-input="myFunction($event)"**

For more information on ngOn check the documentation on the [AngularJS website](https://docs.angularjs.org/api/ng/directive/ngOn)


#### Event details
All event details can be retrieved trough: **$event.detail.property()**
The custom event **c-input** dispatches the following details:


| property          | Type                  |  Description          |    |
| -------------     | ------------          | ------------          | -- |
| _general:_        |                       |                                           |    |
| **component()**   | string                | Component identifier |    |
| **focus()**       | boolean               | Has the component received focus    |   |
| **key()**         | event.key or false    | A string representing the value of the key pressed by the user |   |
| **target()**      | HTMLObject            | Component HTMLObject |    |
| **type()**        | event.type or false   | A DOMString containing the type of event. |    |
| _specific:_       |                       |                                           |    |
| **value()**       | string                | Returns the value attribute value  |    |


#### States
The input component is watching 4 states: **disabled**, **readonly**, **required** and **value**.

| State             | Attribute         | Description  |    |
| -------------     | -------------     | ------------ | -- |
| **disabled**      | disabled          | A Boolean attribute which is present if the input should be disabled |    |
| **readonly**      | readonly          | A Boolean attribute which, if true, indicates that the input cannot be edited |    |
| **required**      | required          | A Boolean attribute which, if true, indicates that the input must have a value before the form can be submitted |    |
| **value**         | value=""          | Sets the value in the input component |    |



#### Example
```html
<c-input
    is="text"
    value="{{myVar}}"
    placeholder="Placeholder text"
    id="your-id"
    name="your-inputfield-name"
    pattern="^[a-zA-Z ]+$"
    required
    ng-on-c-input="myVar = $event.detail.value()"
    error-message-pattern="Error. The text can only contain letters"
    error-message-required="Error. This is a required field">
    Input label
</c-input>
```
