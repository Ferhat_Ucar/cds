import { broadcast } from '../../../factories/event.broadcast'



export const inputController: Function = ((component: any) => {

    const input: any = component.input;
    const error: any = component.error;
    const messagePattern: string = component.errorMessagePattern;
    const messageRequired: string = component.errorMessageRequired;
    const wrapper: any = component.wrapper;



    // classes
    const classes: any = {
        all: ['is-invalid', 'is-required', 'has-focus'],
        focus: ['is-required', 'has-focus'],
        keyup: ['is-invalid', 'is-required'],
    }


    // value
    const value = () => input.value


    // reset
    const reset = (type: string) => {
        if (type === "all" || type === "keyup") {
            if (error) error.innerText = '';
        }
        input.classList.remove(...classes[type]);
        wrapper.classList.remove(...classes[type]);
    }


    // validate
    const validate: any = {
        required: () => {
            const required: boolean = component.required;
            if (value() === "" && required === true) {
                error.innerText = messageRequired;
                input.classList.add('is-required');
                wrapper.classList.add('is-required');
            }
        },
        regex: () => {
            const pattern: string = component.pattern;
            if (!new RegExp(pattern).test(value()) && value() !== "") {
                error.innerText = messagePattern;
                input.classList.add('is-invalid');
                wrapper.classList.add('is-invalid');
            }
        }
    }



    // ---



    const events: Function = () => {

        // event: focus
        input.onfocus = () => {
            reset('focus');
            wrapper.classList.add('has-focus');
        };


        // event: blur
        input.onblur = () => {
            reset('all');
            validate.regex();
            validate.required();
        };


        const broadcaster: any = (event: any) => {
            // event: broadcast
            broadcast(input, 'c-input', event, {
                value: () => value(),
                focus: () => input === document.activeElement,
            });

            // update: component
            component.setAttribute('value', value());
        };


        input.addEventListener('focus', broadcaster);
        input.addEventListener('blur', broadcaster);
        input.addEventListener('keyup', broadcaster);
    }



    // ---



    events();
});
