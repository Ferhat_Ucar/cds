// input.ts
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'
import { inputController } from './input.controller';



// ---



export class CInput extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            is: [
                'text',
                'password',
                'phone',
                'number',
                'email',
                'date',
                'time',
                'datetime',
                'file',
            ],
            placeholder: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'value',
            'required',
            'disabled',
            'readonly'
        ]
    }


    // ---


    private get input() {
        return this.querySelector('input.c-input-text') || false;
    }


    // ---


    private get error() {
        return this.querySelector('.c-input_error') || false;
    }


    // ---


    private get errorMessagePattern() {
        return this.getAttribute('error-message-pattern') || '';
    }


    // ---


    private get errorMessageRequired() {
        return this.getAttribute('error-message-required') || '';
    }


    // ---


    private get pattern() {
        return this.getAttribute('pattern') || '';
    }


    // ---


    private get required() {
        return (this.hasAttribute('required'));
    }


    // ---


    private get wrapper() {
        return this.querySelector('div.c-input') || false;
    }


    // ---


    private controller() {
        inputController(this);
    }


    // ---


    private render() {
        const disabled: string = (this.hasAttribute('disabled')) ? 'disabled' : '';
        const pattern: string = (this.hasAttribute('pattern')) ? `pattern="${this.getAttribute('pattern') || ''}"` : '';
        const required: string = (this.hasAttribute('required')) ? 'required' : '';
        const readonly: string = (this.hasAttribute('readonly')) ? 'readonly' : '';
        const tabindex: string = (this.hasAttribute('tabindex')) ? `tabindex="${this.getAttribute('tabindex') || ''}"` : '';
        const label: string = (this.innerHTML.trim() !== '') ? `<label class="c-input_label" for="c-${this.getAttribute('id') || ''}">${this.innerHTML.trim()}</label>` : ``;
        const errors: string = (required !== '' || pattern !== '') ? `<span id="c-${this.getAttribute('id') || ''}-error-message" class="c-input_error" aria-live="polite"></span>` : '';

        return `
            <div class="c-input">
                ${label}
                <input
                    id="c-${this.getAttribute('id') || ''}"
                    class="c-input-text"
                    name="${this.getAttribute('name') || ''}"
                    value="${this.getAttribute('value') || ''}"
                    type="${this.getAttribute('is') || 'text'}"
                    placeholder="${this.getAttribute('placeholder') || ''}"
                    aria-describedby="c-${this.getAttribute('id') || ''}-error-message"
                    ${tabindex}
                    ${disabled}
                    ${pattern}
                    ${required}
                    ${readonly}
                />
                ${errors}
            </div>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const input: any = this.input;
        const val = newValue !== null;

        if (input) {
            switch(attribute) {
                case 'value':
                    if (input.value !== newValue) {
                        input.value = newValue;
                        input.dispatchEvent(new Event('blur'));
                    }
                    break;

                case 'disabled':
                    if (input.disabled !== val) input.disabled = val;
                    break;

                case 'required':
                    if (input.required !== val) input.required = val;
                    if (!val) input.dispatchEvent(new Event('blur'));
                    break;

                case 'readonly':
                    if (input.readonly !== val) input.readonly = val;
                    break;

                default:
                    return;
            }
        }
    }
}
