The input element is used to create interactive controls for web-based forms in order to accept data from the user.


#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:                        | ------------:                             |------------:|
| **css**           | string                                | The css attribute can be uses to add an extra style class on the element  |  |
| **disabled**      | disabled                              | A Boolean attribute which is present if the input should be disabled |    |
| **error-message-pattern**         | string                | Error message if the input value does not match the given pattern and hat the pattern attribute | * (if pattern) |
| **error-message-required**        | string                | Error message if the input value is empty and has the required attribute | * (if required) |
| **id**            | unique identifier                     | The id attribute specifies a unique id for an HTML element (the value must be unique within the HTML document). | * |
| **is**            | text                                  | defines the text input                    | * |
|                   | password                              | defines the password input                | * |
|                   | phone                                 | defines the phone input                   | * |
|                   | number                                | defines the number input                  | * |
|                   | email                                 | defines the email input                   | * |
|                   | date                                  | defines the date input                    | * |
|                   | time                                  | defines the time input                    | * |
| **name**          | string                                | The input's name, to identify the input in the data submitted with the form's data                | * |
| **placeholder**   | string                                | adds placeholder attribute                | * |
| **pattern**       | regex-pattern                         | adds a pattern attribute e.g. ^([a-zA-Z0-9]{4,10})$    |  |
| **readonly**      | readonly                              | A Boolean attribute which, if true, indicates that the input cannot be edited |    |
| **required**      | required          | A Boolean attribute which, if true, indicates that the input must have a value before the form can be submitted |    |
| **style**         | x                                     | The style attribute is not allowed | x |
| **value**         | string                                | The 'value' attribute is optional |  |



#### User input
Users should be able to focus on the input by clicking on the box directly or by clicking on its label.

| Method                    | Element           | Result        |     |      
| -------------             | -------------     | ------------- | --- |
| **click**                 | input             | Adds focus state on the input |    |
| **tab**                   | input             | Adds focus state on the input |    |
| **touch**                 | input             | Adds focus state on the input |    |
| **click**                 | label             | Adds focus state on the input |    |
| **touch**                 | label             | Adds focus state on the input |    |
