// datepicker.ts
import { errorReporter } from '../../../factories/error.reporter'
import { ready } from '../../../factories/webcomponent.ready'
import { webcomponent } from '../../../factories/webcomponent'
import { datepickerCalendar } from './datepicker.calendar';
import { datepickerController } from './datepicker.controller';


// ---


export class CDatepicker extends HTMLElement {


    private get checkAttributes() {
        return {
            weekdays: 'not-empty',
            months: 'not-empty',
            notation: 'not-empty',
            iso: 'not-empty',
            'label-year-next': 'not-empty',
            'label-year-previous': 'not-empty',
            'label-month-next': 'not-empty',
            'label-month-previous': 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="datepicker-toggle"]',
                'c-input'
            ]
        };
    }


    // ---


    private get date() {
        let date: any = (this.hasAttribute('date')) ? this.getAttribute('date') : false;
        const today: Date = new Date();

        // format check
        if (date !== false) {
            const check = date.split('-');
            if (check.length < 3) errorReporter.critical.render('c-datepicker', ['The date must contain 3 numbers, seperated by a -']);
            if (parseInt(check[0]) < 1000 || parseInt(check[0]) > 3000) errorReporter.critical.render('c-datepicker', ['The year format is incorrect']);
            if (parseInt(check[1]) < 0 || parseInt(check[1]) > 12) errorReporter.critical.render('c-datepicker', ['The month format is incorrect']);
            if (parseInt(check[2]) < 0 || parseInt(check[2]) > 31) errorReporter.critical.render('c-datepicker', ['The day format is incorrect']);
            if (!(new Date(date) instanceof Date)) errorReporter.critical.render('c-datepicker', ['The given date day format is incorrect']);
        }

        return (date !== false) ? new Date(date) : today;
    }


    // ---


    private get minDate() {
        const minDate: any = this.getAttribute('min-date') || false;
        return (minDate) ? new Date(minDate) : new Date(1000, 1, 1);
    }


    // ---


    private get maxDate() {
        const maxDate: any = this.getAttribute('max-date') || false;
        return (maxDate) ? new Date(maxDate) : new Date(3000, 12, 31);
    }

    // ---


    private get weekdays() {
        let days: any = this.getAttribute('weekdays') || 'mon,tue,wed,thu,fri,sat,sun';
        days = days.split(',');
        if (days.length !== 7) errorReporter.critical.render('c-datepicker', ['The attribute weekdays is incorrect']);
        return days;
    }


    // ---


    private get months() {
        let months: any = this.getAttribute('months') || 'januari,februari,march,april,may,june,july,august,september,october,november,december';
        months = months.split(',');
        if (months.length !== 12) errorReporter.critical.render('c-datepicker', ['The attribute months is incorrect']);
        return months;
    }


    // ---


    private get notation() {
        return this.getAttribute('notation') || 'mm/dd/yyyy';
    }


    // ---


    private get iso() {
        return this.getAttribute('iso') || 'en-GB';
    }


    // ---


    private get day() {
        return this.date.getDate();
    }


    // ---


    private get month() {
        return this.date.getMonth();
    }


    // ---


    private get year() {
        return this.date.getFullYear();
    }


    // ---


    private get input() {
        return this.querySelector('c-input');
    }


    // ---


    private daysInMonth(month: number, year: number) {
        return 32 - new Date(year, month, 32).getDate();
    }



    // ---


    private controller() {
        datepickerController(this);
    }


    // ---


    private calendar() {
        return datepickerCalendar(this);
    }


    // ---


    private render() {
        const input: any = this.querySelector('c-input');
        const button: any = this.querySelector('c-button');
        return `
            <div class="c-datepicker">
                <div class="c-datepicker_input">${input.outerHTML}</div>
                <div class="c-datepicker_button">
                    ${button.outerHTML}
                    <div class="c-datepicker_calendar" hidden></div>
                </div>
            </div>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }
}
