// datepicker.ts
import { broadcast } from '../../../factories/event.broadcast'
import { formatDate } from '../../../factories/format.date'


export const datepickerController: Function = ((component: any) => {

    const wrapper: any = component.querySelector('.c-datepicker_calendar');
    const button: any = component.querySelector('.c-button');
    button.setAttribute('aria-expanded', 'false');



    // ---



    const dateString: Function = (value: number) => {
        return (value < 10) ? '0' + value : value
    }



    // ---



    const events: Function = () => {
        // button events
        component.addEventListener('c-button', (event: any) => {
            const task: string = event.detail.task();

            if (event.detail.type() === 'click') {
                if (task === 'datepicker-toggle') toggle();

                if (task === 'datepicker-next-year' || task === 'datepicker-previous-year' || task === 'datepicker-next-month' || task === 'datepicker-previous-month') {
                    let day: number = component.day;
                    let month: number = component.month;
                    let year: number = component.year;

                    // events
                    if (task === 'datepicker-next-year') year = year + 1;
                    if (task === 'datepicker-previous-year') year = year - 1;
                    if (task === 'datepicker-next-month') {
                        month = ((month + 1) > 11) ? 0 : (month + 1);
                        year = (month === 0) ? (year + 1) : year;
                        day = (day > new Date(year, month + 1, 0).getDate()) ? new Date(year, month + 1, 0).getDate() : day;
                    }
                    if (task === 'datepicker-previous-month') {
                        month = ((month - 1) < 0) ? 11 : (month - 1);
                        year = (month === 11) ? (year - 1) : year;
                        day = (day > new Date(year, month + 1, 0).getDate()) ? new Date(year, month + 1, 0).getDate() : day;
                    }

                    // update calender
                    const table: any = component.querySelector('.c-calendar');
                    table.setAttribute('aria-label', `${component.months[month]} ${year}`);
                    component.setAttribute('date', `${year}-${dateString(month + 1)}-${dateString(day)}`);
                    const datepicker: string = component.calendar();
                    wrapper.innerHTML = datepicker;

                    // set focus on button
                    setTimeout(() => {
                        const button: any = component.querySelector(`button[data-task="${event.detail.task()}"]`);
                        button.focus();
                        keyEvents();
                    }, 1);
                }
            }

            if (event.detail.key() === 'Escape') {
                if (task === 'datepicker-next-year' || task === 'datepicker-previous-year' || task === 'datepicker-next-month' || task === 'datepicker-previous-month') {
                    event.preventDefault();
                    event.stopPropagation();
                    toggle();
                }
            }
        });
        
        // broadcast
        const broadcaster: any = (event: any) => {
            // event: broadcast
            broadcast(component.input, 'c-datepicker', event, {
                value: () => component.input.value,
                focus: () => component.input === document.activeElement,
            });
        };
        component.input.addEventListener('focus', broadcaster);
        component.input.addEventListener('blur', broadcaster);
        component.input.addEventListener('change', broadcaster);
        component.input.addEventListener('keyup', broadcaster);
    }



    // ---



    const keyEvents: Function = () => {
        const cells: any = component.querySelectorAll('.c-calendar_day[role="link"]');

        // Days arrow keys
        cells.forEach((cell: HTMLTableCellElement) => {
            cell.onkeydown = (event: any) => {
                if (event.key === 'ArrowLeft' || event.key === 'ArrowRight' || event.key === 'ArrowUp' || event.key === 'ArrowDown' || event.key === 'PageUp' || event.key === 'PageDown') {
                    event.preventDefault();
                    event.stopPropagation();
                }
                if (event.key === 'Enter') {
                    event.preventDefault();
                    event.stopPropagation();
                    component.input.setAttribute('value', formatDate(new Date(component.getAttribute('date')), component.notation));
                    toggle();
                }
                if (event.key === 'Escape') {
                    event.preventDefault();
                    event.stopPropagation();
                    toggle();
                }
            }

            cell.onclick = (event: any) => {
                event.preventDefault();
                event.stopPropagation();
                component.setAttribute('date', cell.dataset.date);
                component.input.setAttribute('value', formatDate(new Date(component.getAttribute('date')), component.notation));
                toggle();
            }



            cell.onkeyup = (event: any) => {
                if (event.key === 'ArrowLeft' || event.key === 'ArrowRight' || event.key === 'ArrowUp' || event.key === 'ArrowDown' || event.key === 'PageUp' || event.key === 'PageDown') {
                    event.preventDefault();
                    event.stopPropagation();

                    if (event.target) {
                        let day: number = component.day;
                        let month: number = component.month;
                        let year: number = component.year;

                        if (event.key === 'ArrowLeft') day = day - 1;
                        if (event.key === 'ArrowRight') day = day + 1;
                        if (event.key === 'ArrowUp') day = day - 7;
                        if (event.key === 'ArrowDown') day = day + 7;
                        if (event.key === 'PageUp') month = month - 1;
                        if (event.key === 'PageDown') month = month + 1;


                        // check if the current day exists in
                        // the calender on month change
                        day = (day > new Date(year, month + 1, 0).getDate())
                            ? (day === 0)
                                ? new Date(year, month + 1, 0).getDate()
                                : day
                            : day;

                        let date: Date = new Date(year, month, day);


                        // min/max date check
                        if (component.maxDate !== null || component.minDate !== null) {
                            if (date.getTime() > component.maxDate.getTime()) {
                                date = component.maxDate;
                                year = date.getFullYear();
                                month = date.getMonth();
                                day = date.getDate();
                            } else if (date.getTime() < component.minDate.getTime()) {
                                date = component.minDate;
                                year = date.getFullYear();
                                month = date.getMonth();
                                day = date.getDate();
                            }
                        }


                        if (date.getMonth() !== component.month || date.getFullYear() !== component.year) {
                            component.setAttribute('date', `${date.getFullYear()}-${dateString(date.getMonth() + 1)}-${dateString(date.getDate())}`);
                            const datepicker: string = component.calendar();
                            wrapper.innerHTML = datepicker;

                            // set focus on cell
                            setTimeout(() => {
                                const cell: any = component.querySelector(`.c-calendar_day.selected-date`);
                                cell.focus();
                                keyEvents();
                            }, 1);
                        } else {
                            component.setAttribute('date', `${date.getFullYear()}-${dateString(date.getMonth() + 1)}-${dateString(date.getDate())}`);

                            // reset cells
                            cells.forEach((cell: HTMLTableCellElement) => {
                                cell.setAttribute('tabindex', '-1');
                                cell.classList.remove('selected-date');
                            });

                            // select current selected date
                            cells.forEach((cell: HTMLTableCellElement) => {
                                if (cell.getAttribute('data-date') === `${year}-${dateString(month + 1)}-${dateString(day)}`) {
                                    cell.setAttribute('tabindex', '0');
                                    cell.classList.add('selected-date');
                                    cell.focus();
                                }
                            });
                        }
                    }
                }
            }
        })
    }


    // ---



    const outsideClick = (event: MouseEvent) => {
        const target: any = event.target;
        if (target.closest('.c-datepicker_calendar') === null) {
            toggle();
        };
    }



    // ---



    const toggle: Function = () => {
        if (wrapper.hasAttribute('hidden')) {
            wrapper.removeAttribute('hidden');
            button.setAttribute('aria-expanded', 'true');
            const datepicker: string = component.calendar();

            wrapper.innerHTML = datepicker;
            document.addEventListener("mouseup", outsideClick);

            setTimeout(() => {
                const cell: any = component.querySelector(`.c-calendar_day.selected-date`);
                cell.focus();
                keyEvents();
                component.setAttribute('date', cell.dataset.date);
            }, 1);

        } else {
            wrapper.setAttribute('hidden', '');
            button.setAttribute('aria-expanded', 'false');
            const input: any = component.querySelector('.c-input-text');
            input.focus();
            document.removeEventListener("mouseup", outsideClick, false);
        }
    }



    // ---


    events();
    keyEvents();
});
