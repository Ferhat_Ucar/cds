// datepicker.calendar.ts
import { formatDate } from '../../../factories/format.date'


// ---


export const datepickerCalendar: Function = ((component: any) => {

    // vars
    let array: any = [];
    let html: string = '';
    let weeks: any = [];
    let weekdays: string = '';
    const today: Date = new Date();
    const nrOfDaysCurrentMonth: number = new Date(component.year, component.month + 1, 0).getDate();
    const nrOfDaysPreviousMonth: number = new Date(component.year, component.month, 0).getDate();
    const firstDay: number = new Date(component.year, component.month).getDay();


    // render: weekdays
    component.weekdays.forEach((day: string) => {
        weekdays += `<th class="c-calendar_weekday" role="presentation">${day.substring(0, 3)}</th>`;
    });



    // render: calenderdays: previous month
    for (let i: number = (nrOfDaysPreviousMonth - firstDay + 1); i < nrOfDaysPreviousMonth; i++) {
        array.push(`<td class="c-calendar_day other-month" role="presentation">${i + 1}</td>`)
    }



    // render: calenderdays: current month
    for (let i: number = 1; i <= nrOfDaysCurrentMonth; i++) {
        const className : string = (i === component.day && component.year === component.date.getFullYear() && component.month === component.date.getMonth()) ? 'selected-date' : '';
        const current   : string = (i === today.getDate() && component.year === today.getFullYear() && component.month === today.getMonth()) ? `aria-current="date"` : '';
        const date      : string = formatDate(new Date(component.year, component.month, i), 'yyyy-mm-dd');
        const label     : string = new Date(Date.UTC(component.year, component.month, i, 3, 0, 0)).toLocaleDateString(component.iso, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
        const tabindex  : string = (i === component.day && component.year === component.date.getFullYear() && component.month === component.date.getMonth()) ? '0' : '-1';

        if (component.maxDate !== null || component.minDate !== null) {
            if (new Date(component.year, component.month, i).getTime() > component.maxDate.getTime()) {
                array.push(`<td class="c-calendar_day out-of-range" role="presentation">${i}</td>`);
            } else if (new Date(component.year, component.month, (i + 1)).getTime() < component.minDate.getTime()) {
                array.push(`<td class="c-calendar_day out-of-range" role="presentation">${i}</td>`);
            } else {
                array.push(`<td role="link" class="c-calendar_day ${className}" ${current} aria-label="${label}" data-date="${date}" tabindex="${tabindex}">${i}</td>`);
            }
        } else {
            array.push(`<td role="link" class="c-calendar_day ${className}" ${current} aria-label="${label}" data-date="${date}" tabindex="${tabindex}">${i}</td>`);
        }
    }



    // divide array into weeks
    while (array.length) {
        weeks.push(array.splice(0, 7));
    }



    // render: calenderdays: next month
    const finalWeek: number = weeks[weeks.length - 1].length;
    let counter: number = 0;
    if (finalWeek < 7) {
        for (let i: number = 1; i <= (7 - finalWeek); i++) {
            weeks[weeks.length - 1].push(`<td class="c-calendar_day other-month">${i}</td>`);
            counter = i;
        }
    }
    // 6 weeks looks better
    if (weeks.length < 6) {
        weeks[5] = [];
        for (let i: number = (counter + 1); i <= (counter + 7); i++) {
            weeks[5].push(`<td class="c-calendar_day other-month">${i}</td>`);
        }
    }



    // create: html
    weeks.forEach((week: any) => {
        html += '<tr>';
        week.forEach((day: any) => { html += day });
        html += '</tr>';
    })



    // return HTML
    return `
        <table class="c-calendar" role="dialog" aria-label="${component.months[component.month]} ${component.year}">
            <thead>
                <tr role="presentation">
                    <th role="presentation">
                        <c-button id="button-datepicker-previous-year" is="button-ghost-icon" task="datepicker-previous-year">
                            <span class="hidden-label">${component.getAttribute('label-year-previous')}</span>
                            <c-icon is="icon-arrow-left"></c-icon>
                        </c-button>
                    </th>
                    <th colspan="5" role="presentation" class="c-calendar_year">${component.year}</th>
                    <th role="presentation">
                        <c-button id="button-clalender-next-year" is="button-ghost-icon" task="datepicker-next-year">
                            <span class="hidden-label">${component.getAttribute('label-year-next')}</span>
                            <c-icon is="icon-arrow-right"></c-icon>
                        </c-button>
                    </th>
                </tr>
                <tr role="presentation">
                    <th role="presentation">
                        <c-button id="button-datepicker-previous-month" is="button-ghost-icon" task="datepicker-previous-month">
                            <span class="hidden-label">${component.getAttribute('label-month-previous')}</span>
                            <c-icon is="icon-arrow-left"></c-icon>
                        </c-button>
                    </th>
                    <th colspan="5" role="presentation" class="c-calendar_month">${component.months[component.month]}</th>
                    <th role="presentation">
                        <c-button id="button-clalender-next-month" is="button-ghost-icon" task="datepicker-next-month">
                            <span class="hidden-label">${component.getAttribute('label-month-next')}</span>
                            <c-icon is="icon-arrow-right"></c-icon>
                        </c-button>
                    </th>
                </tr>
                <tr role="presentation">
                    ${weekdays}
                </tr>
            </thead>
            <tbody>
                ${html}
            </tbody>
        </table>
    `;
});
