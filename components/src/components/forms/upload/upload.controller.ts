import { broadcast } from '../../../factories/event.broadcast'
import { filesize } from '../../../factories/file.size'


export const uploadController: Function = ((component: any) => {


    let errorMessage: string = ' ';
    let fileList: any = [];
    const error: HTMLSpanElement = component.querySelector('.c-upload_error');
    const input: HTMLInputElement = component.querySelector('.c-upload_input');
    const form: HTMLSpanElement = component.querySelector('.c-upload');



    // ---



    const message: Function = () => {
        form.classList.remove('is-invalid');
        if (errorMessage !== ' ') form.classList.add('is-invalid');
        error.innerText = errorMessage;
    }



    // ---



    const filecheck = (files: any) => {
        let size: number = 0;

        // size of files in current queue
        for (const file of fileList) {
            size = size + file.size;
        }


        // check: type of file
        for (const file of files) {
            if (component.accept !== "" && component.accept.indexOf(file.type) < 0) {
                errorMessage = component.messageFileType;
                message();
                return false;
            }
            size = size + file.size;
        }

        // check: file size
        if (component.maxFileSize !== 0 && size > component.maxFileSize) {
            errorMessage = component.messageFileSize;
            message();
            return false;
        }

        return true;
    }



    // ---



    const fileupload = (files: any) => {
        const uploadList: any[] = [];

        for (const file of files) {
            file.id = file.name.replace(/[^a-zA-Z0-9]/g, '').toLowerCase().substring(0, 7) + '_' + Math.random().toString(36).substring(7) + '_' + new Date().getTime().toString();
            component.fileList.push(file);
            uploadList.push(file);


            const element: HTMLDivElement = document.createElement('div');
            element.className = 'c-upload_file';
            element.dataset.fileId = file.id;
            element.innerHTML = `
                <span class="c-upload_file-name">${file.name} <span class="c-upload_file-size">${filesize(file.size)}</span></span>
                <c-loader css="size-extrasmall"></c-loader>
                <c-icon is="icon-check" label="Success" css="circle"></c-icon>
                <c-icon is="icon-clear" label="Fail" css="circle"></c-icon>
                <br />
                <c-button task="cancel-upload" is="button-ghost">Cancel</c-button>
                <c-button task="remove-file" is="button-ghost">Remove file</c-button>

            `;
            form.append(element);


console.log(file.id);


            setTimeout(() => {
                const buttonCancel: any = element.querySelector('button[data-task="cancel-upload"]');
                const buttonRemove: any = element.querySelector('button[data-task="remove-file"]');

                // cancel: upload
                if (buttonCancel) {
                    buttonCancel.onclick = () => {
                        element.classList.add('remove-item-from-dom');
                        if (component.fileList.indexOf(file) > -1) component.fileList.splice(component.fileList.indexOf(file), 1);

                        setTimeout(() => {
                            form.removeChild(element);
                        }, 400);

                        // broadcast
                        broadcast(component, 'c-upload', null, {
                            action: () => 'cancel-upload',
                            files: () => [file]
                        });
                    }
                }

                // remove: upload
                if (buttonRemove) {
                    buttonRemove.onclick = () => {
                        element.classList.add('remove-item-from-dom');

                        setTimeout(() => {
                            form.removeChild(element);
                        }, 400);

                        // broadcast
                        broadcast(component, 'c-upload', null, {
                            action: () => 'remove-file',
                            files: () => [file]
                        });
                    }
                }
            }, 200);
        }

        // reset
        input.value = '';


        // broadcast
        broadcast(component, 'c-upload', null, {
            action: () => 'upload-files',
            files: () => uploadList
        });
    }



    // ---



    const events = () => {
        // event: onchange
        input.onchange = () => {
            errorMessage = ' ';
            message();

            if (input.value !== "") {
                if (filecheck(input.files)) {
                    fileupload(input.files)
                }
            }
        };
    }

    events();
});
