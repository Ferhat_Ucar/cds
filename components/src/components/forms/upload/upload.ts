// upload.ts
import { uploadController } from './upload.controller';
import { jsonCheck } from '../../../factories/data.json.check'
import { webcomponent } from '../../../factories/webcomponent'
import { ready } from '../../../factories/webcomponent.ready'



// ---



export class CUpload extends HTMLElement {
    fileList: any[];

    constructor() {
        super()
        this.fileList = [];
    }


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed',
            css: 'not-allowed'
        }
    };


    // ---


    private get accept() {
        return this.getAttribute('accept') || '';
    }


    // ---


    private get disabled() {
        return (this.hasAttribute('disabled')) ? 'disabled' : '';
    }


    // ---


    private get maxFileSize() {
        return parseInt(this.getAttribute('max-filesize') || '0');
    }


    // ---


    private get messageFailed() {
        return this.getAttribute('error-message-failed') || 'An error occured while uploading the file';
    }


    // ---


    private get messageSuccess() {
        return this.getAttribute('error-message-success') || 'The file was succesfully uploaded';
    }


    // ---


    private get messageFileSize() {
        return this.getAttribute('error-message-filesize') || 'Error. Exceeded the (total) file size';
    }


    // ---


    private get messageFileType() {
        return this.getAttribute('error-message-filetype') || 'Error. This filetype is not allowed';
    }


    // ---


    private get multiple() {
        return (this.hasAttribute('multiple')) ? true : false;
    }


    // ---


    private controller() {
        uploadController(this);
    }


    // ---


    render() {
        const id: string = `c-${this.getAttribute('id') || ''}`;

        return `
            <form class="c-upload" method="post" enctype="multipart/form-data">
                <label for="${id}" classname="c-upload_label">${this.innerHTML.trim() || 'No label set for upload'}</label>
                <input
                    type="file"
                    id="${id}"
                    class="c-upload_input"
                    accept="${this.accept}"
                    multiple="${this.multiple}"
                    aria-describedby="${id}-error-message"
                    ${this.disabled}
                 />
                 <span id="${id}-error-message" class="c-upload_error" aria-live="polite"></span>
            </form>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    static get observedAttributes() {
        return [
            'upload-status'
        ]
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const val: boolean = newValue !== null;
        const form: any = this.querySelector('.c-upload');
        const error: any = this.querySelector('.c-upload_error');

        switch(attribute) {
            case 'upload-status':
                if (val === true && jsonCheck(newValue)) {
                    const files: any[] = JSON.parse(newValue);
                    form.classList.remove('is-invalid');
                    for (let file of files) {
                        const item: any = this.querySelector(`[data-file-id="${file.id}"]`) || null;

                        if (item) {
                            item.classList.remove('is-successfull', 'has-failed');
                            if (file.status === 'success') item.classList.add('is-successfull');
                            if (file.status === 'failed') {

                                error.innerHTML = this.messageFailed + ' ' + file.name;
                                form.classList.add('is-invalid');
                                item.classList.add('has-failed');
                            }
                        }

                        // remove from filelist
                        this.fileList.forEach((listitem: any, index: number) => {
                            if (listitem.id === file.id) this.fileList.splice(index, 1);
                        });
                    }
                }
                break;

            default:
                return;
        }
    }
}
