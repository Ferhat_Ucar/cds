```html
<c-fieldset legend="Upload">
    <p>You can upload a file up to 100Mb.</p>
    <c-upload
        id="upload-id-01"
        accept="image/png, image/jpeg"
        max-nr-of-files="3"
        max-filesize="100000000"
        error-message-nr-of-files="Error. you can select a maximum of 3 files at the same time"
        error-message-filesize="Error. Exceeded the (total) file size of 100Mb."
        error-message-filetype="Error. This filetype is not allowed"
        >
        Select a file
    </c-upload>
</c-fieldset>
```
