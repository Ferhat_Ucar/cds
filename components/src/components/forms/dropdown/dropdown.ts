// dropdown.ts
import { broadcast } from '../../../factories/event.broadcast'
import { webcomponent } from '../../../factories/webcomponent'
import { ready } from '../../../factories/webcomponent.ready'
import { dropdownController } from './dropdown.controller';



// ---



export class CDropdown extends HTMLElement {



    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="toggle"]',
                'c-input',
                'c-options'
            ],
            optional: [
                'c-button[task="clear"]'
            ]
        };
    }




    // ---


    static get observedAttributes() {
        return [
            'value',
            'listitems'
        ]
    }


    // ---


    private get input() {
        return this.querySelector('input.c-input-text') || false;
    }


    // ---


    private get buttonClear() {
        return this.querySelector('.c-button[data-task="clear"]') || null
    }


    // ---


    private get buttonToggle() {
        return this.querySelector('.c-button[data-task="toggle"]') || null;
    }


    // ---


    private get list() {
        return this.querySelector('.c-options') || null;
    }


    // ---


    private render() {
        const buttonToggle: any = this.buttonToggle;
        const input: any = this.input;
        const list: any = this.list;


        // set component elements attributes
        input.setAttribute('role', 'combobox');
        input.setAttribute('aria-autocomplete', 'both');
        input.setAttribute('aria-expanded', 'false');
        input.setAttribute('aria-haspopup', 'true');
        input.setAttribute('aria-owns', list.id || '');
        input.setAttribute('autocomplete', 'off');
        list.setAttribute('tabindex', '-1');
        buttonToggle.setAttribute('tabindex', '-1');

        return `<div class="c-dropdown">${this.innerHTML.trim()}</div>`;
    }


    // ---


    private controller() {
        dropdownController(this);
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const input: any = this.input;
        const inputComponent: any = (input) ? input.parentNode.parentNode : null;
        const val = newValue !== null;
        let timer: any = null;


        if (inputComponent) {
            switch(attribute) {
                case 'value':
                    broadcast(this, 'c-dropdown', null, {
                        value: () => newValue
                    });
                    break;

                case 'listitems':
                    clearTimeout(timer)
                    timer = setTimeout(() => this.controller(), 10);
                    break;

                default:
                    return;
            }
        }
    }
}
