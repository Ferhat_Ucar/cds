// dropdown.controller.ts
import { scroll } from '../../../factories/scroll.intoview'


export const dropdownController: Function = ((component: any) => {

    const buttonClear: any = component.buttonClear;
    const buttonToggle: any = component.buttonToggle;
    const input: any = component.input;
    const list: any = component.list;
    const listitems: Array<HTMLElement> = Array.from(list.querySelectorAll('.c-option'));
    let listoptions: any = listitems;


    // ---


    const state: any = {
        key: '',
        open: false,
        option: '',
        search: '',
        selected: -1
    }



    // ---



    const inputfield = {

        // cursor position
        cursor: {
            start: () => input.setSelectionRange(0, 0),
            end: () => input.setSelectionRange(input.value.length, input.value.length),
            highlight: () => input.setSelectionRange(state.search.length, input.value.length)
        },


        // focus
        focus: () => input.focus(),


        // clear all
        clear: () => {
            input.value = '';
            state.key = '';
            state.option = '';
            state.search = '';
            state.selected = -1;
            inputfield.cursor.start();
        }
    }


    // ---



    const options = {

        // open/close options list
        toggle: (value: boolean) => {
            state.open = value;
            input.setAttribute('aria-expanded', state.open);
            (state.open === true) ? component.classList.add('is-expanded') : component.classList.remove('is-expanded');
        },


        // firts option
        first: () => {
            options.deselect();
            state.key = 'ArrowUp';
            state.selected = 0;
            state.option = listoptions[state.selected] || '';
            options.select();
        },


        // next option
        next: () => {
            options.deselect();
            state.key = 'ArrowDown';
            state.selected = ((state.selected + 1) < listoptions.length) ? (state.selected + 1) : 0;
            state.option = listoptions[state.selected] || '';
            options.select();
        },


        // previous option
        previous: () =>  {
            options.deselect();
            state.key = 'ArrowUp';
            state.selected = ((state.selected - 1) > -1) ? (state.selected - 1) : (listoptions.length - 1);
            state.option = listoptions[state.selected] || '';
            options.select();
        },


        // last option
        last: () =>  {
            options.deselect();
            state.key = 'ArrowDown';
            state.selected = (listoptions.length - 1);
            state.option = listoptions[state.selected] || '';
            options.select();
        },


        // select option
        select: () => {
            if (state.option !== '') {
                input.setAttribute('aria-activedescendant', state.option.id);
                input.value = state.option.innerHTML;
                state.option.setAttribute('aria-selected', 'true');
                scroll(list, state.option, state.key);
            } else {
                state.selected = -1;
            }
            state.key = '';
        },


        // deselect option
        deselect: () => {
            if (state.option !== '') {
                state.option.removeAttribute('aria-selected');
                state.option = '';
            }
            input.removeAttribute('aria-activedescendant');
        },


        // reset options
        reset: () => {
            listoptions.forEach((item: any) => item.removeAttribute('aria-selected'));
            input.removeAttribute('aria-activedescendant');
            state.option = '';
            state.selected = -1;
            options.render();
        },


        // submit an option
        submit: (event: any) => {
            const target : any = event.target;

            event.preventDefault();
            event.stopPropagation();

            options.deselect();

            state.option = target;
            state.selected = listoptions.indexOf(target);

            options.select();
            options.toggle(false);

            input.value = target.innerHTML;
            inputfield.cursor.end();
            input.blur();
        },


        // render options
        render: () => {
            const value = input.value.toLowerCase();

            // reset list
            list.innerHTML = "";
            listoptions = [];

            // render list
            listitems.forEach((option: any) => {
                const optionValue = option.innerHTML.toLowerCase();
                if (value === '') {
                    listoptions.push(option);
                    list.append(option);
                } else if (optionValue.indexOf(value) > -1)  {
                    listoptions.push(option);
                    list.append(option);
                }
            });

            // click events
            const elements: Array<HTMLElement> = Array.from(list.querySelectorAll('.c-option'));
            elements.forEach((element: any) => {
                element.onmousedown = (event: MouseEvent) => {
                    options.submit(event);
                }
            });

            // no results
            if (elements.length < 1) {
                list.innerHTML = `<span class="no-results">${list.dataset.noResults}<span>`;
            }
        }
    }



    // ---



    const events: Function = () => {
        input.addEventListener('c-input', (event: any) => {


            if (event.detail.type() === 'blur' || event.detail.key() === 'Enter') {
                component.setAttribute('value', event.detail.value());
            }

            if (event.detail.focus() === true) {
                if (state.open === false) options.toggle(true);
                inputfield.cursor.end();
            } else {
                input.removeAttribute('aria-activedescendant');
                options.toggle(false);
            }

            // key events
            if (event.detail.key() !== false) {
                switch (event.detail.key()) {
                    case 'ArrowUp':
                        event.preventDefault();
                        if (state.open === true) options.previous();
                        if (state.open === false && state.selected === -1) options.previous();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'ArrowRight':
                        event.preventDefault();
                        break;

                    case 'ArrowDown':
                        event.preventDefault();
                        if (state.open === true) options.next();
                        if (state.open === false && state.selected === -1) options.next();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'ArrowLeft':
                        event.preventDefault();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'PageUp':
                        event.preventDefault();
                        if (state.open === true) options.previous();
                        if (state.open === false && state.selected === -1) options.previous();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'PageDown':
                        event.preventDefault();
                        if (state.open === true) options.next();
                        if (state.open === false && state.selected === -1) options.next();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'Backspace':
                        options.reset();
                        state.search = input.value;
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'Delete':
                        options.reset();
                        state.search = input.value;
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'Escape':
                        event.preventDefault();
                        input.value = '';
                        options.reset();
                        inputfield.clear();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'Control':
                        break;

                    case 'Shift':
                        break;

                    case 'Enter':
                        if (state.open === true) {
                            if (state.option !== '') {
                                options.select();
                                options.toggle(false);
                                input.dispatchEvent(new Event('change'));
                            }
                        } else {
                            options.toggle(true);
                        }

                        break;

                    case 'Home':
                        event.preventDefault();
                        if (state.open === true) options.first();
                        if (state.open === false) inputfield.cursor.start();
                        if (state.open === false) options.toggle(true);
                        break;

                    case 'End':
                        event.preventDefault();
                        if (state.open === true) options.last();
                        if (state.open === false) inputfield.cursor.end();
                        if (state.open === false) options.toggle(true);
                        break;

                    default:
                        event.preventDefault();
                        state.search = input.value;
                        options.reset();
                        options.next();
                        inputfield.cursor.highlight();
                        if (state.open === false) options.toggle(true);
                        break;
                }
            }
        });


        // buttonToggle events
        buttonToggle.onmousedown = (event: MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();

            options.toggle((state.open === false) ? true : false);
            inputfield.focus();
            inputfield.cursor.end();
        }


        // buttonClear events (optional)
        if (buttonClear !== null) {
            buttonClear.addEventListener('c-button', (event: any) => {
                if (event.detail.key() !== false) {
                    switch (event.detail.key()) {
                        case ' ':
                            event.cancelBubble = true;
                            inputfield.clear();
                            options.reset();
                            inputfield.focus();
                            inputfield.cursor.start();

                        case 'Enter':
                            event.cancelBubble = true;
                            inputfield.clear();
                            options.reset();
                            inputfield.focus();
                            inputfield.cursor.start();

                        default:
                            break;
                    }
                }

                if (event.detail.type() === 'click') {
                    event.cancelBubble = true;
                    event.preventDefault();
                    event.stopPropagation();

                    inputfield.clear();
                    options.reset();
                    inputfield.focus();
                    inputfield.cursor.start();
                }
            });
        }
    }



    // ---



    const init: Function = () => {
        options.render();
        events();
    }


    // ---



    init();
});
