**!! the switch component is a checkbox with an is="switch" attribute**

Switch is a control that is used to quickly toggle between two possible states.



#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     | -------------     | ------------ | ------------ |
| **css**           | string            | The css attribute can be uses to add an extra style class on the element  |  |
| **id**            | unique identifier | The id attribute specifies a unique id for an HTML element (the value must be unique within the HTML document). | * |
| **is**            | checkbox          | The attribute 'is' determines how the checkbox is rendered. (renders as checkbox) | * |
| **is**            | switch            | The attribute 'is' determines how the checkbox is rendered. (renders as switch) | * |
| **style**         | x                 | The style attribute is not allowed | x |
| **task**          | string            | The 'task' attribute can be used to identify a specific task the element can pass through to other elements (CDS uses the task attribute internally) | * |
| **value**         | string            | The 'value' attribute is optional |  |



#### User input
Users should be able to select the checkbox by clicking on the box directly or by clicking on its label.

| Method            | Element           | Result        |     |      
| -------------     | -------------     | ------------- | --- |
| **click**         | checkbox          | Toggles the checked state |    |
| **spacebar**      | checkbox          | Toggles the checked state |    |
| **tab **          | checkbox          | Adds focus state on the checkbox |    |
| **touch**         | checkbox          | Toggles the checked state |    |
