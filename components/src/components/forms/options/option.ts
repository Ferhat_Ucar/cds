// option.ts
import { ready } from '../../../factories/webcomponent.ready';
import { webcomponent } from '../../../factories/webcomponent';


// ---


export class COption extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed',
            css: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<li class="c-option" id="${this.getAttribute('id') || ''}" role="option">${this.innerHTML.trim() || 'no label for this option'}</li>`;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then(() => webcomponent(this, this.render(), 'custom-replace', this.checkAttributes));
    }
}
