// options.ts
import { ready } from '../../../factories/webcomponent.ready';
import { webcomponent } from '../../../factories/webcomponent';


// ---


export class COptions extends HTMLElement {

    private get checkAttributes() {
        return {
            id: 'not-empty',
            label: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `<ul
                    id="c-${this.getAttribute('id') || ''}"
                    class="c-options"
                    data-no-results="${this.getAttribute('no-results') || ''}"
                    aria-label="${this.getAttribute('label') || 'label is empty'}"
                    role="listbox">${this.innerHTML.trim()}</ul>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                    }
                });
        }
    }
}
