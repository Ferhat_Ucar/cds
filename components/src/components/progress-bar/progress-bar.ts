// c-progress-bar.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CProgressBar extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    static get observedAttributes() {
        return [
            'progress'
        ]
    }


    // ---


    private get progress() {
        return this.getAttribute('progress') || '0';
    }


    // ---


    private get labelProgress() {
        return this.hasAttribute('label-progress') ? true : false;
    }


    // ---


    private get labelMaxRange() {
        return this.getAttribute('label-max-range') || '';
    }


    // ---


    private get bar() {
        return this.querySelector('.c-progress-bar') || false;
    }


    // ---


    private get label() {
        return this.querySelector('.c-progress-label') || false;
    }


    // ---


    private render() {
        let progress: number = parseInt(this.progress);
        if (isNaN(progress) || progress < 0 || progress > 100) progress = 0;
        const labelProgress: string = (this.labelProgress === true)
            ? (progress > 10) ? `${progress}%` : ''
            : '';


        return `
            <div class="c-progress ${this.getAttribute('css') || ''}">
                <div class="c-progress-bar" style="width: ${progress}%" role="progressbar" aria-progressnow="${progress}" aria-progressmin="0" aria-progressmax="100">${labelProgress}</div>
            </div>
            <div class="c-progress-label" aria-live="polite"></div>
        `;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                }
            });
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const bar: any = this.bar;
        const label: any = this.label;

        if (bar) {
            switch(attribute) {
                case 'progress':
                    let progress: number = parseInt(newValue);
                    if (isNaN(progress) || progress < 0 || progress > 100) progress = 0;

                    bar.setAttribute('style', `width:${progress}%`);
                    bar.setAttribute('aria-progressnow', `${progress}`);

                    bar.innerHTML = (this.labelProgress === true) ? ((progress > 10) ? `${progress}%` : '') : '';
                    label.innerHTML = (progress === 100) ? this.labelMaxRange : '';
                    break;

                default:
                    return;
            }
        }
    }
}
