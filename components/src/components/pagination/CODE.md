```html
<c-pagination page="7" results="100" items="10">

    <c-button is="button-ghost-icon" task="previous">
        <span class="hidden-label">Previous page</span>
        <c-icon is="icon-arrow-left"></c-icon>
    </c-button>

    <c-dropdown>
        <c-input is="text" value="" placeholder=" " id="pagination-id-01" name="inputfield"></c-input>

        <c-button is="button-ghost-icon" task="toggle">
            <span class="hidden-label">Toggle</span>
            <c-icon is="icon-arrow-down"></c-icon>
        </c-button>

        <c-options id="id-options-01" label="options-label" label-no-results="No results">
            <c-option id="pagination-option-01">1</c-option>
        </c-options>
    </c-dropdown>

    <c-button is="button-ghost-icon" task="next">
        <span class="hidden-label">Previous page</span>
        <c-icon is="icon-arrow-right"></c-icon>
    </c-button>

</c-pagination>
```
