// pagination.controller.ts
import { broadcast } from '../../factories/event.broadcast'


export const paginationController: Function = ((component: any) => {

    const buttonNext: any = component.querySelector('.c-button[data-task="next"]');
    const buttonPrevious: any = component.querySelector('.c-button[data-task="previous"]');
    const input: any = component.input;


    // ---


    const pagination = {
        next: () => {
            const page: number = component.page;
            const items: number = component.items;
            const results: number = component.results;
            const pages = Math.ceil(results / items);
            const currentPage: number = ((page + 1) < pages) ? (page + 1) : pages;
            //
            buttonNext.disabled = (currentPage >= pages) ? true : false;
            buttonPrevious.disabled = false;
            input.value = currentPage;
            component.setAttribute('page', currentPage.toString());
            input.dispatchEvent(new Event('change'));
        },
        previous: () => {
            const page: number = component.page;
            const items: number = component.items;
            const results: number = component.results;
            const pages = Math.ceil(results / items);
            const currentPage: number = ((page - 1) > 1) ? (page - 1) : 1;
            //
            buttonPrevious.disabled = (currentPage <= 1) ? true : false;
            buttonNext.disabled = false;
            input.value = currentPage;
            component.setAttribute('page', currentPage.toString());
            input.dispatchEvent(new Event('change'));
        },
        select: (value: number) => {
            const items: number = component.items;
            const results: number = component.results;
            const pages = Math.ceil(results / items);
            const currentPage = (value <= pages && value >= 1)
                    ? value
                    : (value > pages)
                        ? pages
                        : 1;

            buttonPrevious.disabled = (currentPage <= 1) ? true : false;
            buttonNext.disabled = (currentPage >= pages) ? true : false;
            component.setAttribute('page', currentPage.toString());
        }
    }



    // ---



    const init: Function = () => {
        // input event
        component.addEventListener('c-dropdown', (event: any) => {
            pagination.select(parseInt(event.detail.value()));

            // broadcast
            broadcast(component, 'c-pagination', null, {
                page: () => event.detail.value()
            });
        });

        //button event
        component.addEventListener('c-button', (event: any) => {
            if (event.detail.task() === 'next' && event.detail.type() === 'click') pagination.next();
            if (event.detail.task() === 'previous' && event.detail.type() === 'click') pagination.previous();
        });
    }



    // ---



    init();
});
