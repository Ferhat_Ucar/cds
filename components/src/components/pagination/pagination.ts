// pagination.ts
import { errorReporter } from '../../factories/error.reporter'
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { paginationController } from './pagination.controller';



// ---



export class CPagination extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-allowed',
            items: 'not-empty',
            css: 'not-allowed',
            style: 'not-allowed',
            page: 'not-empty',
            results: 'not-empty'
        };
    }


    // ---


    private get checkComponents() {
        return {
            required: [
                'c-button[task="next"]',
                'c-button[task="previous"]',
                'c-dropdown'
            ]
        };
    }


    // ---


    private get items() {
        return parseInt(this.getAttribute('items') || '10');
    }


    // ---


    private get page() {
        return parseInt(this.getAttribute('page') || '1');
    }


    // ---


    private get results() {
        return parseInt(this.getAttribute('results') || '1');
    }


    // ---


    private get resultset() {
        const start: number = Math.floor((this.page - 1) * this.items);
        const end: number = ((start + this.items) <  this.results)
            ? (start + this.items)
            : this.results;
        return `
            <span>${start + 1} - ${(end)} of ${this.results} results</span>
            <span>${this.page} of ${Math.ceil(this.results / this.items)} pages</span>
        `;
    }


    // ---


    private get dropdown() {
        return this.querySelector('c-dropdown') || false;
    }


    // ---


    private get input() {
        return this.querySelector('c-input') || false;
    }


    // ---


    private get text() {
        return this.querySelector('.pagination-resultset') || false;
    }


    // ---


    private get list() {
        const options: any = this.querySelector('c-options');
        return (options) ? options.querySelector('ul') || false : false;
    }


    // ---


    private get options() {
        let html: string = '';

        for (let i = 1; i <= Math.ceil(this.results / this.items); i++) {
            html += `<li class="c-option" id="pagination-option-${i}" role="option">${i}</li>`;
        }

        return html;
    }



    // ---


    private controller() {
        paginationController(this);
    }


    // ---


    private render() {
        const input: any = this.input;
        const items: number = this.items;
        const list: any = this.list;
        const page: number = this.page;
        const results: number = this.results;

        // errors: report
        if (page < 0) errorReporter.critical.render('c-pagination', ['page can not be negative']);
        if (page > Math.ceil(results / items)) errorReporter.critical.render('c-pagination', ['current page number is higher then the total number of pages']);

        // update
        if (list) list.innerHTML = this.options;
        if (input) input.setAttribute('value', this.page);

        // render
        return `
            <div class="c-pagination">
                <p class="pagination-resultset">${this.resultset.trim()}</p>
                ${this.innerHTML.trim()}
            </div>
        `
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes, this.checkComponents);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }



    // ---



    // watcher
    static get observedAttributes() {
        return [
            'items',
            'page',
            'results'
        ];
    }



    // ---



    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const dropdown: any = this.dropdown;
        const input: any = this.input;
        const list: any = this.list;
        const text: any = this.text;

        switch(attribute) {
            case 'page':
                if (input) input.setAttribute('value', this.page);
                if (text) text.innerHTML = this.resultset.trim();
                break;


            case 'results':
                if (list) list.innerHTML = this.options;
                if (dropdown) dropdown.setAttribute('listitems', this.results);
                this.setAttribute('page', '1');
                break;


            case 'items':
                // resultset is also updated
                break;

            default:
                return;
        }
    }
}
