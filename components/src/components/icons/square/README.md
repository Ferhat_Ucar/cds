User interface icons are only allowed to be used within a [c-input](/components/forms/input-text) or [c-button](/components/buttons/button-primary) 



#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **is**            | string            | Sets the icon (same as all other icons) | * |
| **css**            | string           | Sets the icon type (e.g. square)|  |
| **label**         | string            | Sets the inner title of the svg |  |
