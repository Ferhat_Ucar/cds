// icon.ts
import { fetchFile } from '../../services/fetch.file'
import { webcomponent } from '../../factories/webcomponent'
import { icons } from '../../resources/icons'
import { cleansvg } from '../../factories/svg.clean'



// ---


export class CIcon extends HTMLElement {


    private render(icon: string, found: boolean) {
        const emoji = found ? '' : 'display-crying-emoji';
        icon = cleansvg(icon, this.getAttribute('label') || '');

        return `
            <i class="c-icon ${this.getAttribute('is') || 'icon-add'} ${emoji} ${this.getAttribute('css') || ''}">${icon.trim()}</i>
        `;
    }


    // ---


    connectedCallback() {
        const icon: string = this.getAttribute('is') || '';
        const theme: string = sessionStorage.getItem(`cds:theme`) || 'calvi';
        const path: string = `/assets/${theme}/assets/svg/`;
        const session: any = sessionStorage.getItem(`cds:${theme}:icon:${icons[icon]}`) || false;

        // fetch: icon file
        if (icons[icon]) {
            if (session) {
               webcomponent(this, this.render(session, true), 'custom-replace');
            } else {
                fetchFile(path + icons[icon]).then((response: any) => {
                    if (response.indexOf('<svg') > -1)  {
                        sessionStorage.setItem(`cds:${theme}:icon:${icons[icon]}`, response);
                        webcomponent(this, this.render(response, true), 'custom-replace');
                    } else {
                        webcomponent(this, this.render('', false), 'custom-replace');
                    }
                });
            }
        } else {
            webcomponent(this, this.render('', false), 'custom-replace');
        }
    }
}
