User interface icons are only allowed to be used within a [c-input](/components/forms/input-text) or [c-button](/components/buttons/button-primary) 



#### Attributes
| Attribute         | Value             | Description  | Required     |
| -------------     |:-------------:    | ------------: |------------:|
| **is**            | string            | Sets the icon | * |
|                   |                   | icon-add      |  |
|                   |                   | icon-arrow-small-up      |  |
|                   |                   | icon-arrow-small-right      |  |
|                   |                   | icon-arrow-small-down      |  |
|                   |                   | icon-arrow-small-left      |  |
|                   |                   | icon-arrow-up                 |  |
|                   |                   | icon-arrow-right      |  |
|                   |                   | icon-arrow-down      |  |
|                   |                   | icon-arrow-left      |  |
|                   |                   | icon-close    |  |
|                   |                   | icon-clear    |  |
|                   |                   | icon-content-indent     |  |
|                   |                   | icon-content-outdent     |  |
|                   |                   | icon-delete    |  |
|                   |                   | icon-drag-and-drop    |  |
|                   |                   | icon-enable-columns     |  |
|                   |                   | icon-fullscreen     |  |
|                   |                   | icon-help     |  |
|                   |                   | icon-home     |  |
|                   |                   | icon-list-numbered     |  |
|                   |                   | icon-list-unordered     |  |
|                   |                   | icon-quickview     |  |
|                   |                   | icon-refresh     |  |
|                   |                   | icon-sort     |  |
|                   |                   | icon-tooltip     |  |
|                   |                   | icon-type-bold     |  |
|                   |                   | icon-type-italic     |  |
|                   |                   | icon-type-underline     |  |
|                   |                   | icon-type-privileges     |  |
|                   |                   | icon-user     |  |
|                   |                   | icon-zoom-in     |  |
|                   |                   | icon-zoom-out     |  |
|                   |                   | icon-search     |  |
|                   |                   | icon-breadcrumb     |  |
| **label**         | string            | Sets the inner title of the svg |  |