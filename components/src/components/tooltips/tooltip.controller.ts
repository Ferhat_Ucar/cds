// tooltip.controller.ts
import { collision } from '../../factories/collision'

export const tooltipController: Function = ((component: any) => {
    const button: any = component.button;
    const tip: any = component.tip;

    const toggle: Function = () => {
        button.classList.toggle('tooltip-is-visible');

        if (button.classList.contains('tooltip-is-visible')) {
            collision(button, tip);
            button.focus();
        }
    };

    const events: Function = () => {

        // mouse events
        button.onmousedown = (event: MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            toggle();
        };
        button.onclick = (event: MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
        };

        // keyboard events
        button.onkeydown = (event: KeyboardEvent) => {
            if (event.key === ' ') toggle();
        };

        // blur
        button.onblur = () => {
            button.classList.remove('tooltip-is-visible');
        };

        // resize
        window.addEventListener("resize", () => collision(button, tip), false);
    };

    events();
});
