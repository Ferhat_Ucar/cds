// tooltip.ts
import { ready } from '../../factories/webcomponent.ready'
import { tooltipController } from './tooltip.controller';
import { webcomponent } from '../../factories/webcomponent';

export class CTooltip extends HTMLElement {
    private static get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed',
            css: 'not-allowed'
        };
    }

    private get button() {
        return this.querySelector('[type="button"]') || false;
    }

    private get tip() {
        return this.querySelector('[role="tooltip"]') || false;
    }

    private render() {
        const id: string = `c-${this.getAttribute('id') || ''}`;

        return `
            <span class="c-tooltip">
                <button id="${id}" type="button" class="c-tooltip_button position-right" aria-labelledby="${id}-tooltip" role="tooltip">
                    <c-icon is="icon-tooltip" css="circle-outline"></c-icon>
                </button>
                <span class="c-tooltip_tip" id="${id}-tooltip" role="tooltip">${this.innerHTML.trim()}</span>
            </span>
        `;
    }

    private controller() {
        tooltipController(this);
    }

    connectedCallback() {
        // if the components does not exists in the DOM
        if (!this.hasAttribute('ready')) {
            console.log(this);
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(
                            this,
                            this.render(),
                            'custom-append',
                            CTooltip.checkAttributes
                        );
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }

    disconnectedCallback() {}
}
