// loader.ts
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CLoader extends HTMLElement {


    private render() {
        return `
            <div class="c-loader ${this.getAttribute('css') || ''}"></div>
        `;
    }


    // ---


    connectedCallback() {
        webcomponent(this, this.render(), 'custom-replace');
    }
}
