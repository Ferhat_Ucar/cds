// tabs.controller.ts


export const tabsController: Function = ((component: any, number: any = 0) => {


    let focused: number = 0;
    let selected: number = 0;
    const tabs: any = component.tabs;
    const panels: any = component.panels;


    // ---


    const deselect = () => {
        tabs.forEach((tab: any) => {
            const button : HTMLElement = tab;
            button.setAttribute('aria-selected', 'false');
            button.setAttribute('tabindex', '-1');
        })
        panels.forEach((panel: any) => {
            const div : HTMLElement = panel;
            div.setAttribute('hidden', 'hidden');
        })
    }


    // ---


    const select = () => {
        deselect();

        const button : HTMLElement = tabs[selected];
        const panel : HTMLElement = panels[selected];

        button.setAttribute('aria-selected', 'true');
        button.removeAttribute('tabindex');

        if (panel) panel.removeAttribute('hidden');
    }


    // ---


    const focus = () => {
        tabs[focused].focus();
    }


    // ---


    const events: Function = () => {
        tabs.forEach((tab: any) => {

            // click events
            tab.onclick = (event: Event) => {
                const element : any = event.target;
                selected = tabs.indexOf(element);
                focused = selected;
                select();
            }

            // keyboard events
            tab.onkeydown = (event: KeyboardEvent) => {
                // arrow: left
                if (event.key === 'ArrowLeft') {
                    focused = (focused > 0) ? (focused - 1) : (tabs.length - 1);
                    focus();
                }
                // arrow: right
                if (event.key === 'ArrowRight') {
                    focused = (focused === (tabs.length - 1)) ? 0 : (focused + 1);
                    focus();
                }
                // spacebar
                if (event.key === ' ' || event.key === 'Spacebar') {
                    selected = focused;
                    select();
                    focus();
                }
            }
        })
    }


    // ---


    const init: Function = () => {

        // check if a tab is selected
        if (tabs.filter((tab: any) => tab.getAttribute('aria-selected') === 'true').length < 1)  {
            select();
        }
        if (number !== 0) {
            selected = number - 1;
            select();
        }

        // set events
        events();
    }


    init();
});
