// tab-panel.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CTabPanel extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        const id: string = this.getAttribute('id') || '';

        return `
            <div
                id="c-${id}"
                class="c-tab-panel ${this.getAttribute('css') || ''}"
                role="tabpanel"
                aria-labelledby="c-${id.replace('-panel', '')}"
                tabindex="0">
                    ${this.innerHTML.trim() || 'No content set for this tab panel'}
            </div>
        `;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    // timeout is for <markdown content
                    setTimeout(() => {
                        webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                    }, 1);

                }
            });
    }
}
