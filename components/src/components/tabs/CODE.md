```html
<c-tabs>
    <!-- tab -->
    <c-tab id="tabID">Tab label 1</c-tab>
    <c-tab id="tabID">Tab label 2</c-tab>
    <c-tab id="tabID">Tab label 3</c-tab>

    <!-- tab-panel -->
    <c-tab-panel id="tabID-panel">Content for first tab goes here.</c-tab-panel>
    <c-tab-panel id="tabID-panel">Content for second tab goes here.</c-tab-panel>
    <c-tab-panel id="tabID-panel">Content for third tab goes here.</c-tab-panel>
</c-tabs>
```
