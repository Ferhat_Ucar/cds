// tab.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class CTab extends HTMLElement {


    private get checkAttributes() {
        return {
            id: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <button type="button"
                id="c-${this.getAttribute('id') || ''}"
                class="c-tab ${this.getAttribute('css')}"
                role="tab"
                task="tab"
                aria-selected="${this.hasAttribute('selected') ? 'true' : 'false'}"
                aria-controls="c-${this.getAttribute('id')}-panel">
                    ${this.innerHTML.trim() || 'No label set for this tab'}
            </button>
        `;
    }


    // ---


    connectedCallback() {
        ready(this, 'components')
            .then((result: boolean) => {
                if (result) {
                    webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                }
            });
    }
}
