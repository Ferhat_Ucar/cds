// tabs.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'
import { tabsController } from './tabs.controller'


// ---


export class CTabs extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }

    // ---


    static get observedAttributes() {
        return [
            'selected'
        ]
    }


    // ---


    private get tabs() {
        return Array.from(this.querySelectorAll('[role="tab"]')) || [];
    }


    // ---


    private get panels() {
        return Array.from(this.querySelectorAll('[role="tabpanel"]')) || [];
    }


    // ---


    private render() {
        return `<div class="c-tabs ${this.getAttribute('css')}" role="tablist">${this.innerHTML.trim() || 'No Tab content'}</div>`;
    }


    // ---


    private controller() {
        tabsController(this, 0);
    }


    // ---



    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.controller();
                    }
                });
        } else {
            this.controller();
        }
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const tabs: any = this.tabs;
        const val = newValue !== null;

        switch(attribute) {
            case 'selected':
                if (parseInt(newValue) > 0 && parseInt(newValue) <= tabs.length) {
                    tabsController(this, parseInt(newValue));
                }
                break;

            default:
                return;
        }
    }
}
