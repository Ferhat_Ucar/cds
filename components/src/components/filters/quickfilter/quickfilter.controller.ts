// quickfilter.ts
import { broadcast } from '../../../factories/event.broadcast'


export const quickfilterController: Function = ((component: any) => {


    const events: Function = () => {
        const checkbox: any = component.checkbox;

        if (checkbox) {

            // event: function
            const broadcaster: any = (event: any) => {

                // event: broadcast
                broadcast(checkbox, 'c-quickfilter', event, {
                    checked: () => checkbox.checked,
                    task: () => checkbox.getAttribute('data-task') || false,
                    value: () => checkbox.value || false,
                });

                // update: component
                (checkbox.checked)
                    ? component.setAttribute('checked', '')
                    : component.removeAttribute('checked');
            };

            // event: handlers
            checkbox.addEventListener('change', broadcaster, true);
        }
    }


    // ---


    events();
});
