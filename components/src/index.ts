// index.ts


// --- theming
import { theming } from './factories/theme';



// --- components:

// components: accordion
import { CAccordion } from './components/accordion/accordion';
import { CAccordionRegion } from './components/accordion/accordion-region';
import { CAccordionButton } from './components/accordion/accordion-button';
// components: breadcrumbs
import { CBreadcrumb } from './components/breadcrumbs/breadcrumb';
import { CBreadcrumbs } from './components/breadcrumbs/breadcrumbs';
// components: buttons
import { CButton } from './components/buttons/button';
// components: charts:
import { CChart } from './components/charts/chart';
import { CChartItem } from './components/charts/chart-item';
// components: context-menu
import { CContextMenu } from './components/context-menu/context-menu';
// components: divider
import { CDivider } from './components/dividers/divider';
// components: filters
import { CQuickfilter } from './components/filters/quickfilter/quickfilter';
// components: form fieldset
import { CFieldset } from './components/forms/fieldset/fieldset';
// components: form datepicker
import { CDatepicker } from './components/forms/datepicker/datepicker';
// components: form dropdown
import { CDropdown } from './components/forms/dropdown/dropdown';
// components: form input-checkbox
import { CCheckbox } from './components/forms/input-checkbox/checkbox';
// components: form input-radio
import { CRadio } from './components/forms/input-radio/radio';
// components: form input-text
import { CInput } from './components/forms/input-text/input';
// components: form input-textarea
import { CTextarea } from './components/forms/input-textarea/textarea';
// components: form options-list
import { COption } from './components/forms/options/option';
import { COptions } from './components/forms/options/options';
// components: form search
import { CSearch } from './components/forms/search/search';
// components: form upload
import { CUpload } from './components/forms/upload/upload';
// components: icons
import { CIcon } from './components/icons/icon';
// components: links
import { CLink } from './components/text/links/link';
import { CSkipToLink } from './components/text/skiptolinks/skiptolink';
// components: list
import { CList } from './components/lists/list';
import { CListItem } from './components/lists/list-item';
// components: loaders
import { CLoader } from './components/loaders/loader';
// components: modal
import { CModal } from './components/modal/modal';
// components: notification
import {CNotification} from './components/notification/notification';
// components: pagination
import { CPagination } from './components/pagination/pagination';
// components: logo
import { CLogo } from './components/logo/logo';
// components: progress-bar
import { CProgressBar } from './components/progress-bar/progress-bar';
// components: tab
import { CTab } from './components/tabs/tab';
import { CTabPanel } from './components/tabs/tab-panel';
import { CTabs } from './components/tabs/tabs';
// components: Table
import { CTableCell } from './components/table/table-cell/table.cell';
import { CTableHeader } from './components/table/table-header/table.header';
import { CTableRow } from './components/table/table-row/table.row';
import { CTableBody } from './components/table/table-body/table.body';
import { CTableHead } from './components/table/table-head/table.head';
import { CTable } from './components/table/table';
// components: tile
import { CTile } from './components/tile/tile';
import { CTileicon } from './components/tile/tile-icons/tile-icon';
// components: tooltip
import { CTooltip } from './components/tooltips/tooltip';



// --- blocks

// blocks: button-groups
import { BlockButtonGroup } from './blocks/button-groups/button-group';
// components: accordion-group
import { CAccordionGroup } from './components/accordion/accordion-group';
// blocks: grid
import { BlockGrid } from './blocks/grid/grid';
// blocks: grid-action-bar
import { BlockGridActionBar } from './blocks/grid-action-bar/grid-action-bar';
// blocks: header
import { BlockHeader } from './blocks/header/header';
// blocks: layout
import { BlockLayout } from './blocks/layout/layout';
// blocks: tile-group
import { BlockTileGroup } from './blocks/tile-group/tile-group';


// Angular needs to be loaded first
setTimeout(() => {
    // theming
    theming();
    // components
    window.customElements.define('c-accordion-region', CAccordionRegion);
    window.customElements.define('c-accordion-button', CAccordionButton);
    window.customElements.define('c-accordion', CAccordion);
    window.customElements.define('c-breadcrumb', CBreadcrumb);
    window.customElements.define('c-breadcrumbs', CBreadcrumbs);
    window.customElements.define('c-button', CButton);
    window.customElements.define('c-checkbox', CCheckbox);
    window.customElements.define('c-datepicker', CDatepicker);
    window.customElements.define('c-divider', CDivider);
    window.customElements.define('c-fieldset', CFieldset);
    window.customElements.define('c-chart', CChart);
    window.customElements.define('c-chart-item', CChartItem);
    window.customElements.define('c-icon', CIcon);
    window.customElements.define('c-input', CInput);
    window.customElements.define('c-table-cell', CTableCell);
    window.customElements.define('c-table-header', CTableHeader);
    window.customElements.define('c-table-row', CTableRow);
    window.customElements.define('c-table-head', CTableHead);
    window.customElements.define('c-table-body', CTableBody);
    window.customElements.define('c-link', CLink);
    window.customElements.define('c-skip-to-link', CSkipToLink);
    window.customElements.define('c-list', CList);
    window.customElements.define('c-list-item', CListItem);
    window.customElements.define('c-loader', CLoader);
    window.customElements.define('c-logo', CLogo);
    window.customElements.define('c-modal', CModal);
    window.customElements.define('c-option', COption);
    window.customElements.define('c-options', COptions);
    window.customElements.define('c-progress-bar', CProgressBar);
    window.customElements.define('c-search', CSearch);
    window.customElements.define('c-quickfilter', CQuickfilter);
    window.customElements.define('c-radio', CRadio);
    window.customElements.define('c-tab', CTab);
    window.customElements.define('c-tab-panel', CTabPanel);
    window.customElements.define('c-textarea', CTextarea);
    window.customElements.define('c-tile', CTile);
    window.customElements.define('c-tile-icon', CTileicon);
    window.customElements.define('c-notification', CNotification);
    window.customElements.define('c-tooltip', CTooltip);
    window.customElements.define('c-upload', CUpload);
    // combined components
    window.customElements.define('c-context-menu', CContextMenu);
    window.customElements.define('c-dropdown', CDropdown);
    window.customElements.define('c-table', CTable);
    window.customElements.define('c-pagination', CPagination);
    window.customElements.define('c-tabs', CTabs);
    window.customElements.define('c-accordion-group', CAccordionGroup);
    // blocks
    window.customElements.define('block-button-group', BlockButtonGroup);
    window.customElements.define('block-grid', BlockGrid);
    window.customElements.define('block-grid-action-bar', BlockGridActionBar);
    window.customElements.define('block-header', BlockHeader);
    window.customElements.define('block-layout', BlockLayout);
    window.customElements.define('block-tile-group', BlockTileGroup);
}, 1);
