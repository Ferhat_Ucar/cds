// grid-action-bar.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class BlockGridActionBar extends HTMLElement {


    private get checkAttributes() {
        return {
            'label-selected-single': 'not-empty',
            'label-selected-multiple': 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private get labelSelectedSingle() {
        return this.getAttribute('label-selected-single') || 'item selected'
    }


    // ---


    private get labelSelectedMultiple() {
        return this.getAttribute('label-selected-multiple') || 'items selected'
    }


    // ---


    private render() {
        let html: string = this.innerHTML || 'No html for this Grid Action Bar';
        html = html.replace('<p>', '<p aria-live="polite">');
        return `<div class="block-grid-action-bar">${html}</div>`;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                if (result) webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
            });
        }
    }

    // ---


    static get observedAttributes() {
        return [
            'selected'
        ]
    }


    // ---


    attributeChangedCallback(attribute: string, oldValue: any, newValue: any) {
        const text: any = document.querySelector("p");

        switch(attribute) {
            case 'selected':
                if (text) text.innerHTML = (newValue === '0')
                    ? ''
                    : (parseInt(newValue) === 1)
                        ? `1 ${this.labelSelectedSingle}`
                        : `${newValue} ${this.labelSelectedMultiple}`
                break;

            default:
                return;
        }
    }
}
