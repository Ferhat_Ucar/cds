// blocks/tile-group.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class BlockTileGroup extends HTMLElement {


    private get checkAttributes() {
        return {
            css: 'not-allowed',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <div class="block-tile-group">${this.innerHTML.trim() || 'No html for this tile group'}</div>
        `;
    }




    // ---



    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                    if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                });
        }
    }
}
