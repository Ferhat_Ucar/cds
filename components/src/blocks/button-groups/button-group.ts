// button-group.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class BlockButtonGroup extends HTMLElement {


    private get checkAttributes() {
        return {
            is: ['button-group-align-left',
                'button-group-align-center',
                'button-group-align-right',
                'button-group-inline'],
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <div class="block-button-group is-${this.getAttribute('is') || ''}">${this.innerHTML.trim() || 'No html for this button-group'}</div>
        `;
    }


    // ---


    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components').then((result: boolean) => {
                    if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                });
        }
    }
}
