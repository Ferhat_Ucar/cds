// block-layout.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class BlockLayout extends HTMLElement {


    private get checkAttributes() {
        return {
            css: 'not-empty',
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <div class="block-layout ${this.getAttribute('css') || ''}">${this.innerHTML.trim() || 'No html for this layout block'}</div>
        `;
    }




    // ---



    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components and blocks').then((result: boolean) => {
                    if (result) webcomponent(this, this.render(), 'custom-replace', this.checkAttributes);
                });
        }
    }
}
