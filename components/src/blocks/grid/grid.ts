// grid.ts
import { ready } from '../../factories/webcomponent.ready'
import { webcomponent } from '../../factories/webcomponent'


// ---


export class BlockGrid extends HTMLElement {


    private get checkAttributes() {
        return {
            style: 'not-allowed'
        };
    }


    // ---


    private render() {
        return `
            <div class="block-grid">${this.innerHTML.trim() || 'No html for this grid'}</div>
        `;
    }


    // ---


    private listners() {
        this.addEventListener('c-table', (event: any) => {
            const actionBar: any = this.querySelector('block-grid-action-bar') || false;

            if (actionBar && event.detail.selectAllRows) {
                (event.detail.selectAllRows() === true) ? actionBar.setAttribute('visible', '') : actionBar.removeAttribute('visible');
            }
        });
    }



    // ---



    connectedCallback() {
        if (!this.hasAttribute('ready')) {
            ready(this, 'components and blocks')
                .then((result: boolean) => {
                    if (result) {
                        webcomponent(this, this.render(), 'custom-append', this.checkAttributes);
                        this.listners();
                    }
                });
        } else {
            this.listners();
        }
    }



    // ---


    disconnectedCallback() {}
}
