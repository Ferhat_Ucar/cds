// tile-icons.ts
export const icons: object = {
    'adaptiveportalconfiguration'   : 'icon__adaptiveportalconfiguration.svg',
    'advancedreporting'             : 'icon__advancedreporting.svg',
    'asset'                         : 'icon__asset.svg',
    'assetOverview'                 : 'icon__assetOverview.svg',
    'assetsgrid_custom'             : 'icon__assetsgrid_custom.svg',
    'assetsgrid'                    : 'icon__assetsgrid.svg',
    'assetspivot_custom'            : 'icon__assetspivot_custom.svg',
    'assetspivot'                   : 'icon__assetspivot.svg',


    'ban'                           : 'icon__ban.svg',
    'budgetcontrol'                 : 'icon__budgetcontrol.svg',


    'callNowTransfer'               : 'icon__callNowTransfer.svg',
    'cdrDownload'                   : 'icon__cdrDownload.svg',
    'cdrgrid_custom'                : 'icon__cdrgrid_custom.svg',
    'cdrgrid'                       : 'icon__cdrgrid.svg',
    'cdrpivot_custom'               : 'icon__cdrpivot_custom.svg',
    'cdrpivot'                      : 'icon__cdrpivot.svg',
    'changeOverview'                : 'icon__changeOverview.svg',
    'costcenter'                    : 'icon__costcenter.svg',
    'costcentercosts'               : 'icon__costcentercosts.svg',
    'costcentergrid_custom'         : 'icon__costcentergrid_custom.svg',
    'costcentergrid'                : 'icon__costcentergrid.svg',
    'costcenterpivot_custom'        : 'icon__costcenterpivot_custom.svg',
    'costcenterpivot'               : 'icon__costcenterpivot.svg',


    'discounts'                     : 'icon__discounts.svg',
    'DisputeManagementAdminTools'   : 'icon__DisputeManagementAdminTools.svg',
    'DisputeManagementMaintenance'  : 'icon__DisputeManagementMaintenance.svg',
    'downloadcenter'                : 'icon__downloadcenter.svg',
    'downloadtilesinvpdf'           : 'icon__downloadtilesinvpdf.svg',
    'downloadtilesinvsource'        : 'icon__downloadtilesinvsource.svg',


    'fixedSubscription'             : 'icon__fixedSubscription.svg',
    'FP'                            : 'icon__FP.svg',


    'genericConfigManagement'       : 'icon__genericConfigManagement.svg',


    'help'                          : 'icon__help.svg',
    'hrupload'                      : 'icon__hrupload.svg',

    'internationalgrid_custom'      : 'icon__internationalgrid_custom.svg',
    'internationalgrid'             : 'icon__internationalgrid.svg',
    'internationalpivot_custom'     : 'icon__internationalpivot_custom.svg',
    'internationalpivot'            : 'icon__internationalpivot.svg',

    'manageAssetClass'              : 'icon__manageAssetClass.svg',
    'manageBlacklist'               : 'icon__manageBlacklist.svg',
    'managedownloadtypes'           : 'icon__managedownloadtypes.svg',
    'managepublishedprofilereports' : 'icon__managepublishedprofilereports.svg',   
    'managerawdataconfiguration'    : 'icon__managerawdataconfiguration.svg',
    'manageSpecifiedProfileImpersonation'             : 'icon__manageSpecifiedProfileImpersonation.svg',
    'manageWhiteList'               : 'icon__manageWhiteList.svg',
    'ML'                            : 'icon__ML.svg',   
    'mobilegrid_custom'             : 'icon__mobilegrid_custom.svg',
    'mobilegrid'                    : 'icon__mobilegrid.svg',
    'mobilepayment'                 : 'icon__mobilepayment.svg',
    'mobilepivot_custom'            : 'icon__mobilepivot_custom.svg',   
    'mobilepivot'                   : 'icon__mobilepivot.svg',
    'mobileSubscription'            : 'icon__mobileSubscription.svg',
    'mydownloads'                   : 'icon__mydownloads.svg',

    'locations'                     : 'icon__locations.svg',

    'oneoff'                        : 'icon__oneoff.svg',
    'orchestrationBillCycleConsolidatedView'  : 'icon__orchestrationBillCycleConsolidatedView.svg',
    'orchestrationBillCycleOverview'  : 'icon__orchestrationBillCycleOverview.svg',
    'orchestrationCustomerFeedOverview'            : 'icon__orchestrationCustomerFeedOverview.svg',   
    'otherCosts'                   : 'icon__otherCosts.svg',
    'otherSubscription'            : 'icon__otherSubscription.svg',
    'outputmanagement'             : 'icon__outputmanagement.svg',    


    'Package-Basic'                : 'icon__Package-Basic.svg',
    'Package-Extreme'              : 'icon__Package-Extreme.svg',
    'Package-Professional'         : 'icon__Package-Professional.svg',
    'Package-Ultimate'             : 'icon__Package-Ultimate.svg',
    'packages'                     : 'icon__packages.svg',  
    'pe'                           : 'icon__pe.svg',
    'person'                       : 'icon__person.svg',
    'persongrid_custom'            : 'icon__persongrid_custom.svg',    
    'persongrid'                   : 'icon__persongrid.svg',
    'personpivot_custom'           : 'icon__personpivot_custom.svg',
    'personpivot'                  : 'icon__personpivot.svg',
    'piegrid'                      : 'icon__piegrid.svg',   
    'plustile'                     : 'icon__plustile.svg',
    'pievot'                       : 'icon__piepivot.svg',
    'pievot_custom'                : 'icon__piepivot_custom.svg',

    'productgrid_custom'           : 'icon__productgrid_custom.svg',
    'productgrid'                  : 'icon__productgrid.svg',
    'productManagement'            : 'icon__productManagement.svg',    
    'productpivot_custom'          : 'icon__productpivot_custom.svg',
    'productpivot'                 : 'icon__productpivot.svg',
    'profilemanagement'            : 'icon__profilemanagement.svg',
    'promotionadvancedreporting'   : 'icon__promotionadvancedreporting.svg',   
    'promotionbudgetcontrol'       : 'icon__promotionbudgetcontrol.svg',
    'promotioncostcenter'          : 'icon__promotioncostcenter.svg',
    'promotiondownloadcenter'      : 'icon__promotiondownloadcenter.svg',    
    'promotionhrupload'            : 'icon__promotionhrupload.svg',
    'promotionoutputmanagement'    : 'icon__promotionoutputmanagement.svg',   
    'promotionperson'              : 'icon__promotionperson.svg',
    'promotionrawdataexportinvoiceoverview'            : 'icon__promotionrawdataexportinvoiceoverview.svg',
    'promotionuser'                 : 'icon__promotionuser.svg',   

    'rawdataexportinvoiceinsight'   : 'icon__rawdataexportinvoiceinsight.svg',
    'rawdataexportinvoiceoverview'  : 'icon__rawdataexportinvoiceoverview.svg',   
    'recurring'                     : 'icon__recurring.svg',
    'reportTemplates'               : 'icon__reportTemplates.svg',
    'ReportManager'                 : 'icon__ReportManager.svg',  

    'servicegrid_custom'            : 'icon__servicegrid_custom.svg',
    'servicegrid'                   : 'icon__servicegrid.svg',   
    'serviceNumber'                 : 'icon__serviceNumber.svg',
    'servicepivot_custom'           : 'icon__servicepivot_custom.svg',
    'servicepivot'                  : 'icon__servicepivot.svg',  
    'splitbilling'                  : 'icon__splitbilling.svg',  


    'topCallingAbroad'              : 'icon__top10.svg',
    'topCallingWhileAbroad'         : 'icon__top10.svg',
    'topDataNational'               : 'icon__top10.svg',
    'topRoamingData'                : 'icon__top10.svg',
    'topServiceNumbers'             : 'icon__top10.svg',
    'topSMS'                        : 'icon__top10.svg',
    'topSpeechNational'             : 'icon__top10.svg',
    'topTotalCosts'                 : 'icon__top10.svg',
    'TE'                            : 'icon__TE.svg',
    'tilemanagement'                : 'icon__tilemanagement.svg',
    'totalcostdistribution'         : 'icon__totalcostdistribution.svg',

    'quickcheck'                    : 'icon__quickcheck.svg',
 
    'usage'                         : 'icon__usage.svg',
    'usageInternational'            : 'icon__usageInternational.svg',
    'user'                          : 'icon__user.svg',


    'watches'                       : 'icon__watches.svg',
    'watchesAdminTools'             : 'icon__watchesAdminTools.svg'

    

};
