// format.date

export const formatDate: Function = (value: string, format: string) => {


    // set: variables
    const seperator: string = format.replace(/dd/g,'').replace(/mm/g,'').replace(/yy/g,'')[0];
    const formatter: string[] = format.split(seperator);


    // calculate: date
    const date: any = new Date(value);
    const day: any = (date.getDate() < 10) ? `0${date.getDate()}` : `${date.getDate()}`;
    const month: any = ((date.getMonth() + 1) < 10) ? `0${(date.getMonth() + 1)}` : `${(date.getMonth() + 1)}`;
    const year: any = `${date.getFullYear()}`;


    // format: date
    formatter.forEach((item: string, index: number = 0) => {
        if (item === 'dd') formatter[index] = day;
        else if (item === 'mm') formatter[index] = month;
        else formatter[index] = year;
    })


    // retrun: date
    return formatter.join(seperator);
}
