// file.size.ts

export const filesize = (bytes: number) => {
    if (bytes < 1024) return bytes + " Bytes";
    else if (bytes < 1048576) return(bytes / 1024).toFixed(0) + " Kb.";
    else if (bytes < 1073741824) return(bytes / 1048576).toFixed(0) + " Mb.";
    else return (bytes / 1073741824).toFixed(0) + " Gb.";
}
