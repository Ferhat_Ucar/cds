// clean.svg.ts

export const cleansvg: Function = (svg: string = '', label: string = '') => {

    //-- enrich svg with attributes
    svg = svg.replace('<svg', '<svg aria-hidden="true" role="image"');

    //-- clean styling (if applicable)
    svg = svg.replace(/<style>[\s\S]*?<\/style>/g, '');


    //-- clean comments (if applicable)
    svg = svg.replace(/<!--[\s\S]*?-->/g, '');

    //-- replace inner title
    svg = svg.replace(/<title>[\s\S]*?<\/title>/g, '<title>' + label + '</title>');

    return svg;

}
