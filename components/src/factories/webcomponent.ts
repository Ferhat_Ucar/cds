// webcomponent.ts
import { attributesValidator } from './validate.attributes'
import { componentsValidator } from './validate.components'

export const webcomponent: Function = (
    parent: any,
    component: any,
    method: string = 'append',
    validateAttributes: any = false,
    validateComponents: any = false
) => {
    // validate: attributes
    if (validateAttributes) attributesValidator(parent, validateAttributes);
    if (validateComponents) componentsValidator(parent, validateComponents);

    // DOM insert: component
    switch(method) {
        case 'custom-append':
            parent.setAttribute('ready','');
            parent.innerHTML = component.trim();
        break;
        case 'custom-replace':
            if (parent.parentNode !== null) {
                const template: HTMLTemplateElement = document.createElement('template');
                template.innerHTML = component.trim();
                parent.parentNode.replaceChild(template.content, parent);
            }
        break;
        default:
            return;
    }
};
