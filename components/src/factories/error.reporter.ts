export const errorReporter: any = {

    // throw a warning
    warning: {

        html: (text: string) => `<div class="calvi-critical-warning">${text}</div>`,

        render: (title: string, messages: string[]) => {
            let text : string = `<h4>Warning: ${title}</h4>`;

            for (const message of messages) {
                text = text + message + '<br />';
                console.warn(title, message);
            }

            // render
            const element : any = document.getElementById('calvi-critical-messages');
            if (element) {
                element.insertAdjacentHTML('afterbegin', errorReporter.warning.html(text));
            } else {
                document.getElementsByTagName('BODY')[0].insertAdjacentHTML('afterbegin', `<div id="calvi-critical-messages">${errorReporter.warning.html(text)}</div>`);
            }
        }
    },


    // throw an error
    critical: {
        html: (text: string) => `<div class="calvi-critical-error">${text}</div>`,

        render : (title: string, messages: string[]) => {
            let text : string = `<h4>Error: ${title}</h4>`;
            let error : string = '';

            for (const message of messages) {
                text    = text + message + '<br />';
                error   = error + message + ' ';
            }

            // render
            const element : any = document.getElementById('calvi-critical-messages');
            if (element) {
                element.insertAdjacentHTML('afterbegin', errorReporter.critical.html(text + text));
            } else {
                document.getElementsByTagName('BODY')[0].insertAdjacentHTML('afterbegin', `<div id="calvi-critical-messages">${errorReporter.critical.html(text)}</div>`);
            }
            throw new Error(error);
        }
    }
};
