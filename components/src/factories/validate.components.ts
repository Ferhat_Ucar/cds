// validate.components.ts
import { errorReporter } from './error.reporter';



// ---



export const componentsValidator: Function = (parent: HTMLElement, components: object) => {

    const error: any = {
        state: false,
        type: 'warning',
        message: ['']
    }


    // ---


    let nodelist: any = Array.from(parent.children);
    const required: string[] = components['required'] || [];
    const optional: string[] = components['optional'] || [];


    // ---


    if (nodelist.length === 0 && required !== [] && required.length !== 0) {
        error.state = true;
        error.type = 'critical';
        error.message.push(`<strong>Child Nodes</strong>: The component must contain explicit child nodes`);
    }


    // ---


    if (nodelist.length > 0) {

        // clean whitespaces
        nodelist.forEach((node: any, index: number) => {
            if (node.textContent && node.nodeValue !== null) {
                if(node.nodeValue.trim() === "") {
                    nodelist.splice(index, 1);
                }
            }
        });


        // check: required elements
        required.forEach((item: string) => {
            const elements: any = parent.querySelectorAll(item);

            if (nodelist.indexOf(elements[0]) < 0) {
                error.state = true;
                error.type = 'critical';
                error.message.push(`<strong>Childnode</strong>: The component must contain ${item}`);
            } else {
                nodelist = nodelist.filter((node: any) => !node.matches(item));
            }
        });

        // check: optional elements
        optional.forEach((item: string) => {
            nodelist = nodelist.filter((node: any) => !node.matches(item));
        })



        nodelist.forEach((node: any) => {
            error.state = true;
            error.type = 'critical';
            error.message.push(`<strong>Childnode</strong>: The component can not contain ${node.nodeName}`);
        });
    }


    // ---


    // report: error
    if (error.state) errorReporter[error.type].render(parent.nodeName, error.message);
}
