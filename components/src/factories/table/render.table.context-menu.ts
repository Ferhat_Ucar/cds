// render.table.context-menu

export const tableContextMenu = (object: any, index: number) => {


    // set: variables
    let icon: string = '';
    let items: string = '';
    let context: string = '';


    // create: context menu
    if (object.context) {
        object.context.items.forEach((item: any, i: number) => {
            if (item.link) {
                icon = (item.link.icon !== null) ? `<c-icon is="${item.link.icon}"></c-icon>` : '';
                items += `<c-list-item><c-link link="${item.link.path}" css="${item.link.css}">${icon}${item.link.label}</c-link></c-list-item>`;
            }

            if (item.button) {
                icon = (item.button.icon !== null) ? `<c-icon is="${item.button.icon}"></c-icon>` : '';
                items += `<c-list-item><c-button is="${item.button.is}" css="${item.button.css}" task="${item.button.task}">${icon}${item.button.label}</c-button></c-list-item>`;
            }


            if (item.checkbox) {
                const checked: string = (item.checkbox.checked === true) ? 'checked' : '';
                items += `<c-list-item><c-checkbox is="${item.checkbox.is}" css="${item.checkbox.css}" task="${item.checkbox.task}" id="table-context-menu-checkbox-${i}" ${checked}>${item.checkbox.label}</c-checkbox></c-list-item>`;
            }
        });


        icon = (object.context.button.icon !== null) ? `<c-icon is="${object.context.button.icon}"></c-icon>` : '';


        context = `
            <c-context-menu id="table-context-menu-${index}">
                <c-button is="${object.context.button.is}" css="${object.context.button.css}" task="context-menu-toggle">
                    ${object.context.button.label}
                    ${icon}
                </c-button>
                <c-list css="${object.context['list-css']}" is="context">
                    ${items}
                </c-list>
            </c-context-menu>`;
    }


    // return: context menu
    return context.trim();
}
