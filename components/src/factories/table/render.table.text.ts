// render.table.text
import { objectMapper } from '../object.mapper'


// ---


export const tableText = (data: any, object: any) => {

    // set: variables
    let text: string = '';


    // render: text
    if (object.text.datasource) {
        text = objectMapper(object.text.datasource, data);
        if (typeof text === 'number') text = Number(text).toFixed(2);
    }


    // return: text
    return text;
}
