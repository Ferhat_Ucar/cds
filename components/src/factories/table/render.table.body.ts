// parse.table.ts
import { tableAmount } from './render.table.amount'
import { tableButton } from './render.table.button'
import { tableCheckbox } from './render.table.checkbox'
import { tableContextMenu } from './render.table.context-menu'
import { tableDate } from './render.table.date'
import { tableLink } from './render.table.link'
import { tableText } from './render.table.text'



// ---



export const renderTableBody = (data: any, template: any, start: number, nrOfRowsPerPage: number, selected: string[]) => {


    // set: variables
    let rows: string = '';
    const results: Array<any> = data.slice(((start - 1) * nrOfRowsPerPage), (((start - 1) * nrOfRowsPerPage) + nrOfRowsPerPage));


    // create: body
    const body: HTMLTableSectionElement = document.createElement('tbody');
    body.className = `c-table-body`;


    // render: body html
    if (template[0].body) {
        results.forEach((datarow: any, index: number) => {
            let cells: string = ``;

            template[0].body.forEach((cell: any) => {
                if (cell.amount)    cells += `<td data-label="${cell.amount.label || ''}" class="c-table-cell content-is-amount">${tableAmount(datarow, cell)}</td>`;
                if (cell.button)    cells += `<td class="c-table-cell content-is-icon-button">${tableButton(cell, index)}</td>`;
                if (cell.checkbox)  cells += `<td class="c-table-cell content-is-checkbox">${tableCheckbox(datarow, cell, index, selected)}</td>`;
                if (cell.context)   cells += `<td class="c-table-cell content-is-context-menu">${tableContextMenu(cell, index)}</td>`;
                if (cell.date)      cells += `<td data-label="${cell.date.label || ''}" class="c-table-cell content-is-date">${tableDate(datarow, cell)}</td>`;
                if (cell.link)      cells += `<td data-label="${cell.link.label || ''}" class="c-table-cell content-is-link">${tableLink(datarow, cell)}</td>`;
                if (cell.text)      cells += `<td data-label="${cell.text.label || ''}" class="c-table-cell content-is-text">${tableText(datarow, cell)}</td>`;
            });

            rows += `<tr class="c-table-row fx-appear">${cells}</tr>`;
        });

        body.innerHTML = rows.trim();
    }


    // return: body
    return body
}
