// render.table.date
import { objectMapper } from '../object.mapper'
import { formatDate } from '../format.date'


// ---


export const tableDate = (data: any, object: any) => {

    // set: variables
    let label: string = '';


    // render: label
    if (object.date.datasource) {
        const value: string = objectMapper(object.date.datasource, data);

        label = formatDate(value, object.date.format);
    }


    // return: label
    return label;
}
