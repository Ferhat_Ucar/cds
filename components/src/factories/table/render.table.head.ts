// parse.table.ts
import { tableButton } from './render.table.button'
import { tableCheckbox } from './render.table.checkbox'
import { tableContextMenu } from './render.table.context-menu'
import { tableInput } from './render.table.input'



// ---



export const renderTableHead = (template: any) => {

    // set: variables
    let filter: string = '';
    let header: string = '';
    let html: string = '';



    // create: head
    const head: HTMLTableSectionElement = document.createElement('thead');
    head.className = `c-table-head`;


    // render: headers
    if (template[0].head) {
        template[0].head.forEach((heading: any, index: number) => {

            if (heading.button)     header += `<th class="c-table-header ${heading.classname || ''}">${tableButton(heading, index)}</th>`;
            if (heading.checkbox)   header += `<th class="c-table-header ${heading.classname || ''}">${tableCheckbox(null, heading, 'select-all')}</th>`;
            if (heading.context)    header += `<th class="c-table-header ${heading.classname || ''}">${tableContextMenu(heading, index)}</th>`;
            if (heading.heading)    header += `<th class="c-table-header ${heading.classname || ''}">${heading.heading.label || ''}</th>`;
        });
        html += `<tr class="c-table-row">${header.trim()}</tr>`;
    }


    if (template[0].filter) {
        template[0].filter.forEach((heading: any, index: number) => {

            if (heading.input)      filter += `<th class="c-table-header ${heading.classname || ''}">${tableInput(heading, index)}</th>`;
            if (heading.heading)    filter += `<th class="c-table-header ${heading.classname || ''}">${heading.heading.label || ''}</th>`;
        });
        html += `<tr class="c-table-row filter">${filter.trim()}</tr>`;
    }

    head.innerHTML = html.trim();



    // return
    return head;
}
