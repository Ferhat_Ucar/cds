// render.table.button

export const tableButton = (object: any, index: number) => {

    // set: variables
    let button: string = '';
    let data: string = '';


    // render: html
    if (object.button) {
        const label: string = object.button.label;
        const icon: string = (object.button.icon) ? `<c-icon is="${object.button.icon}"></c-icon>` : '';


        if (object.button.sort) {
            data = `data-sort-sortable="${object.button.sort.sortable}" data-sort-type="${object.button.sort.type}" data-sort-field="${object.button.sort.field}"`;
        }

        button = `<c-button task="${object.button.task}" is="${object.button.type}" css="${object.button.css || ''}" id="c-table-${object.button.task}-${index}" ${data}>
                    ${label}
                    ${icon}
                </c-button>`;
    }


    // return: button
    return button.trim();
}
