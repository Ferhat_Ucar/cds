// render.table.input

export const tableInput = (object: any, index: number) => {

    let input: string = '';

    // create: input
    if (object.input) {
        input = `
            <c-input
                is="text"
                value=""
                placeholder="${object.input.placeholder}"
                tabindex="-1" 
                id="${object.input.id}">
                ${object.input.value}
            </c-input>`;
    }


    // return: input
    return input.trim();
}
