// render.table.link
import { objectMapper } from '../object.mapper'


// ---


export const tableLink = (data: any, object: any) => {

    // set: variables
    let link: string = '';


    // render: link
    if (object.link) {
        link = `<c-link link="${object.link.path}">${objectMapper(object.link.datasource, data)}</c-link>`;
    }


    // return: link
    return link.trim();
}
