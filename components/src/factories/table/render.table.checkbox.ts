// render.table.checkbox
import { objectMapper } from '../object.mapper'

export const tableCheckbox = (data: any, object: any, index: any, selected: string[] = []) => {

    // set: variables
    let checkbox: string = '';
    let value: string = '';
    let checked: string = '';


    // render: html
    if (object.checkbox) {
        if (data === null) {
            if (object.checkbox.value) value = object.checkbox.value;
        } else {
            if (object.checkbox.value) {
                value = objectMapper(object.checkbox.value, data);
                if (selected.indexOf(value.toString()) > -1) checked = 'checked';
            }
        }

        checkbox = `<c-checkbox is="checkbox" task="${object.checkbox.task}" value="${value}" id="table-checkbox-${index}" ${checked}>${object.checkbox.label}</c-checkbox>`;
    }


    // return: checkbox
    return checkbox.trim();
}
