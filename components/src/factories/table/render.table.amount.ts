// render.table.amount
import { objectMapper } from '../object.mapper'


// ---


export const tableAmount = (data: any, object: any) => {

    // set: variables
    let label: string = '';


    // render: innerHTML
    if (object.amount.datasource) {
        label = objectMapper(object.amount.datasource, data);
        if (typeof label === 'number') label = Number(label).toFixed(2);
    }


    // return: html
    return label;
}
