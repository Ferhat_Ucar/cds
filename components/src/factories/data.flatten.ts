// flatten json to array

export const dataFlatten: Function = (object: any = {}) => {

    return Object.keys(object).reduce((result: any, key: any) => {

        if (typeof object[key] === 'object' && object[key] !== null) {
            result.push(...dataFlatten(object[key]));
        } else {
            if (object[key] !== null) result.push(object[key].toString());
        }

        return result;
    }, []);
}
