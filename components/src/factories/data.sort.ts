// sort data

export const dataSort: Function = (field: string = '', direction: boolean = true, type: string = "") => {

    return (a: any, b: any) => {
       let first: any   = '';
       let second: any  = '';

       // get nested items
       const A = field.split('.').reduce((object: object, key: string) => (object && object[key] !== 'undefined') ? object[key] : undefined, a);
       const B = field.split('.').reduce((object: object, key: string) => (object && object[key] !== 'undefined') ? object[key] : undefined, b);

       switch(type) {
           case 'date':
               // @ts-ignore
               first   = new Date(A);
               // @ts-ignore
               second  = new Date(B);
                break;
           case 'number':
               // @ts-ignore
               first   = parseInt(A);
               // @ts-ignore
               second  = parseInt(B);
                break;
           case 'string':
                first   = A;
                second  = B;
                break;

            default:
                first   = A;
                second  = B;
        }

        return ( (first < second) ? -1 : ((first > second) ? 1 : 0) ) * [-1, 1][+!!direction];
   }
}
