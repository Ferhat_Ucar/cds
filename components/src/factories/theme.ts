// theme.ts

export const theming: Function = () => {
    const theme: string = document.body.getAttribute('data-theme') || 'calvi';
    sessionStorage.setItem('cds:theme', theme);
}
