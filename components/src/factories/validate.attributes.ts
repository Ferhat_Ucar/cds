// attributes.validator.ts
import { errorReporter } from './error.reporter';


// ---


export const attributesValidator: Function = (element: any, attributes: object) => {

    const error : any = {
        state: false,
        type: 'warning',
        message: ['']
    }


    for (const attribute in attributes) {

        // not allowed
        if (attributes[attribute] === 'not-allowed') {
            if (element.hasAttribute(attribute)) {
                error.state = true;
                error.type = 'critical';
                error.message.push(`Attribute <strong>${attribute}</strong>: not allowed`);
            }
        }

        // not empty
        if (attributes[attribute] === 'not-empty') {
            if (!element.hasAttribute(attribute)) {
                error.state = true;
                error.type = (error.type === 'critical') ? 'critical' : 'warning';
                error.message.push(`Attribute <strong>${attribute}</strong>: missing`);
            } else {
                if (element.getAttribute(attribute) === "") {
                    error.state = true;
                    error.type = (error.type === 'critical') ? 'critical' : 'warning';
                    error.message.push(`Attribute <strong>${attribute}</strong>: empty`);
                }
            }
        }

        // array types
        if (attribute === 'is') {
            const filter = attributes[attribute].filter((item: string) => (
                element.getAttribute(attribute) === item ||
                (element.hasAttribute(attribute) === false && item === ''))
            );

            if (filter.length < 1) {
                error.state = true;
                error.type = 'critical';
                error.message.push(`Attribute <strong>${attribute}</strong>: value ${element.getAttribute(attribute)} is not permitted`);
            }
        }

        // inner html
        if (attribute === 'html') {
            for (const value of attributes[attribute]) {
                if (element.getAttribute('is') === value[0]) {
                    const html = element.innerHTML;
                    const pattern = new RegExp(`<.*.${value[1]}.*>.*?</.*>`);
                    const result = pattern.test(html);
                    if (!result) {
                        error.state = true;
                        error.type = 'critical';
                        error.message.push(`Element <strong>${value[0]}</strong>: ${value[1]} is missing`);
                    }
                }
            }

        }
    }


    // error reporting
    if (error.state) {
        errorReporter[error.type].render(element.nodeName, error.message);
    }
}
