// scroll.intoview.ts |-> if needed
interface collisionInterface {
    top: boolean,
    bottom: boolean
}

// ---


export const scroll: Function = (parent: HTMLElement, child: HTMLElement, key: string) => {

    // vars
    const elements: any = Array.from(parent.querySelectorAll(child.nodeName));
    const index: number = elements.indexOf(child);


    // collision
    const collision: collisionInterface = {
        top: ((child.offsetTop - parent.offsetTop) < parent.scrollTop),
        bottom: (child.offsetTop - parent.offsetTop + child.clientHeight) > (parent.scrollTop + parent.clientHeight - 50),
    }


    // if needed scroll into view
    if (key === 'ArrowDown' && (collision.bottom || index === 0)) {
        parent.scrollTop = child.offsetTop;
    }
    if (key === 'ArrowUp' && (collision.top || (index === (elements.length - 1)))) {
        parent.scrollTop = child.offsetTop;
    }
}
