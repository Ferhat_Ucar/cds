// search data
import { dataFlatten } from './data.flatten'


export const dataSearch: Function = (haystack: any, needle: any = "") => {

    return haystack.reduce((result: any, object: any) => {

        const flat: string[] = dataFlatten(object);
        const search: string[] = flat.filter((item: any) => item.toLowerCase().indexOf(needle.toLowerCase()) > -1);

        if (search.length > 0) result.push(object);

        return result;
    }, []);
    
}
