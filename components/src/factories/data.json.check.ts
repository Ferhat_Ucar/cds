// check if data is valid json

export const jsonCheck: Function = (value: string) => {
    try {
        return (JSON.parse(value) && !!value);
    } catch (e) {
        return false;
    }
}
