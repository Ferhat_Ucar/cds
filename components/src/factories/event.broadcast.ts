// broadcast dispatch event

export const broadcast: Function = (component: any, identifier: string = '', event: any = null, details: object = {}) => {
    console.log(identifier)
    console.log(details)
    return component.dispatchEvent(new CustomEvent(identifier, {
        bubbles: true,
        detail: {
            component: () => identifier,
            focus: () => component === document.activeElement,
            key: () => event.key || false,
            target: () => event.target || false,
            type: () => event.type || false,
            ...details,
        },
    }));
}
