// collision
interface clientInterface {
    top: number,
    left: number,
    width: number,
    height: number,
}

interface dimensionsInterface {
    parent: ClientRect,
    child: ClientRect
}

export const collision: Function = (parent: HTMLElement, child: HTMLElement) => {
    // set: vars
    let align: string = 'position-right';

    // client
    const client: clientInterface = {
        top: window.scrollY,
        left: window.scrollX,
        width: window.innerWidth,
        height: window.innerHeight
    };

    // dimensions
    const dimensions: dimensionsInterface = {
        parent: parent.getBoundingClientRect(),
        child: child.getBoundingClientRect()
    };

    // align
    if (dimensions.parent.top + (dimensions.child.bottom - dimensions.child.top) > client.height) {
        align = 'position-bottom';
    }
    if (dimensions.parent.left - (dimensions.child.right - dimensions.child.left) > 0) {
        align = 'position-left';
    }
    if (dimensions.parent.top - (dimensions.child.bottom - dimensions.child.top) > 0) {
        align = 'position-top';
    }

    // default
    if (dimensions.parent.right + (dimensions.child.right - dimensions.child.left) < client.width) {
        align = 'position-right';
    }

    // update dom
    parent.classList.remove(
        'position-top',
        'position-right',
        'position-bottom',
        'position-left'
    );
    parent.classList.add(align);
};
