// format.camelcase

export const hyphenToCamelCase = (value: string) => value.replace(/-([a-z])/g, (value) => value[1].toUpperCase());


export const camelCaseToHyphen = (value: string) => value.replace(/([A-Z])/g, (value) => '-' + value.toLowerCase());
