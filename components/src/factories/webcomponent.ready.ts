/*
    components are rendered from the inside out
    @input: component: webcomponent
    @input: check: 'blocks', 'components', 'view's
    @returns: promise
*/

export const ready: Function = (
    component: any,
    check: string
) => new Promise((resolve) => {
    // first check
    if ((component.innerHTML.trim().indexOf('<c-') < 0 ||
        component.children.length === 0) &&
        !component.hasAttribute('ready')) {
        resolve(true);
        return;
    }

    const observer: MutationObserver = new MutationObserver(() => {
        const items: any = Array.from(component.children).filter((item: any) => {
            if (check === 'components') return item.tagName.match(/^c-/i) && !item.hasAttribute('ready');
            if (check === 'blocks') return item.tagName.match(/^block-/i) && !item.hasAttribute('ready');
            if (check === 'views') return item.tagName.match(/^view-/i) && !item.hasAttribute('ready');
            if (check === 'components and blocks') return (
                item.tagName.match(/^c-/i) &&
                !item.hasAttribute('ready')
            ) || (
                item.tagName.match(/^block-/i) &&
                !item.hasAttribute('ready')
            );
        });

        if (items.length < 1) {
            observer.disconnect();
            resolve(component);
            return;
        }
    });

    observer.observe(component, {
        attributes: false,
        childList: true,
        characterData: false,
        subtree: true
    });
});
