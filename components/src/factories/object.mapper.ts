// object.mapper
// map data object to template variables

export const objectMapper: Function = (needle: any, haystack: any) =>
    needle
        .split('.')
        .reduce((object: object, key: string) => (object && object[key] !== 'undefined') ? object[key] : undefined, haystack);
