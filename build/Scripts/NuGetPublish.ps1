﻿
param
(
    [string]$nugetExe,
    [string]$octopusApiKey
)


$url = "https://deploy.dev.calvi.nl/nuget/packages?replace=true"

& $nugetExe push -Source $url -ApiKey $octopusApiKey .\repository\*.nupkg