﻿
param
(
    [string]$nugetExe,
    [string]$packageVersion
)

$basePath = (Get-Location).Path
$outputDirectory = ".\repository"

$packages = @()
$packages += "calvi-design-system.cds.nuspec"
$packages += "calvi-design-system.cds.deployment.nuspec"

# Recreate outputDirectory
if (Test-Path $outputDirectory)
{
    Remove-Item $outputDirectory -Force
}
New-Item $outputDirectory -ItemType Directory

# Create nuget packages.
foreach ($package in $packages)
{
    & $nugetExe pack "Build\Packaging\$($package)" -Version $packageVersion -BasePath $basePath -OutputDirectory $outputDirectory
}