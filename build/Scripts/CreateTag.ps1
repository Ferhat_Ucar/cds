﻿
param 
(
    $branch,
    $packageVersion
)

# Create git tag (except for bugfix and feature branches)
if (-not $branch.StartsWith("bugfix/") -and -not $branch.StartsWith("feature/")) {
    git tag $packageVersion
    git push origin $packageVersion
}