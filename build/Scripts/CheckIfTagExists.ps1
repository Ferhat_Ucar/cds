﻿
param
(
    [string]$branch
)

# Abort build if a tag already exists on the head.
# This validation should be skipped for bugfix and feature branches. 
# This ensures that no duplicate releases will be created.
if (-not $branch.StartsWith("bugfix/") -and -not $branch.StartsWith("feature/"))
{
    $tags = git tag -l --points-at HEAD
    if ($tags) 
    {
        Write-Host "There already exists a tag on the current head! Aborting.." 
        Exit -1
    }
}