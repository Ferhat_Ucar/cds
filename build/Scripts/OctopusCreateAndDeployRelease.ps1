﻿
param
(
    [string]$octopusApiKey,
    [string]$packageVersion,
    [string]$channel,
    [string]$environment,
    [string]$binding
)

# Copied this script from: https://github.com/OctopusDeploy/OctopusDeploy-Api/blob/master/REST/PowerShell/Deployments/CreateReleaseAndDeployment.ps1

##CONFIG##
$OctopusURL = "https://deploy.dev.calvi.nl"
$ProjectName = "CDS" 

##PROCESS##

# Set authorization header
$Header =  @{ "X-Octopus-ApiKey" = $octopusApiKey }

# Get project resource
$ProjectResource = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/projects/$($ProjectName)" `
    -Headers $Header `
    -UseBasicParsing `
    | ConvertFrom-Json

# Get channel resource
$ChannelResources = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/projects/$($ProjectResource.Id)/channels" `
    -Headers $Header `
    -UseBasicParsing `
    | ConvertFrom-Json
$ChannelResource = $ChannelResources.Items | ?{$_.Name -eq $channel}

# Get environment resource
$EnvironmentResources = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/Environments/all" `
    -Headers $Header `
    -UseBasicParsing `
    | ConvertFrom-Json
$EnvironmentResource = $EnvironmentResources | ?{$_.Name -eq $environment}

# Create release
$ReleaseBody =  @{ 
    Projectid = $ProjectResource.Id
    ChannelId = $ChannelResource.Id
    version = $packageVersion
    SelectedPackages = @(
	    @{
            StepName = "Delete website"
            Version = $packageVersion
        },
        @{
            StepName = "Delete application pool"
            Version = $packageVersion
        },
        @{
            StepName = "Deploy to IIS"
            Version = $packageVersion
        },
        @{
            StepName = "Test heartbeat endpoints"
            Version = $packageVersion
        }
    )
} | ConvertTo-Json
$ReleaseResource = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/releases" `
    -Method Post `
    -Headers $Header `
    -Body $ReleaseBody `
    -UseBasicParsing `
    | ConvertFrom-Json

# Fetch bindingId from DeploymentPreviewResource
$DeploymentPreviewResource = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/releases/$($ReleaseResource.Id)/deployments/preview/$($EnvironmentResource.Id)" `
    -Method Get `
    -Headers $Header `
    -UseBasicParsing `
    | ConvertFrom-Json
$elements = $DeploymentPreviewResource.Form.Elements
$bindingId = $elements | ?{$_.Control.Name -eq "binding"} | Select -ExpandProperty Name
Write-Host "bindingId: '$bindingId'"

# Set formValues
# bindingId should only be added to formValues for bugfix and feature builds.
$formValues = @{}
if ($bindingId -ne $null)
{
    $formValues = @{
        $bindingId = $binding
    }
}

# Deploy release
$DeploymentBody = @{ 
    ReleaseID = $ReleaseResource.Id
    EnvironmentID = $EnvironmentResource.Id
    FormValues = $formValues
} | ConvertTo-Json        
$DeploymentResource = Invoke-WebRequest `
    -Uri "$($OctopusURL)/api/deployments" `
    -Method Post `
    -Headers $Header `
    -Body $DeploymentBody `
    -UseBasicParsing `
    | ConvertFrom-Json

# Wait until deployment is done
while ($true)
{
    $TaskResource = Invoke-WebRequest `
        -Uri "$($OctopusURL)/api/tasks/$($DeploymentResource.TaskId)" `
        -Method Get `
        -Headers $Header `
        -UseBasicParsing `
        | ConvertFrom-Json

    $taskState = $TaskResource.State
    Write-Host "Task state: $($taskState)"

    $runningStates = @("Queued", "Executing")
    if ($runningStates -icontains $taskState)
    {
        Write-Host "Task is still running - wait for 10 seconds."
        Start-Sleep -Seconds 10
    }
    else
    {
        if ($taskState -ieq "Success")
        {
            Write-Host "Task succeeded - break out of while loop."
            break
        }
        else
        {
            Write-Error "Task didn't succeed."
            Exit
        }

    }
}