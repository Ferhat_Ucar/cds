﻿
param 
(
    [string]$branch,
    [string]$stashUsername,
    [string]$stashPassword,
    [string]$binding
)

## VARIABLE SECTION
$repoName = "CDS"

## PROGRAM SECTION
$usernamePwd = "$($stashUsername):$($stashPassword)"
if(!$branch.ToUpper().startswith("RELEASE/") -and !$branch.ToUpper().startswith("MASTER"))
{
    $binding = "https://CDS-$($binding).portal.dev.calvi-insight.com"
    $project = 'CDS'
    $bitbucketUrl = "https://code.dev.calvi.nl"

    $headers = @{}
    $headers.Add('Accept', 'application/json')

    $bytes = [System.Text.Encoding]::UTF8.GetBytes($usernamePwd)
    $creds = 'Basic ' + [Convert]::ToBase64String($bytes)
    $headers.Add('Authorization', $creds)

    $url = '{0}/rest/api/1.0/projects/{1}/repos/{2}/pull-requests?limit=100' -f $bitbucketUrl,$project,$repoName

    $response = try {
        Invoke-RestMethod -Method Get `
                -Uri $url `
                -Headers $headers `
                -ContentType 'application/json' `
    } catch {
        $_.Exception.Response
    }

    Foreach($pullRequest in $response.values)
    {
        If ($pullRequest.fromRef.displayId -ieq $branch)
        {
            $comment = "CDS is deployed successfully at: $($binding)."

            $data = '{"text": "' + $comment + '"}'
            $url = '{0}/rest/api/1.0/projects/{1}/repos/{2}/pull-requests/{3}/comments' -f $bitbucketUrl,$project,$repoName,$pullRequest.id
        
            Invoke-RestMethod -Method Post `
                -Uri $url `
                -Headers $headers `
                -ContentType 'application/json' `
                -Body $data
        }
    }
}