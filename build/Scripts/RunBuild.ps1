﻿
param
(
    [int]$buildCounter,
    [string]$teamcityAgentName,
    [string]$stashUsername,
    [string]$stashPassword,
    [string]$nugetExe,
    [string]$octopusApiKey
)

function InitializeStep($step)
{
    $Error.Clear()
    Write-Host ""
    Write-Host "Execute step: '$($step)'."
}

function FinalizeStep ($step)
{
    if ($Error -ne $null)
    {
        Write-Host "Failed to execute step: '$($step)'"
        Write-Host "Aborting...."
        Exit -1
    }
    Write-Host "Successfully executed step: '$($step)'."
}


# Check if tag already exists
$step = "Check if tag already exists."
InitializeStep -step $step
& "$($PSScriptRoot)\CheckIfTagExists.ps1" `
    -branch $branch
FinalizeStep -step $step

# Set build parameters
$step = "Set build parameters"
InitializeStep -step $step
Write-Host "Set build parameters."
& "$($PSScriptRoot)\SetBuildParameters.ps1" `
    -buildCounter $buildCounter `
    -teamcityAgentName $teamcityAgentName `
    -outputBranch "branch" `
    -outputMajorVersion "majorVersion" `
    -outputMinorVersion "minorVersion" `
    -outputPatchVersion "patchVersion" `
    -outputPackageVersion "packageVersion" `
    -outputChannel "channel" `
    -outputBinding "binding" `
    -outputEnvironment "environment"
Write-Host "Build parameters:"
Write-Host "* branch: $($branch)"
Write-Host "* majorVersion: $($majorVersion)"
Write-Host "* minorVersion: $($minorVersion)"
Write-Host "* patchVersion: $($patchVersion)"
Write-Host "* packageVersion: $($packageVersion)"
Write-Host "* channel: $($channel)"
Write-Host "* binding: $($binding)"
Write-Host "* environment: $($environment)"
FinalizeStep -step $step

# NVM install
$step = "Set node version"
InitializeStep -step $step
& "$($PSScriptRoot)\SetNodeVersion.ps1"
FinalizeStep -step $step

# NPM install
$step = "NPM install"
InitializeStep -step $step
& "$($PSScriptRoot)\NpmInstall.ps1"
FinalizeStep -step $step

# NPM build
$step = "NPM build"
InitializeStep -step $step
& "$($PSScriptRoot)\NpmBuild.ps1" `
    -nugetExe $nugetExe
FinalizeStep -step $step

# NuGet pack
$step = "NuGet pack"
InitializeStep -step $step
& "$($PSScriptRoot)\NuGetPack.ps1" `
    -nugetExe $nugetExe `
    -packageVersion $packageVersion
FinalizeStep -step $step

# NuGet publish
$step = "NuGet publish"
InitializeStep -step $step
& "$($PSScriptRoot)\NuGetPublish.ps1" `
    -nugetExe $nugetExe `
    -octopusApiKey $octopusApiKey
FinalizeStep -step $step

# Octopus create release
$step = "Octopus create and deploy release"
InitializeStep -step $step
& "$($PSScriptRoot)\OctopusCreateAndDeployRelease.ps1" `
    -octopusApiKey $octopusApiKey `
    -packageVersion $packageVersion `
    -channel $channel `
    -environment $environment `
    -binding $binding
FinalizeStep -step $step

# Create tag
$step = "Create tag"
InitializeStep -step $step
Write-Host 
& "$($PSScriptRoot)\CreateTag.ps1" `
    -branch $branch `
    -packageVersion $packageVersion 
FinalizeStep -step $step

# Post comment to pull request
$step = "Post comment to pull request"
InitializeStep -step $step
& "$($PSScriptRoot)\PostCommentToPullRequest.ps1" `
    -branch $branch `
    -stashUsername $stashUsername `
    -stashPassword $stashPassword `
    -binding $binding
FinalizeStep -step $step