﻿
param
(
    [int]$buildCounter,
    [string]$teamcityAgentName,
    [string]$buttonFormData,
    [string]$outputBranch,
    [string]$outputMajorVersion,
    [string]$outputMinorVersion,
    [string]$outputPatchVersion,
    [string]$outputPackageVersion,
    [string]$outputChannel,
    [string]$outputBinding,
    [string]$outputEnvironment
)


# Fetch version number from TXT file and split it into major, minor and patch versions.
$versions = ((Get-ChildItem |Where-Object {$_.Name.EndsWith(".txt") -and $_.Length -eq 0}|Select-Object -ExpandProperty Name).Replace(".txt","")).Split('.')
$majorVersion = $versions[0]
$minorVersion = $versions[1]
$patchVersion = $buildCounter

# Get branch
$branch = git rev-parse --abbrev-ref HEAD

# Set build variables
if ($branch.StartsWith("bugfix/") -or $branch.StartsWith("feature/")) {
    $packageVersion = "$($majorVersion).$($minorVersion).$($patchVersion)-dev"
    $channel = "CDS Development"
    $PBI = ($branch -replace '(.*)[/-]((DEV)-[0-9]*)(-.*)','$2')
    $binding = "R$($majorVersion)$($minorVersion)-$($PBI)" 
    $environment = $teamcityAgentName
}
else
{
    $packageVersion = "$($majorVersion).$($minorVersion).$($patchVersion)"
    $channel = "CDS Release $($majorVersion)"
    $binding = "RELEASE-$($majorVersion)$($minorVersion)"
    $environment = "CDS Release $($majorVersion)"
}

# Set output variables.
Set-Variable -Name $outputBranch -Value $branch -Scope 1
Set-Variable -Name $outputMajorVersion -Value $majorVersion -Scope 1
Set-Variable -Name $outputMinorVersion -Value $minorVersion -Scope 1
Set-Variable -Name $outputPatchVersion -Value $patchVersion -Scope 1
Set-Variable -Name $outputPackageVersion -Value $packageVersion -Scope 1
Set-Variable -Name $outputChannel -Value $channel -Scope 1
Set-Variable -Name $outputBinding -Value $binding -Scope 1
Set-Variable -Name $outputEnvironment -Value $environment -Scope 1