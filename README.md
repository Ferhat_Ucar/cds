# Calvi Design System

Calvi Design System is the web-component based System for all Calvi applications

## Requirements

Both the Angular CLI and generated project have dependencies that require Node 8.9 or higher,
together with NPM 5.5.1 or higher.

This application uses Node version 10.15.0

If you need to maintain older node versions for other projects, please use Node Version Manager (nvm)
https://github.com/creationix/nvm/blob/master/README.md


## Angular installation

Install Angular CLI global:

```
npm install -g @angular/cli
```


## Installation

How to install and build all sub-projects, from the root of the project:

```
npm install
npm run calvi-install
```


## Run

Run all sub-projects, from the root of the project:

```
npm run calvi
```


## NPM

Build a new NPM package

```
npm run calvi-npm
```

This will publish the javascript and css files to the /npm-output directory

Before publishing, update the version number in /npm-output/package.json

Publishing:
If you have the correct credentials:

```
cd npm-output
npm run calvi-npm
```
